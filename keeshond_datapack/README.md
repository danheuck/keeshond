# 🐶 KEESHOND Datapack 🐶

## About

Keeshond Datapack lets you easily load resources for your game and cache them in memory, mapped via pathname and
accessible by simple numeric ID lookup. Define how they load with the `DataObject` trait, by implementing a function
that takes a `Read + Seek` object. Define where they load from by instantiating a `Source`.

Datapack is used by Keeshond but can work with any engine.

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.


## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
