#![warn(missing_docs)]

//! `keeshond_datapack` lets you easily load resources for your game and cache them in memory,
//!  mapped via pathname and accessible by simple numeric ID lookup. Define how they load
//!  with the [DataObject] trait, by implementing a function that takes a [Read] + [Seek] object.
//!  Define where they load from by instantiating a [source::Source].
//!
//! Datapack is used by Keeshond but can work with any engine.
//!
//! # How to use
//!
//! - Implement the [DataObject] trait for each type of object you want to be able to load in.
//! - Instantiate a [source::SourceManager] object and give it a [source::Source] such as a
//!  [source::FilesystemSource]
//! - Create a folder for each package on disk (if using [FilesystemSource](source::FilesystemSource)).
//! - Create subfolders within each package for each data type and place files within.
//! - Instantiate a [DataStore] for each data type you want to load.
//! - Use [DataStore::load_package()] to ensure the package is loaded at start.
//! - When creating objects that need resources, use [DataStore::get_id()] or [DataStore::get_id_mut()]
//!  to get a [DataId]. Store this id somewhere it can be used later, as these functions do involve a
//!  couple hash lookups.
//! - When using a resource, use [DataStore::get()] or [DataStore::get_mut()] with your id to get
//!  a reference to it so you can use it.
//!
//! Pathnames are of the form `path/to/file.png`. They must match exactly, including the use of one
//!  forward slash as a separator, and no separators at the beginning or end. The package name and
//!  data type folders are not included (they are implied). Pathnames are also case-sensitive even on
//!  platforms with case-insensitive filesystems (Windows, macOS).

extern crate hashbrown;
#[macro_use] extern crate try_or;
extern crate failure;
#[macro_use] extern crate failure_derive;
extern crate walkdir;

#[cfg(test)] mod tests;

/// The `Source` trait and implementations.
pub mod source;

use crate::source::{PackageError, Source, SourceManager};

use hashbrown::hash_map::HashMap;
use hashbrown::hash_set::HashSet;

use std::cell::RefCell;
use std::rc::Rc;
use std::io::{Read, Seek};
use std::marker::Sized;

/// A numeric ID used to refer to a [DataObject] of a specific type.
pub type DataId = usize;

/// Return type when loading information from a [DataStore]
#[derive(Debug, PartialEq)]
pub enum DataStoreOk
{
    /// The package was loaded successfully
    Loaded,
    /// The package was already loaded, so no action was taken
    AlreadyLoaded
}

/// Return type when failing to load a [DataObject] from a [DataStore]
#[derive(Debug, Fail)]
pub enum DataError
{
    /// The given package was not found in the [source::Source]
    #[fail(display = "The package was not found")]
    PackageNotFound,
    /// The package is loaded, but has no [DataObject] by the given pathname
    #[fail(display = "The data object was not found")]
    DataNotFound,
    /// An invalid character (forward slash) was used in a package or type folder name
    #[fail(display = "Invalid character in name")]
    BadName,
    /// An error occurred while reading the list of items of the package
    ///  from the [source::Source]
    #[fail(display = "Failed to read the package list: {}", _0)]
    PackageListError(PackageError),
    /// An error of type [std::io::Error] occurred.
    #[fail(display = "{}", _0)]
    IoError(#[cause] std::io::Error),
    /// A logical error occurred while reading a [DataObject]
    #[fail(display = "The data object contained invalid data: {}", _0)]
    BadData(String)
}

/// Return type for when [PreparedStore] operations fail
#[derive(Debug, Fail)]
pub enum PreparedStoreError
{
    /// The DataStore provided in [PreparedStore::new()] is already associated with another
    ///  [PreparedStore]
    #[fail(display = "The provided DataStore is already associated with a PreparedStore.")]
    AlreadyConnected,
    /// The DataStore provided in [PreparedStore::sync()] is not the same one passed to
    ///  [PreparedStore::new()]
    #[fail(display = "The provided DataStore is not the one used to create this PreparedStore.")]
    WrongDataStore
}

impl From<std::io::Error> for DataError
{
    fn from(error : std::io::Error) -> DataError
    {
        DataError::IoError(error)
    }
}

struct LoadedData<T : DataObject + 'static>
{
    data_object : T,
    index : DataId,
    pathname : String
}

/// A boxable trait that implements both [Read] and [Seek], used by the
///  [source::Source] types.
pub trait ReadSeek : Read + Seek {}

impl<T: Read + Seek> ReadSeek for T {}

/// Represents a data item loaded from a package. Implement this trait to allow the system
///  to load new types of data.
pub trait DataObject
{
    /// The folder name that [DataObjects](DataObject) of this type are stored in
    fn folder_name() -> &'static str where Self : Sized;
    /// Determines whether or not a given file should be loaded while iterating through a package.
    fn want_file(pathname : &str) -> bool where Self : Sized;
    /// A constructor that returns a new [DataObject] of this type given a [Read] + [Seek] object
    fn from_io(reader: Box<dyn ReadSeek>, full_pathname : &str, source : &mut Box<dyn Source>) -> Result<Self, DataError> where Self : Sized;
}

/// Storage that allows lookup and access of [DataObjects](DataObject) of a given type
pub struct DataStore<T : DataObject + 'static>
{
    source_manager : Rc<RefCell<SourceManager>>,
    unloaded_packages : HashMap<String, HashMap<String, DataId>>,
    data_list : Vec<Option<T>>,
    data_map : HashMap<String, HashMap<String, DataId>>,
    next_index : DataId,
    prepare_info : Option<Rc<RefCell<PrepareInfo>>>
}

impl<T : DataObject + 'static> DataStore<T>
{
    /// Constructs a new [DataStore] that gets its [Sources](source::Source) from the given
    ///  [SourceManager]
    pub fn new(source_manager : Rc<RefCell<SourceManager>>) -> DataStore<T>
    {
        DataStore::<T>
        {
            source_manager,
            unloaded_packages : HashMap::new(),
            data_list : Vec::new(),
            data_map : HashMap::new(),
            next_index : 1,
            prepare_info : None
        }
    }
    
    /// Loads the package by the given name if it is not already loaded.
    pub fn load_package(&mut self, package_name : &str) -> Result<DataStoreOk, DataError>
    {
        if package_name.find('/').is_some()
        {
            return Err(DataError::BadName);
        }
        
        if self.package_loaded(package_name)
        {
            return Ok(DataStoreOk::AlreadyLoaded);
        }
        
        if let Some(mut source) = self.source_manager.borrow_mut().get_package_source(package_name)
        {
            if !source.has_package(package_name)
            {
                return Err(DataError::PackageNotFound);
            }
            
            let type_folder = T::folder_name();
            
            if type_folder.find('/').is_some()
            {
                return Err(DataError::BadName);
            }
            
            let iter = source.iter_entries(package_name, type_folder);
            
            let mut loaded_items : Vec<LoadedData<T>> = Vec::new();
            let mut new_index = self.next_index;
            
            if let Some(old_package_map) = self.unloaded_packages.get(package_name)
            {
                self.data_map.insert(package_name.to_string(), old_package_map.clone());
            }
            self.unloaded_packages.remove(package_name);
            
            let package_map = self.data_map.entry(package_name.to_string()).or_insert(HashMap::new());
            
            for entry in iter
            {
                let pathname = try_or_else!(entry,
                    |error| Err(DataError::PackageListError(error)));
                
                if pathname.starts_with('/') || pathname.ends_with('/')
                {
                    return Err(DataError::BadName);
                }
                
                if !T::want_file(&pathname)
                {
                    continue;
                }
                
                let full_pathname = format!("{}/{}/{}", package_name, type_folder, pathname);
                
                let reader = try_or_else!(source.read_file(&full_pathname),
                    |error| Err(DataError::PackageListError(error)));
                
                let data_object = T::from_io(reader, &full_pathname, &mut source)?;
                let index : DataId;
                
                if let Some(entry) = package_map.get(&pathname)
                {
                    index = *entry;
                }
                else
                {
                    index = new_index;
                    new_index += 1;
                }
                
                loaded_items.push(LoadedData::<T> { data_object, index, pathname });
            }
            
            self.next_index = new_index;
            
            while self.data_list.len() <= self.next_index
            {
                self.data_list.push(None);
            }
            
            for item in loaded_items.drain(..)
            {
                self.data_list[item.index] = Some(item.data_object);
                package_map.insert(item.pathname, item.index);
                
                if let Some(prepare_info) = &self.prepare_info
                {
                    prepare_info.borrow_mut().to_prepare.insert(item.index);
                }
            }
        }
        else
        {
            return Err(DataError::PackageNotFound);
        }
        
        Ok(DataStoreOk::Loaded)
    }
    
    /// Gets the numeric ID of the [DataObject] from the given package at the given pathname.
    pub fn get_id(&self, package_name : &str, pathname : &str) -> Result<DataId, DataError>
    {
        if let Some(package_map) = self.data_map.get(package_name)
        {
            if let Some(id) = package_map.get(pathname)
            {
                return Ok(*id);
            }
            else
            {
                return Err(DataError::DataNotFound);
            }
        }
        
        Err(DataError::PackageNotFound)
    }
    
    /// Gets the numeric ID of the [DataObject] from the given package at the given pathname.
    ///  If the package is not loaded, it will be loaded automatically.
    pub fn get_id_mut(&mut self, package_name : &str, pathname : &str) -> Result<DataId, DataError>
    {
        if let Some(package_map) = self.data_map.get(package_name)
        {
            if let Some(id) = package_map.get(pathname)
            {
                return Ok(*id);
            }
            else
            {
                return Err(DataError::DataNotFound);
            }
        }
        else
        {
            self.load_package(package_name)?;
            
            return self.get_id_mut(package_name, pathname);
        }
    }
    
    /// Returns a reference to the [DataObject] by the given [DataId], if one exists.
    ///  Otherwise returns [None].
    pub fn get(&self, index : DataId) -> Option<&T>
    {
        if index >= self.data_list.len()
        {
            return None;
        }
        
        match &self.data_list[index]
        {
            Some(data) => Some(data),
            None => None
        }
    }
    
    /// Returns a mutable reference to the [DataObject] by the given [DataId], if one exists.
    ///  Otherwise returns [None].
    pub fn get_mut(&mut self, index : DataId) -> Option<&mut T>
    {
        if index >= self.data_list.len()
        {
            return None;
        }
        
        match &mut self.data_list[index]
        {
            Some(data) => Some(data),
            None => None
        }
    }
    
    /// Returns a string list of the names of each [DataObject] in the given package.
    ///  The package will need to have been loaded or this will return an empty list.
    ///  Please do not call this repeatedly.
    pub fn list_package_contents(&self, package_name : &str) -> Vec<String>
    {
        let mut listing : Vec<String> = Vec::new();
        
        if let Some(package_map) = self.data_map.get(package_name)
        {
            for (name, &index) in package_map.iter()
            {
                if self.data_list[index].is_some()
                {
                    listing.push(name.clone());
                }
            }
            
            listing.sort();
        }
        
        listing
    }
    
    /// Unloads the given package from memory. Any [DataObjects](DataObject) will be dropped,
    ///  but pathname-id mappings will be retained in memory so that existing references will
    ///  not be invalidated. Returns true if the package was loaded.
    pub fn unload_package(&mut self, package_name : &str) -> bool
    {
        if let Some(package_map) = self.data_map.get(package_name)
        {
            for id in package_map.values()
            {
                self.data_list[*id as DataId] = None;
                
                if let Some(prepare_info) = &self.prepare_info
                {
                    prepare_info.borrow_mut().to_unprepare.insert(*id);
                }
            }
            
            self.unloaded_packages.insert(package_name.to_string(), package_map.clone());
        }
        else
        {
            return false;
        }
        
        self.data_map.remove(package_name);
        
        true
    }
    
    /// Unloads all packages from memory. Any [DataObjects](DataObject) will be dropped,
    ///  but pathname-id mappings will be retained in memory so that existing references will
    ///  not be invalidated. Returns true if the package was loaded.
    pub fn unload_all(&mut self)
    {
        for (package_name, package_map) in &self.data_map
        {
            for id in package_map.values()
            {
                if let Some(prepare_info) = &self.prepare_info
                {
                    prepare_info.borrow_mut().to_unprepare.insert(*id);
                }
            }
            
            self.unloaded_packages.insert(package_name.to_string(), package_map.clone());
        }
        
        self.data_map.clear();
        self.data_list.clear();
    }
    
    /// Returns true if the given package is loaded.
    pub fn package_loaded(&self, package_name : &str) -> bool
    {
        self.data_map.contains_key(package_name)
    }
}

struct PrepareInfo
{
    to_prepare : HashSet<DataId>,
    to_unprepare : HashSet<DataId>
}

impl PrepareInfo
{
    fn new() -> PrepareInfo
    {
        PrepareInfo
        {
            to_prepare : HashSet::new(),
            to_unprepare : HashSet::new(),
        }
    }
}

/// Used with [PreparedStore], this allows the definition of behavior when initializing resources
///  with a backend. See [PreparedStore] for more information.
pub trait DataPreparer<T : DataObject + 'static, U>
{
    /// Called when data has been loaded but not "prepared" yet. You can use this, for example,
    ///  to load textures onto the GPU.
    fn prepare(&mut self, data : &mut T, id : DataId) -> U;
    /// Called when data has been unloaded recently and should be cleared in the backend. For example,
    ///  use this to unload textures from GPU memory.
    fn unprepare(&mut self, prepared : &mut U, id : DataId);
}

/// An optional companion to a [DataStore]. If you have data that needs initialization with a backend
///  (for example, textures you need to upload to a GPU), [PreparedStore] provides a way to handle this
///  in a two-step process that is borrow-checker-friendly.
///
/// Template parameter `T` is the type of the source DataObject, and template parameter `U` is the prepared
///  data. This system allows you to separate logical, backend-agnostic data from backend-specific resources.
///  For graphics, you might use `T` to represent the raw image data and metadata such as width and height,
///  and use `U` to represent a handle to the texture in VRAM.
///
/// In order to function, you should call [PreparedStore::sync()] once per frame before needing to use the resources in
///  question in your backend. This will "prepare" or "unprepare" any resources as needed.
///
/// The [PreparedStore] instance must be associated with a [DataStore]. Only one [PreparedStore] can be associated
///  with any given [DataStore].
pub struct PreparedStore<T : DataObject + 'static, U>
{
    data_list : Vec<Option<U>>,
    data_preparer : Box<dyn DataPreparer<T, U>>,
    prepare_info : Rc<RefCell<PrepareInfo>>
}

impl<T : DataObject + 'static, U> PreparedStore<T, U>
{
    /// Creates a new [PreparedStore] that holds prepared versions `U` of [DataObjects](DataObject) `T`, and
    ///  associates with the given [DataStore].
    pub fn new(data_store : &mut DataStore<T>, data_preparer : Box<dyn DataPreparer<T, U>>) -> Result<PreparedStore<T, U>, PreparedStoreError>
    {
        if data_store.prepare_info.is_some()
        {
            return Err(PreparedStoreError::AlreadyConnected);
        }
        
        let prepare_info = Rc::new(RefCell::new(PrepareInfo::new()));
        data_store.prepare_info = Some(prepare_info.clone());
        
        Ok(PreparedStore::<T, U>
        {
            data_list : Vec::new(),
            data_preparer,
            prepare_info
        })
    }
    
    /// Returns a reference to the prepared data by the given [DataId], if one exists.
    ///  Otherwise returns [None].
    pub fn get(&self, index : DataId) -> Option<&U>
    {
        if index >= self.data_list.len()
        {
            return None;
        }
        
        match &self.data_list[index]
        {
            Some(data) => Some(data),
            None => None
        }
    }
    
    /// Returns a mutable reference to the prepared data by the given [DataId], if one exists.
    ///  Otherwise returns [None].
    pub fn get_mut(&mut self, index : DataId) -> Option<&mut U>
    {
        if index >= self.data_list.len()
        {
            return None;
        }
        
        match &mut self.data_list[index]
        {
            Some(data) => Some(data),
            None => None
        }
    }
    
    /// Synchronizes this store with the associated [DataStore]. If any [DataObjects](DataObject) were recently
    ///  loaded, they will be prepared by calling [DataPreparer::prepare()]. If any were recently unloaded,
    ///  they will be "unprepared" (unloaded from the backend) by calling [DataPreparer::unprepare()].
    pub fn sync(&mut self, data_store : &mut DataStore<T>) -> Result<(), PreparedStoreError>
    {
        if let Some(prepare_info) = &mut data_store.prepare_info
        {
            if !Rc::ptr_eq(&prepare_info, &self.prepare_info)
            {
                return Err(PreparedStoreError::WrongDataStore);
            }
        }
        else
        {
            return Err(PreparedStoreError::WrongDataStore);
        }
        
        for id in &self.prepare_info.borrow_mut().to_unprepare
        {
            if *id < self.data_list.len()
            {
                if let Some(prepared) = &mut self.data_list[*id]
                {
                    self.data_preparer.unprepare(prepared, *id);
                }
                
                self.data_list[*id] = None;
            }
        }
        for id in &self.prepare_info.borrow_mut().to_prepare
        {
            if let Some(data) = data_store.get_mut(*id)
            {
                // TODO: Make faster
                while self.data_list.len() <= *id
                {
                    self.data_list.push(None);
                }
                
                self.data_list[*id] = Some(self.data_preparer.prepare(data, *id));
            }
        }
        
        self.prepare_info.borrow_mut().to_unprepare.clear();
        self.prepare_info.borrow_mut().to_prepare.clear();
        
        Ok(())
    }
}
