+++
title = "Scenes and SceneTypes"
weight = 7
+++

Scenes are where everything happens in your game. Keeshond has a single Scene that is run by the gameloop at any time. During initial setup, you can specify the initial Scene to use by constructing it and then setting it in the `Gameloop` object like so:

```rust
let my_scene = Box::new(Scene::<MySceneType>::new());

gameloop.control_mut().goto_constructed_scene(my_scene);
```

To switch to a different scene during the game, you can use the following within a System:

```rust
let my_scene = Box::new(Scene::<MySceneType>::new());

game_control.goto_constructed_scene(my_scene);
```

At this point, we have covered the basics of Scenes and the ECS. But you're probably wondering about how some of this might scale to a larger game. So far we've constructed Entities and given them Components manually. But what if you didn't want to have to do this every single time you created an object? What if you just wanted to state the kind of object you wanted to create? And what if you want to provide a list of Systems to use instead of adding them one by one?

Well, that's what SceneTypes are for.

Much like `Component` and `ThinkerSystem`, `SceneType` is a trait:

```rust
pub trait SceneType
{
    type SpawnableIdType : FromStr + 'static;
    
    fn new() -> Self where Self : Sized;
    fn spawn(&mut self, spawn : &mut SpawnControl,
        spawnable_id : Self::SpawnableIdType,
        x : f64, y : f64, args : &[SpawnArg]);
    fn thinkers(&mut self, game : &mut GameControl)
        -> Vec<Box<dyn ThinkerSystem<Self>>>;
    fn drawers(&mut self, game : &mut GameControl)
        -> Vec<Box<dyn DrawerSystem>>;
}
```

First there is a standard `new()` constructor. As is Rust convention, implement this to allow creating a new SceneType object of your type.

You'll notice that there is an inner type called `SpawnableIdType`. This is where the idea of Spawnables comes into play.

Let's say you want to make levels for your game, and you need a list of different objects that you could place, or "spawn" within each level. You need two things: first, a name for the different objects you need to spawn, and second, a list of Components that should be added to each object. We can start by naming the different objects by defining `SpawnableIdType`. Typically you'll want to set this to an enum you've created, but regardless of what you choose it needs to be creatable from a string (for the purposes of loading from data files). I suggest using the [strum](https://crates.io/crates/strum) crate to create an enum that implements `FromStr`:

```rust
use strum_macros::EnumString;

#[derive(EnumString)]
enum MySpawnId
{
    Player,
    Crate,
    Spikeball,
    RedKey,
    ScoreThing
}
```

An implementation of `spawn()` might look like this:

```rust
fn spawn(&mut self, spawn : &mut SpawnControl,
    spawnable_id : MySpawnId, x : f64, y : f64,
    args : &[SpawnArg])
{
    spawn.with(Movable::new(x, y));
    
    match spawnable_id
    {
        MySpawnId::Player =>
        {
            spawn.with(Player::new());
        },
        MySpawnId::Spikeball =>
        {
            spawn.with(Enemy::new());
        },
        _ => ()
    }
}
```

As you can see, we give all spawned Entities the `Movable` Component, and then depending on what type of object we want to create, add additional Components as needed.

During gameplay, we can spawn any Spawnable with code like this:

```rust
scene.spawn_later(MySpawnId::Spikeball, 100.0, 200.0, &[]);
```

Finally, there are the `thinkers()` and `drawers()` functions. You simply return a `Vec` of Systems you'd like to add to Scenes of this given type.

```rust
fn thinkers(&mut self, game : &mut GameControl)
    -> Vec<Box<dyn ThinkerSystem<Self>>>
{
    vec!
    [
        Box::new(MovableThinker::new()),
        Box::new(PlayerThinker::new()),
        Box::new(EnemyThinker::new())
    ]
}
fn drawers(&mut self, game : &mut GameControl)
    -> Vec<Box<dyn DrawerSystem>>
{
    vec!
    [
        Box::new(SpriteDrawer::new())
    ]
}
```

`Scene`s and `ThinkerSystem`s need to take a SceneType. This is so that they know about Spawnables and the Systems to create with the Scene. That is what the type parameter is for when constructing a `Scene`. From before:

```rust
let my_scene = Box::new(Scene::<MySceneType>::new());
```

If you're creating a small program, chances are you might not want to define a SceneType. In this case, you may use the provided `NullSceneType`.
