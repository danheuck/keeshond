+++
title = "Resource Basics"
weight = 1
+++

The first thing you should know about when dealing with resources is the concept of Sources. A Source is a location that resources can be loaded from. For example, you could load from the filesystem, or maybe a ZIP archive. You can specify multiple Sources, and the first Source to have a requested package will be the one to load it. 

Sources are generally set during the initialization of your program. Here is how a `FilesystemSource` is typically set up:

```rust
// let mut gameloop = ...

let mut gameloop = Gameloop::new(gameinfo);

let source_manager = gameloop.control_mut().source_manager();

let source = FilesystemSource::new("data");
source_manager.borrow_mut().add_source(Box::new(source));
```

This will add a new `FilesystemSource` that looks for its files in the "data" folder where the program is run from.

At this point, it is a good idea to get familiar with how Packages, DataObjects, and resource paths work. The "data" folder should contain a series of subfolders. Each one refers to a different Package. Each Package is a separate, loadable unit. For example, you might have folders (and therefore Packages) for "common", "title_screen", "forest", etc. This lets you load all the resources for a given part of your game at once without having the entire game loaded in memory.

Next is the concept of DataObjects. A DataObject is the more formal name for a resource that is loaded from a Source. Several types of DataObjects are provided by Keeshond, including Sheets, Shaders, and Sounds. These will be detailed later.

Resource paths take up the form `path/to/file.png`. The forward slash (/) is always used as the path separator, regardless of platform. Paths cannot start or end with a forward slash, are case-sensitive, and must match exactly. Otherwise they will fail to resolve.
