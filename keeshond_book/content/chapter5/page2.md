+++
title = "Loading Resources"
weight = 2
+++

Loading, querying, and accessing resources is done through the `DataStore` type. Each `DataStore` object handles operations for a specific `DataObject` type. For example, a `DataStore<Foo>` would handle loading of all `DataObject`s of type `Foo`. Keeshond provides a few standard DataStores that you use to manage the standard engine resource types. You can get at these from the `GameControl` object:

```rust
let mut resources = control.res_mut();
```

This returns a mutable reference to the engine's `GameResources` object, which contains `DataStore`s accessible through the following methods:

- `sheets_mut()` to access Sheets
- `shaders_mut()` to access Shaders
- `sounds_mut()` to access Sounds

Of course, immutable access is also available for use within `DrawerSystem`s. Simply use `res()` and then either `sheets()` or `shaders()`.

There are two ways to load resources, and both should generally be done at the start of your Scene. First, you can load the Package containing the resources ahead of time with `load_package()`:

```rust
resources.sheets_mut().load_package("doggymark");
```

This loads all `Sheet`s within the Package named "doggymark". From the filesystem's perspective, they are loaded from `doggymark/sheets`. So assuming you have several files in there named `doggy.png`, `hud.png`, and `background.png`, they would all be loaded together.

The second way to load resources is also the way to query them:

```rust
let sheet_id = resources.sheets_mut().get_id_mut("doggymark", "doggy.png").expect("Failed to load package");
```

On success, this function returns a `DataId`, which you can use to get at the `DataObject`, or simply to reference it. As mentioned in Chapter 3.2, you should look up this ID once and reuse it instead of performing the lookup multiple times. Also, if the given package ("doggymark" here) is not loaded, it will be loaded for you as if you called `load_package()`.

Once you have the `DataId`, you can access the object it refers to:

```rust
let mut sheet_opt = resources.sheets_mut().get_mut(sheet_id);

if let Some(sheet) = sheet
{
    // Do stuff with the Sheet
}
```

But for the Keeshond data types, you typically won't be doing this kind of direct access, instead passing around the `DataId`, for example to specify the Sheet to use for a rendering operation.
