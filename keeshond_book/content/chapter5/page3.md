+++
title = "Unloading and ID Validity"
weight = 3
+++

It is possible to unload Packages. For a single Package:

```rust
resources.sheets_mut().unload_package("doggymark");
```

Or for all Packages:

```rust
resources.sheets_mut().unload_all();
```

If you were to reload the affected Packages and then look up resource IDs again, you might notice that they're the same. That's because `DataId`s retain validity throughout the duration of the program - that is, the same resource will continue to have the same `DataId` associated with it. This allows you to simply store the `DataId` within any objects without having to worry about them suddenly becoming invalid or pointing to some other resource. Unlike `Entity`, `DataId` is reused but only for the resource it was originally for. Keeshond makes no distinction between different loads of the same resource. This means you can even hot-reload resources when you make changes on disk!

Keep in mind, however, that these IDs are only valid for the current session, so don't try to save the raw IDs out to a file or hardcode them into your game or anything like that. That's because IDs are assigned based on the order resources are loaded in a given session. So the first resource gets an ID of 1, the second gets an ID of 2, etc. Someone could, for example, visit your levels - and thus load your resources - in a different order, causing the IDs to change.

Additionally, `DataId` is only considered valid for the specific `DataStore` it came from. So using a `Sheet` `DataId` on the `DataStore` for `Shader`s would be incorrect. If you were to do this you would get some other resource instead.
