+++
title = "Playing Sounds"
weight = 2
+++

Once you're ready to play audio, you can control playback through the `Audio` object inside the `GameControl`:

```rust
let mut audio = game.audio_mut();
```

If all you want to do is play a sound without any frills, simply use `play_sound()`:

```rust
audio.play_sound(self.sound_id);
```

It is possible to set many more parameters, however. If you want more control over the sound, use a `VoiceInfo` object:

```rust
let voice_info = VoiceInfo
{
    volume : 0.5,
    pitch : 1.0,
    position : (0.0, 0.0),
    local : false,
    looping : true,
    .. Default::default()
};

let voice_res = audio.play_sound_with(self.sound_id, &voice_info);
```

When you play a sound, you'll get back an `Option<Voice>`. This holds an ID for a currently playing voice. If there are no free channels to play a voice, `None` is returned instead. You may use the `Voice` handle to change parameters for the voice while it's being played, like this:

```rust
let new_voice_info = VoiceInfo
{
    volume : 0.5,
    pitch : 1.2,
    position : (10.0, 0.0),
    local : false,
    looping : true,
    .. Default::default()
};

audio.set_voice(&voice_res, &voice_info);
```

In this example, we change the voice so it's slightly higher in pitch, and is 10 units over to the right spatially.

There are quite a few parameters, and you don't need to list all of them, as long as you end with `.. Default::default()`. Here are all the properties you can change, and what they do:

- `volume` - How loud the sound is. 1.0 is normal volume, 0.0 is silent.
- `pitch` - Adjusts the frequency of the sound. 1.0 is normal pitch. 2.0 plays the sound twice as fast.
- `position` - Where the sound is in the 2D space. The farther away the sound is, the quieter it will be, and depending on the horizontal position it will pan between the left and right speakers. *Note that only mono sounds will be panned.*
- `distance` - A higher distance means you can hear the sound louder from farther away. You can use this to simulate loudness - use short distance values for rustling of leaves, and large distance values for explosions.
- `rolloff_start` - How far away you need to be from the sound in order for it to start getting quieter. For most sounds you'll want to leave this at 0.0. For things like ambient tracks covering large areas, you'll want to increase this value.
- `local` - If true, the sound is always anchored to the listener. That is, no matter where you move in the game world, the sound will stay put. If false, the sound will move around as the listener is repositioned. Use true for interface sounds, and false for world sounds.
- `looping` - Whether the sound should loop. If false, the voice stops once the sound is done playing.

There are a couple parameters within `Audio` itself for configuring the listener that are good to know. First, there is the listener position, which can be set through `Audio::set_listener_position()`. Generally, you'll want this to be the same position as your camera. There is also `Audio::set_unit_scale()`. This lets you configure the size of the "units" used to determine distance from the listener. Setting this to the same value as your game's base resolution width and positioning voices based on graphics coordinates will give a good result.

You may play up to 32 voices at a time. This is so that you will get the same results across systems, especially on mobile devices where the OpenAL implementation is usually limited to 32 voices (even though on desktop OpenAL is often configured for 256 voices or more). When all voice channels are in use, new sounds can't be played, which can radically alter the end result if you're not careful. As long as you avoid playing too many, you should be fine. In the future Keeshond may be extended to have a voice priority system, so that this can be handled automatically.
