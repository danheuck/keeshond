+++
title = "Playing Audio"
weight = 7
sort_by = "weight"
+++

Keeshond allows simplified access to high-quality game audio with an easy interface for playback via OpenAL under the hood. Much like with graphics, you'll load your sounds from a Package, and then all you need to do is tell Keeshond what audio should be played when.
