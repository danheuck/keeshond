+++
title = "Why Keeshond?"
weight = 1
+++

There are many game engines available today, so why Keeshond? While there is a lot of choice in engines, few actually make for good recommendations for a variety of reasons:

## Engine scope

On one end of the scale you have famous engines like Unreal and Unity. These are large, complex engines with a massive number of systems involved, and are often aimed at driving "high end" "AAA" game experiences. Not only are such experiences essentially infeasible for smaller developers, but the cruft that comes with these engines creates a significant barrier to entry in the form of a steep learning curve. While these engines claim to do everything, their heavy architecture makes them surprisingly inflexible.

On the other hand, you have game engines that more closely resemble multimedia libraries. Such libraries include some basic features such as window creation, event handling, graphics blitting, basic audio playback, and not much else. While these ingredients can be used to make a game, an engine at this level is too barebones to recommend to a beginner looking to get their feet wet. Such engines do not offer an object or data model, self-contained game state, or even a working gameloop with timekeeping. Developers should be spending their time making a game rather than reinventing the wheel trying to correctly implement a framerate limiter.

Keeshond takes a different approach, by providing useful primitives that work well and are needed by games, while also providing a blank canvas to create just about any kind of 2D game you could want, and staying out of the way so you can make it.

## Language choice

Traditionally, games are programmed in languages such as C/C++ (and even various flavors of assembly). While these languages may have fast execution speeds, they are error-prone and often hard to debug when things go wrong. They also lack a standardized cross-platform compiler.

Of course it is also possible to write games in many other languages, such as C#, Java, Lua, Python, and Ruby, to name a few. Most of these languages are ill-suited toward performance-critical code. Distributing standalone applications is also made complex due to the fact that they do not generate platform-native binaries.

Keeshond uses the Rust programming language which combines execution performance with more stringent compile-time code checks to maintain robustness.

## Platform support

Any engine targeting the desktop should support Linux, macOS, and Windows, at the bare minimum. Many engines do not properly support macOS, and fewer have a functioning Linux port, requiring the use of Wine or other solutions in order to run these games. Even when these engines have ports on other platforms, it is not a guarantee that they will work well, especially on Linux.

Keeshond uses proven cross-platform multimedia interfaces SDL2 and OpenAL. SDL2 manages the window and input, and even disables compositing on Linux so you can have a smooth framerate. Raw mouse input, screensaver inhibition, and a gamepad API are useful features that SDL2 brings to all supported operating systems. This is accompanied by OpenAL, a snappy, high-performance game audio engine that's a natural fit for cross-platform games with no strings attached.

## Correct timekeeping

Many engines implement some form of timekeeping, but few implement it in a way that works consistently across machines. Some engines use variable timestep and are non-deterministic as a result (Construct), while others have no strategy for catching up when the display framerate drops (Game Maker).

Keeshond uses fixed timestep synchronization so you can be sure that the same inputs will create predictable outcomes regardless of machine, without the need to plaster deltatime all over your codebase.
