+++
title = "Conclusion"
weight = 9
+++

This is the end of the tutorial chapter, but it doesn't have to be the end of the tutorial! Here are a few exercises you can choose to take on:

- Change the color of the bricks (several colors are included in the provided texture)
- Allow individual bricks to have different colors
- Add bricks that take multiple hits
- Add multiple levels to the game
- Create an endless mode with slowly advancing bricks
