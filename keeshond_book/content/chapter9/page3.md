+++
title = "Setting the Scene Part 1"
weight = 3
+++

In this step we will define the different objects in our game, what they look like, and where they spawn when we start the Scene. Let's create one last file in `src/` named `scenes.rs`, and go piece by piece. Let's start with the top of the file:

```rust
use keeshond::gameloop::GameControl;
use keeshond::scene::{DrawerSystem, ThinkerSystem, SceneType, SpawnControl, SpawnArg};
use keeshond_datapack::DataId;

use crate::components::{Physical, Sprite};
use crate::systems::{SpriteDrawer, GameThinker};

use strum_macros::EnumString;

#[derive(EnumString)]
pub enum SpawnId
{
    Background,
    Bat,
    Ball,
    Brick
}
```

Here we create a new enum that we use as a spawn ID type. Using the `strum` crate we included in `Cargo.toml`, we will derive `EnumString`. This is required by Keeshond so that it can create objects given a string for the object name. For internal code, we just need to use the enum itself. We have four objects defined:

- `Background`, which will display a nice backdrop image
- `Bat`, the player-controlled bar near the bottom of the screen
- `Ball`, the bouncing ball that must be kept in play
- `Brick`, the objects we need to destroy to clear the level

Whenever we create an object, we need to decide which resources to give it. For this tutorial, each object will have one resource: a Sheet for the `Sprite`. We check to see if the Sheet it uses is loaded, and if so, assign it to the `Sprite`. We'll use this helper function to make this simpler:

```rust
fn get_sheet(game : &mut GameControl, package : &str, name : &str) -> DataId
{
    game.res_mut().sheets_mut().get_id_mut(package, name).expect("Failed to load package")
}
```

Now we need to define a new `SceneType` to be able to spawn our objects. Here's a basic skeleton for our `BatAndBallScene`:

```rust
pub struct BatAndBallScene
{
    
}

impl SceneType for BatAndBallScene
{
    type SpawnableIdType = SpawnId;
    
    fn new() -> Self where Self : Sized { BatAndBallScene {} }
    fn thinkers(&mut self, _game : &mut GameControl) -> Vec<Box<dyn ThinkerSystem<Self>>>
    {
        vec!
        [
            // Thinkers go here
        ]
    }
    fn drawers(&mut self, _game : &mut GameControl) -> Vec<Box<dyn DrawerSystem>>
    {
        vec!
        [
            // Drawers go here
        ]
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
        game : &mut GameControl, x : f64, y : f64, args : &[SpawnArg])
    {
        // Spawn logic goes here
    }
}
```

Let's fill in the `thinkers()`, `drawers()`, and `spawn()`, one by one. Let's start with `thinkers()`. For now we will have just one Thinker, which will be the system that sets up our game and handles basic game flow, which we will define later. For now, let's replace the placeholder `vec![]` in `thinkers()` with this:

```rust
vec!
[
    Box::new(GameThinker {}),
]
```

We will do similar for `drawers()`, using the `SpriteDrawer` we defined in the previous step:

```rust
vec!
[
    Box::new(SpriteDrawer::new())
]
```
