+++
title = "Basic Components"
weight = 2
+++

Before we go any further, it would be a good idea to define some common Components that will be shared between all the objects in our game. We will need:

- A way to describe a physical object, including its location in coordinate space.
- A way to describe a sprite, which will give an appearance to all of these physical objects.

Within `src/` we will create a new file named `components.rs`. Here are the contents of that file:

```rust
use keeshond::scene::Component;

use keeshond_datapack::DataId;

#[derive(Clone)]
pub struct Physical
{
    pub x : f64,
    pub y : f64,
    pub w : f64,
    pub h : f64
}

impl Component for Physical {}

pub struct Sprite
{
    pub scale_x : f32,
    pub scale_y : f32,
    pub angle : f32,
    pub sheet_id : DataId,
    pub sheet_x : f32,
    pub sheet_y : f32,
    pub sheet_w : f32,
    pub sheet_h : f32,
}

impl Component for Sprite {}

```

Here we define two Components, `Physical` and `Sprite`. The `Physical` component gives an object an X and Y coordinate, as well as a width and height for bounding box collisions. To keep things simple, we will assume that the bounding box originates from the center of the object (that is, the edges of the box extend in either direction equally).

Note the `#[derive(Clone)]` above the `pub struct Physical`. This is important as it allows us to easily `clone()` the Component. Make sure you include it!

The `Sprite` component is largely taken from Chapter 6. The difference this time is that we now no longer store the X and Y positions of sprites within this component, but in `Physical` instead. With the use of Multi-Component Systems we can get the coordinates out of the `Physical` to pair with the same entity's `Sprite`.

Now let's make another new file in `src/` named `systems.rs`:

```rust
use keeshond::scene::{ComponentControl, DrawerSystem};
use keeshond::renderer::{DrawSpriteInfo, DrawControl, DrawTransform};

use crate::components::{Physical, Sprite};

pub struct SpriteDrawer {}

impl SpriteDrawer
{
    pub fn new() -> SpriteDrawer { SpriteDrawer {} }
}

impl DrawerSystem for SpriteDrawer
{
    fn draw(&self,
        components : &ComponentControl,
        drawing : &mut Box<dyn DrawControl>,
        transform : &DrawTransform,
        _interpolation : f32)
    {
        let sprite_store = components.store::<Sprite>();
        let physical_store = components.store::<Physical>();
        
        for (entity, sprite) in sprite_store.iter()
        {
            if let Some(physical) = physical_store.get_entity(&entity)
            {
                let mut transform = transform.clone();
                transform.translate(physical.x as f32, physical.y as f32);
                transform.rotate(sprite.angle);
                transform.scale(sprite.scale_x, sprite.scale_y);
                
                let info = DrawSpriteInfo
                {
                    sheet_id : sprite.sheet_id,
                    shader_id : 0,
                    transform,
                    transform_offset_x : -0.5,
                    transform_offset_y : -0.5,
                    texture_x : sprite.sheet_x,
                    texture_y : sprite.sheet_y,
                    texture_w : sprite.sheet_w,
                    texture_h : sprite.sheet_h,
                    r : 1.0,
                    g : 1.0,
                    b : 1.0,
                    alpha : 1.0
                };
                
                drawing.draw_sprite(&info);
            }
        }
    }
}
```

Again, this code is largely taken from Chapter 6 - refer back if you're feeling lost. This time however, we now combine `Physical` and `Sprite`. In fact, `Physical` is a requirement for any `Sprite` to be drawn. If the entity does not have both of these Components assigned, the `Sprite` will effectively do nothing.

You may notice that we cast the `Physical` X and Y coordinates to `f32`. This is because we use `f64` in our game logic for enhanced precision, whereas the graphics renderer needs to work with `f32`.

Finally, we need an addition near the top of `main.rs` to tell the Rust compiler about our new submodules:

```rust
mod components;
mod systems;
```

Your project should be able to compile at this stage, but you will still have a black screen. Don't worry - we'll make some stuff show up during the next step.
