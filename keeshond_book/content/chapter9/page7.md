+++
title = "Gameplay Part 3"
weight = 7
+++

Now it's time for object-to-object collisions. We will do this by iterating over every candidate object we wish to be able to collide with. This is a simple way to handle object collisions for a small game, but keep in mind that it doesn't scale well - if you have a large number of objects you should use spatial indexing or tilemap lookups instead. There are plenty of resources online, as well as libraries (such as [ncollide](https://crates.io/crates/ncollide2d)). Having said that, let's continue.

We need to introduce a couple new functions to `Physical` to detect object collisions horizontally and vertically:

```rust
pub fn overlaps(&self, other : &Physical) -> bool
{
    self.left() < other.right() && self.right() > other.left()
        && self.top() < other.bottom() && self.bottom() > other.top()
}

pub fn collide_horizontal(&mut self, other : &Physical, vel_x : f64) -> bool
{
    if self.overlaps(other)
    {
        if vel_x > 0.0
        {
            self.check_right(other.left());
        }
        else if vel_x < 0.0
        {
            self.check_left(other.right());
        }
        
        return true;
    }
    
    false
}

pub fn collide_vertical(&mut self, other : &Physical, vel_y : f64) -> bool
{
    if self.overlaps(other)
    {
        if vel_y > 0.0
        {
            self.check_bottom(other.top());
        }
        else if vel_y < 0.0
        {
            self.check_top(other.bottom());
        }
        
        return true;
    }
    
    false
}
```

Now let's fill in some `TODO`s.

`// TODO: X brick collision`

Here we check to see if the ball hits a brick. If there's a horizontal collision, we rebound the ball by reversing its X velocity and then remove the brick from the game.

```rust
for (brick_entity, _brick) in brick_store.iter_mut()
{
    if let Some(brick_physical) = physical_store.get_entity_mut(&brick_entity)
    {
        if physical.collide_horizontal(&brick_physical, ball.vel_x)
        {
            scene.remove_entity_later(&brick_entity);
            ball.vel_x = -old_vel_x;
        }
    }
}
```

`// TODO: Y brick collision`

Similar to the code for the horizontal collision, this time we are doing a vertical collision check, and reversing the ball's Y velocity instead of X velocity.

```rust
for (brick_entity, _brick) in brick_store.iter_mut()
{
    if let Some(brick_physical) = physical_store.get_entity_mut(&brick_entity)
    {
        if physical.collide_vertical(&brick_physical, ball.vel_y)
        {
            scene.remove_entity_later(&brick_entity);
            ball.vel_y = -old_vel_y;
        }
    }
}
```

`// TODO: bat collision`

Here we check to see if the ball has hit our paddle. We rebound by reversing the Y coordinate, and set a new X velocity based on where the ball hits the paddle. If the ball hits further to the left side, it will be sent further leftward. Vice versa for hitting further to the right side.

```rust
for (bat_entity, _bat) in bat_store.iter_mut()
{
    if let Some(bat_physical) = physical_store.get_entity_mut(&bat_entity)
    {
        if ball.vel_y > 0.0 && physical.collide_vertical(&bat_physical, ball.vel_y)
        {
            ball.vel_x = (physical.x - bat_physical.x) / 8.0;
            ball.vel_y = -old_vel_y;
        }
    }
}
```

Finally, let's add a win condition. We want to check if all the bricks have been destroyed. Let's fill out `think()` for `GameThinker`, the same thinker responsible for setting up the game:

```rust
let brick_store = components.store_mut::<Brick>();

if brick_store.count() == 0
{
    let my_scene = Box::new(Scene::<BatAndBallScene>::new());
    
    game.goto_constructed_scene(my_scene);
}
```

So now we have all our components set up, but there are two important things we must not forget to do, or the game won't actually respond to our inputs! First, we must define the input bindings. In `main.rs`, put the following after the creationg of the gameloop:

```rust
let input = gameloop.control_mut().input_mut();

input.add_bind("left", "left");
input.add_bind("right", "right");
input.add_bind("start", "space");
input.add_bind("start", "z");
input.add_bind("start", "x");

std::mem::drop(input);
```

The other thing we must do is add the new thinkers to the SceneType - otherwise our new logic won't run at all! Let's revisit `scenes.rs` and change the `use crate::systems::` line to the following:

```rust
use crate::systems::{ SpriteDrawer, GameThinker, BatThinker, BallThinker};
```

And for `fn thinkers()`, we'll change the list of thinkers:

```rust
vec!
[
    Box::new(BatThinker {}),
    Box::new(BallThinker::new()),
    Box::new(GameThinker {}),
]
```

Now you should be able to compile and have a working game!
