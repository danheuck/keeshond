+++
title = "Introduction and Template"
weight = 1
+++

During the course of the tutorial we will be turning this:

![Before](bab_start.png)

Into this:

![Before](bab_finished.png)

# [Download the tutorial files](keeshond_bab_tutorial.zip)

The archive for this tutorial includes the data files as well as the project template, finished result, and intermediate steps.

Go ahead and copy the contents of the `start` folder into a new directory of your choosing. You'll find the following:

- A `data` folder containing two `sheets` and three `sounds`
- A `main.rs` file
- A `Cargo.toml` file

Let's start by looking in the `Cargo.toml`. First, the `[package]` section:

```toml
[package]
name = "keeshond_bab_tutorial"
version = "0.1.0"
edition = "2018"
```

Standard fare. After that you will notice the following `profile` sections:

```toml
[profile.dev]
opt-level = 2

[profile.release]
opt-level = 3
lto = true
```

This instructs the Rust compiler to apply some optimizations to the resulting executable even when in debug mode. This is important, as without these optimizations, a heavy game will run very slow, and even simple things like loading individual sheets can take several seconds. If you need more debugging information, feel free to set `opt-level` to 1 or 0. You'll probably want to change it back after you're done.

Now we have the `[dependencies]` section:

```toml
[dependencies]
keeshond = "0.11.0"
keeshond_datapack = "0.5.1"
strum = "0.15.0"
strum_macros = "0.15.0"
```

In any new project you'll be including `keeshond` and `keeshond_datapack` at a minimum. Use of the `strum` crates is also highly recommended as we will use them to help us construct our Scene. You'll learn more in Chapter 9.3.

# [Download the tutorial files](keeshond_bab_tutorial.zip)
