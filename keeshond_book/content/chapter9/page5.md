+++
title = "Gameplay Part 1"
weight = 5
+++

It's time to make our objects actually do things. We'll add some more stuff to our `Physical` component and then define new components for our `Bat`, `Ball`, and `Brick`.

Let's start with `Physical`. This struct already contains information on the object's bounding box. In this step we'll introduce some functions for handling collisions. Some say that logic does not belong in components and should only be in systems. I disagree. If it's useful functionality, it doesn't bloat, and it's useful to reuse, I say go ahead and give components functions.

For this game we'll use a fairly simple method of moving our objects and detecting collisions - first we move horizontally and check horizontal collisions, and then we move vertically and check vertical collisions. The big advantage to this approach is that by independently checking each axis, we can reliably differentiate between horizontal and vertical collisions, and we will never have to handle the case of an object colliding diagonally with the corner of another. The big disadvantage is that this approach only really makes sense for box-shaped collisions, but that's all we need for this game anyway.

In the future I would like to have this collision handling in a separate companion library for easy use in new projects. For now we'll just define the code inside the game itself. In `components.rs`, insert the following below the `struct Physical` block:

```rust
impl Physical
{
    pub fn left(&self) -> f64 { self.x - (self.w / 2.0) }
    pub fn right(&self) -> f64 { self.x + (self.w / 2.0) }
    pub fn top(&self) -> f64 { self.y - (self.h / 2.0) }
    pub fn bottom(&self) -> f64 { self.y + (self.h / 2.0) }
    
    pub fn check_left(&mut self, other_x : f64) -> bool
    {
        let edge = other_x + (self.w / 2.0);
        
        if self.x < edge
        {
            self.x = edge;
            return true;
        }
        
        false
    }
    pub fn check_right(&mut self, other_x : f64) -> bool
    {
        let edge = other_x - (self.w / 2.0);
        
        if self.x > edge
        {
            self.x = edge;
            return true;
        }
        
        false
    }
    pub fn check_top(&mut self, other_y : f64) -> bool
    {
        let edge = other_y + (self.h / 2.0);
        
        if self.y < edge
        {
            self.y = edge;
            return true;
        }
        
        false
    }
    pub fn check_bottom(&mut self, other_y : f64) -> bool
    {
        let edge = other_y - (self.h / 2.0);
        
        if self.y > edge
        {
            self.y = edge;
            return true;
        }
        
        false
    }
}
```

This code allows us to do two things. First, we can get the location of any of the box edges (left, right, top, bottom edges). Second, the `check_` series of functions lets us check if any of those edges is past another edge (for example, the screen edge), and reposition the object at the contact point if so. We also return a `bool` from the `check_` functions so we can react to the collision however we like.

While we're at it, let's also define `Bat`, `Ball`, and `Brick`:

```rust
pub struct Bat {}

impl Bat
{
    pub fn new() -> Bat { Bat {} }
}

impl Component for Bat {}

pub struct Ball
{
    pub vel_x : f64,
    pub vel_y : f64
}

impl Ball
{
    pub fn new() -> Ball
    {
        Ball
        {
            vel_x : 6.0,
            vel_y : -6.0
        }
    }
}

impl Component for Ball {}

pub struct Brick {}

impl Brick
{
    pub fn new() -> Brick { Brick {} }
}

impl Component for Brick {}
```

The `Bat` and `Brick` components are unique in that they have no data. Their sole purpose is to tag an object such to say, "this object is a Bat" or "this object is a Brick". We do introduce data members to `Ball` however: the X and Y velocities. Since the `Ball` is the only object in the game that needs to store state information regarding its velocity, we define `vel_x` and `vel_y` here instead of in a more general-use component. Let's go and use these new components when spawning objects. Jump back to `scenes.rs` and amend the `use crate::components::` line:

```rust
use crate::components::{Physical, Sprite, Bat, Ball, Brick};
```

Under `SpawnId::Bat`, add the new component after the `Sprite` component:

```rust
spawn.with(Bat::new());
```

And do similar with `SpawnId::Ball` and `SpawnId::Brick`:

```rust
spawn.with(Ball::new());
```

```rust
spawn.with(Brick::new());
```
