+++
title = "Adding Sound"
weight = 8
+++

Our gameplay is now complete but it could use some sprucing up. We'll finish things up by adding some sound effects.

All changes will be made within `systems.rs`.

First, we'll introduce another helper function. Much like `get_sheet()`, this function will retrieve a sound resource:

```rust
fn get_sound(game : &mut GameControl, package : &str, name : &str) -> DataId
{
    game.res_mut().sounds_mut().get_id_mut(package, name).expect("Failed to load package")
}
```

Now, we will need a place to store the `DataId`s of sounds. We could use a method like we did with sheets where the Components are responsible for holding the `DataId`. To keep things simple however, we'll just store them within a System.

We will update the `BallThinker` struct to have data members for each sound:

```rust
pub struct BallThinker
{
    sound_bat : DataId,
    sound_rebound : DataId,
    sound_brick : DataId,
}

impl BallThinker
{
    pub fn new() -> BallThinker
    {
        BallThinker
        {
            sound_bat : 0,
            sound_rebound : 0,
            sound_brick : 0,
        }
    }
}
```

In addition, we will add some code to run at the start of the scene by implementing `start()`:

```rust
fn start(&mut self,
    _components : &mut ComponentControl,
    _scene : &mut SceneControl<BatAndBallScene>,
    game : &mut GameControl)
{
    self.sound_bat = get_sound(game, "batandball", "bat.ogg");
    self.sound_rebound = get_sound(game, "batandball", "rebound.ogg");
    self.sound_brick = get_sound(game, "batandball", "brick.ogg");
}
```

Now all we have to do is tell Keeshond when to play the sounds by issuing calls to `game.audio_mut().play_sound()`.

In the X and Y brick collision code:

```rust
if physical.collide_horizontal(&brick_physical, ball.vel_x)
{
    scene.remove_entity_later(&brick_entity);
    ball.vel_x = -old_vel_x;
    game.audio_mut().play_sound(self.sound_brick); // NEW
}
```
```rust
if physical.collide_vertical(&brick_physical, ball.vel_y)
{
    scene.remove_entity_later(&brick_entity);
    ball.vel_y = -old_vel_y;
    game.audio_mut().play_sound(self.sound_brick); // NEW
}
```

In the X and Y screen edge rebound code:

```rust
if physical.check_left(0.0) || physical.check_right(1280.0)
{
    ball.vel_x = -old_vel_x;
    game.audio_mut().play_sound(self.sound_rebound); // NEW
}
```
```rust
if physical.check_top(0.0)
{
    ball.vel_y = -old_vel_y;
    game.audio_mut().play_sound(self.sound_rebound); // NEW
}
```

And finally, in the code that handles collision with the bat:

```rust
if ball.vel_y > 0.0 && physical.collide_vertical(&bat_physical, ball.vel_y)
{
    ball.vel_x = (physical.x - bat_physical.x) / 8.0;
    ball.vel_y = -old_vel_y;
    game.audio_mut().play_sound(self.sound_bat); // NEW
}
```

With that, the game is finished! Congrats on getting this far!
