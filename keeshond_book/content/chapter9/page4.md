+++
title = "Setting the Scene Part 2"
weight = 4
+++

Now we need to define the appearance and components of our objects. In `spawn()` we will create a `match` statement that will look at which type of object we are spawning. Depending on the type, we create and assign different components. The basic structure looks like this:

```rust
match spawnable_id
{
    SpawnId::Background =>
    {
        
    },
    SpawnId::Bat =>
    {
        
    },
    SpawnId::Ball =>
    {
        
    },
    SpawnId::Brick =>
    {
        
    },
}
```

Let's focus on the `Bat` for this example. We need to create a `Physical` and a `Sprite`. Then we tell the `SpawnControl` that we want to use the components we just created:

```rust
let physical = Physical
{
    x, y, w : 192.0, h : 32.0
};

let sprite = Sprite
{
    scale_x : 192.0, scale_y : 32.0,
    angle : 0.0,
    sheet_id : get_sheet(game, "batandball", "objects.png"),
    sheet_x : 0.0625, sheet_y : 0.875,
    sheet_w : 0.375, sheet_h : 0.0625,
};

spawn.with(physical);
spawn.with(sprite);
```

You might notice that we are specifying the scale and Sheet coordinates of each `Sprite` manually. In the future you'll build and slice these Sheets using other methods, but for now we'll do this ourselves.

Here are the components for the other object types (don't forget to add the components using `spawn.with()`):

`Background`:

```rust
let physical = Physical
{
    x, y, w : 0.0, h : 0.0
};

let sprite = Sprite
{
    scale_x : 2048.0, scale_y : 2048.0,
    angle : 0.0,
    sheet_id : get_sheet(game, "batandball", "bg.png"),
    sheet_x : 0.0, sheet_y : 0.0,
    sheet_w : 1.0, sheet_h : 1.0,
};
```

`Ball`:

```rust
let physical = Physical
{
    x, y, w : 32.0, h : 32.0
};

let sprite = Sprite
{
    scale_x : 32.0, scale_y : 32.0,
    angle : 0.0,
    sheet_id : get_sheet(game, "batandball", "objects.png"),
    sheet_x : 0.0625, sheet_y : 0.75,
    sheet_w : 0.0625, sheet_h : 0.0625,
};
```

`Brick`:

```rust
let physical = Physical
{
    x, y, w : 128.0, h : 64.0
};

let sprite = Sprite
{
    scale_x : 128.0, scale_y : 64.0,
    angle : 0.0,
    sheet_id : get_sheet(game, "batandball", "objects.png"),
    sheet_x : 0.0625, sheet_y : 0.0625,
    sheet_w : 0.25, sheet_h : 0.125,
};
```

With all that out of the way, let's spawn our objects! Let's revisit `systems.rs` and create our `GameThinker` system:

```rust
pub struct GameThinker {}

impl ThinkerSystem<BatAndBallScene> for GameThinker
{
    fn start(&mut self, _components : &mut ComponentControl, scene : &mut SceneControl<BatAndBallScene>,
        game : &mut GameControl)
    {
        scene.spawn_later(SpawnId::Background, game, 640.0, 360.0, &[]);
        scene.spawn_later(SpawnId::Bat, game, 640.0, 672.0, &[]);
        
        for x in 0..10
        {
            for y in 0..4
            {
                scene.spawn_later(SpawnId::Brick, game, 64.0 +  x as f64 * 128.0, 96.0 +  y as f64 * 64.0, &[]);
            }
        }
    }
    
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<BatAndBallScene>,
        game : &mut GameControl)
    {
        
    }
}
```

Pay attention to `start()`. We begin by creating our `Background` and `Bat`. The `Background` is large and we just create it in the center of the screen. The `Bat` is placed near the bottom-center of the screen. Remember that our reference screen coordinates are 1280x720, so by halving these X and Y values we get the center coordinates.

We use a nested `for` loop to create our `Brick` objects. There's four rows of ten `Brick`s each. By changing this code, you can tweak how many spawn, and where.

Finally, let's make some changes to `main.rs`. We have a new `extern` and `mod`, and a few new `use` statements to add:

```rust
extern crate keeshond;
```

```rust
mod scenes;
```

```rust
use keeshond::scene::Scene;
use keeshond_datapack::source::FilesystemSource;

use crate::scenes::BatAndBallScene;
```

Two new things need to happen in our `main()` function for all this to work. First, once we create our gameloop, we need to tell Keeshond where our resources are stored. We'll be loading them from a folder on the filesystem named `data`:

```rust
let source_manager = gameloop.control_mut().source_manager();

let source = FilesystemSource::new("data");
source_manager.borrow_mut().add_source(Box::new(source));

std::mem::drop(source_manager);
```

Next, just before we run the gameloop we need to tell it which Scene to run. Let's create one of our new `SceneType`:

```rust
let my_scene = Box::new(Scene::<BatAndBallScene>::new());

gameloop.control_mut().goto_constructed_scene(my_scene);
```

If all is well, you should be able to compile the program and see some graphics onscreen! There's no movement or interactivity yet, but we'll add that in the next step!
