+++
title = "Gameplay Part 2"
weight = 6
+++

With our preparations mostly done, let's start writing a few new `ThinkerSystem`s. One will be used to drive the `Bat` while the other will handle the `Ball`. First we should update our existing `use` statements with the following:

```rust
use keeshond::gameloop::{GameControl};
use keeshond::scene::{Scene, SceneControl, ComponentControl, DrawerSystem, ThinkerSystem};
use keeshond::renderer::{DrawSpriteInfo, DrawControl, DrawTransform};

use keeshond_datapack::DataId;

use crate::components::{Physical, Sprite, Bat, Ball, Brick};
use crate::scenes::{BatAndBallScene, SpawnId};
```

This pulls in our new components, as well as `DataId` (for later). Let's write the `BatThinker`.

```rust
pub struct BatThinker {}

impl ThinkerSystem<BatAndBallScene> for BatThinker
{
    fn think(&mut self,
        components : &mut ComponentControl,
        scene : &mut SceneControl<BatAndBallScene>,
        game : &mut GameControl)
    {
        let mut bat_store = components.store_mut::<Bat>();
        let mut physical_store = components.store_mut::<Physical>();
        let ball_store = components.store_mut::<Ball>();
        
        // Code goes here
    }
}
```

Here is the skeleton of our new thinker. This thinker will be responsible for moving the bat left and right, and for spawning a new ball if one is not already in play. Thus we will need to get the `ComponentStore` for `Bat`, `Physical` (to move the bat), and `Ball` (to check if there's currently a ball). Let's fill in our `// Code goes here`:

```rust
for (entity, _bat) in bat_store.iter_mut()
{
    if let Some(physical) = physical_store.get_entity_mut(&entity)
    {
        if game.input().held("left")
        {
            physical.x -= 16.0;
        }
        if game.input().held("right")
        {
            physical.x += 16.0;
        }
        
        physical.check_left(0.0);
        physical.check_right(1280.0);
        
        if game.input().down_once("start") && ball_store.count() == 0
        {
            scene.spawn_later(SpawnId::Ball, game, physical.x, physical.y - 32.0);
        }
    }
}
```

Why are we running a for loop for a component that we'll only have one instance of? Well, you never really know if you might have multiple bats! So we just use a for loop for the sake of it.

We check if the player is holding left or right. If so, we move the bat accordingly, by changing its `Physical`. Next, we use the `Physical::check_left()` and `Physical::check_right()` functions to see if the bat has hit the edges of the screen. If so, we want to stop the bat. These functions handle that logic for us.

Now we want to do something when the user presses the button to launch the ball. We need to know how many balls are in play, so we use `ball_store.count() == 0` to check that there are zero instances of the `Ball` entity. If so, we spawn the `Ball`. Otherwise the button does nothing.

That's it for the `Bat` logic! Now comes the `Ball` code:

```rust
pub struct BallThinker {}

impl BallThinker
{
    pub fn new() -> BallThinker
    {
        BallThinker {}
    }
}

impl ThinkerSystem<BatAndBallScene> for BallThinker
{
    fn think(&mut self,
        components : &mut ComponentControl,
        scene : &mut SceneControl<BatAndBallScene>,
        game : &mut GameControl)
    {
        let mut bat_store = components.store_mut::<Bat>();
        let mut physical_store = components.store_mut::<Physical>();
        let mut ball_store = components.store_mut::<Ball>();
        let mut brick_store = components.store_mut::<Brick>();
        
        for (entity, ball) in ball_store.iter_mut()
        {
            if let Some(mut physical) = physical_store.clone_from_entity(&entity)
            {
                let old_vel_x = ball.vel_x;
                let old_vel_y = ball.vel_y;
                
                // Move and check X
                
                physical.x += ball.vel_x;
                
                // TODO: X brick collision
                
                if physical.check_left(0.0) || physical.check_right(1280.0)
                {
                    ball.vel_x = -old_vel_x;
                }
                
                // Move and check Y
                
                physical.y += ball.vel_y;
                
                // TODO: Y brick collision
                // TODO: bat collision
                
                if physical.check_top(0.0)
                {
                    ball.vel_y = -old_vel_y;
                }
                if physical.y > 756.0
                {
                    scene.remove_entity_later(&entity);
                }
                
                physical_store.set_entity(&entity, physical);
            }
        }
    }
}
```

For now we will just focus on having the ball move around, bounce off the edges of the screen, and fall off the bottom of the screen. As mentioned previously, we are handling X and Y movement in separate steps. We move the ball horizontally, then check if it hits something and react. Then we move the ball vertically and do the same. If the ball hits the left or right side of the screen, we reverse its X velocity. If the ball hits the top of the screen, we reverse its Y velocity. If it falls off the bottom, we remove it from play. The player will then be able to spawn a new ball to continue playing.
