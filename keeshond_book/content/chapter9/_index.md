+++
title = "Tutorial: Bat and Ball Game"
weight = 9
sort_by = "weight"
+++

Hopefully the material up until this point has helped you get to know game programming with Keeshond. In this chapter, we will apply this knowledge to create a simple "Bat and Ball" game, similar to "Breakout" and "Arkanoid". Keeshond is certainly capable of more complex games, but this tutorial will be a good starting point.
