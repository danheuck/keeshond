+++
title = "Initialization and Gameloop"
weight = 1
+++

In the previous chapter we created a new `Gameloop` object. We had to call `Gameloop::run()` in order for the program to run continuously. Otherwise, the program would just skip past our gameloop and to the end of the `main` function, ending the program. If `run()` is called instead, the program will loop inside of the `Gameloop` object - hence the name.

Since Keeshond is a framework, you will notice that a lot of flow control like this is handled in the background for you. Still, it helps to know what is going on behind the scenes. The gameloop code looks something like this:

```rust
while running
{
    // Read events (keypresses, window changes, etc.)
    
    // Run thinker logic
    // Run drawing logic
    // Wait for next frame
}
```

The first thing that happens every loop is a reading of every event currently in the queue. This includes events such as keypresses, mouse movement and presses, window focus/size changes, close requests, etc. Outside of internal SDL2 event handling, currently Keeshond mostly uses this to adjust the viewport after a resize (explained later in the Rendering chapter), handle common key shortcuts such as Alt + Enter to toggle fullscreen, and allow the user to close the program. At some point I would like to open this up to allow users to add their own handling of certain events.

Next is the thinker logic. This is where logic such as object movement, collisions, reactions, and other behavior occurs within the game. This logic may be run multiple times in one loop as needed in order to catch up to the target framerate, in case the graphics fall behind - this is explained later in the chapter.

After that, we process all graphics rendering. Systems draw whatever sprites and other graphics they would like to show up on screen in this step.

Finally, we wait until the next frame in cases where the current frame finished early. This helps keep the timing smooth by not performing any processing until it's needed. After that, we run the loop again, assuming that the user hasn't exited the program.

Generally, you don't interact with the `Gameloop` itself while it is running. Instead, you have a `GameControl` object which acts as a sort of "remote control" allowing you to access the various subsystems yourself.

## Other ways to initialize

The standard way to initialize Keeshond is to call `Gameloop::new()` and then call `Gameloop::run()` on the resulting object. This will do two things:

- Initialize the subsystems including creating a window, setting up a graphics renderer, initializing the audio engine, etc.
- Cause a `panic` with a friendly error message in case something goes wrong.

However, there are cases where you might not want these behaviors. For example, if you are integrating Keeshond into a larger program and don't want an initialization failure to panic, you may want to use this instead:

```rust
let mut gameloop_result = Gameloop::try_new();
```

This will instead return a `Result` object which you may check for errors yourself.

Sometimes you don't even want to initialize the multimedia subsystems, for example when you need to run tests, or are creating a CLI application (such as a replay verifier) and don't care about timing and just want everything to run as fast as possible. In this case, you should create a `GameControl` object instead of a `Gameloop`. This will create placeholder rendering and audio subsystems that do nothing, useful for running your application "headless". Such code might look like this:

```rust
let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
let mut my_scene = Scene::new();

my_scene.think(&mut dummy);
```

Don't worry about the `Scene` object for now. That will be covered in the next chapter.
