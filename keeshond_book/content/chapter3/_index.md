+++
title = "Keeshond Basics"
weight = 3
sort_by = "weight"
+++

Before we start using Keeshond proper, it is important to know a few key concepts regarding the engine that will come up a lot in different places. In this chapter you will learn about essentials such as the gameloop, IDs, and fixed timestep with synchronization and how it applies to Keeshond.
