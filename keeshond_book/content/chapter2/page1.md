+++
title = "Environment Setup"
weight = 1
+++

## Rust setup

If you do not already have Rust installed, I strongly recommend using `rustup`:

<https://rustup.rs/>

Keeshond does not currently use any nightly-only features, so feel free to use the stable version of Rust. Build has been tested with stable 1.35 and nightly 1.40. Although it is likely that earlier versions can also build, this is not guaranteed. When in doubt, update your stable version of Rust via rustup:

```
rustup update stable
```

### All platforms

Keeshond uses the SDL2 and OpenAL libraries, which require a bit of additional setup. Instructions vary by operating system, but SDL2 setup is officially documented at the following repository page:

<https://github.com/Rust-SDL2/rust-sdl2/blob/master/README.md#requirements>


### Windows

If you are on Windows I recommend choosing the MSVC toolchain over MinGW, as one or two external library dependencies may rely on it. The community edition of Visual Studio can be installed for free.

You may want to use the 32-bit platform + toolchain (i686-pc-windows-msvc).

You will need an OpenAL DLL within your working directory to be able to run programs produced with Keeshond. The official download for the OpenAL Soft DLL (soft_oal.dll) can be found [here](https://kcat.strangesoft.net/openal.html#download).

### Linux

Ensure that you have the dev versions of the packages for SDL2 and OpenAL installed. The names of these packages will vary based on distro.

## Running examples

While you can obtain the library to use in your programs via `cargo`, you won't receive the examples this way. Instead, clone the git repository in a separate directory:

```
git clone https://gitlab.com/cosmicchipsocket/keeshond.git
```

Next, change to the `keeshond` directory and run one of the examples:

```
cd keeshond
cargo run --example doggymark
```

or perhaps

```
cargo run --example audio
```

This is a great way to check that your environment is set up correctly!
