+++
title = "Creating a New Project"
weight = 2
+++

Let's start off by opening a terminal. Assuming you have `cargo` installed (it will be installed by `rustup`), run the following command:

```
cargo new --bin my_game
```

This will create a new directory, called `my_game`. Inside this directory is a `Cargo.toml` file. We will edit this to include `keeshond` as a dependency. Visit the crates.io page at <https://crates.io/crates/keeshond> and copy the Cargo.toml line for the latest package and paste it underneath the currently-empty `[dependencies]` section.

Now run

```
cargo run
```

And Keeshond should be automatically downloaded for you as your new project is built. Of course, even though you have the dependency now, it's not actually using Keeshond yet, so you will only see "Hello, world!" printed out to your console window. With a bit of code, we can change that.

Open up `main.rs`. First, we want to inform Rust that we are using the `keeshond` crate by putting this at the top of the file:

```rust
extern crate keeshond;
```

Next, we are going to be using a few structs from Keeshond's `gameloop` module to get everything set up. To make our lives easier, we will pull them into our namespace. Place these lines just below the `extern crate` (and above the `main` function):

```rust
use keeshond::gameloop::{Gameloop, GameInfo};
```

Now, we will add some code to `main`. Delete the `println` line (but only if you want to), and construct a `GameInfo` object:

```rust
let gameinfo = GameInfo
{
    package_name : "my_game",
    friendly_name : "Hello Keeshond!",
    base_width : 1280,
    base_height : 720,
    .. Default::default()
};
```

This struct contains some basic information about our game such as its name, the base resolution, whether we want texture filtering by default, and more. Not everything needs to be specified here - that's what the `.. Default::default()` bit is for - it tells Rust that the default values are fine to use for every field not mentioned.

You should probably make sure that `package_name` is equal to the name of your Rust project, as this value is used internally (for example, to have the default log print any messages from your program).

Next, we need to create the `Gameloop` object. This is essentially the "core" of Keeshond. Simply calling `new()` will initialize the whole engine for you. All you need is this bit of code:

```rust
let mut gameloop = Gameloop::new(gameinfo);
```

Now the engine is initialized, but unless we tell the gameloop to start running, the program will quit instantly. Here is the last line of code you need:

```rust
gameloop.run();
```

And that's it! Your `main.rs` should look something like this:

```rust
extern crate keeshond;

use keeshond::gameloop::{Gameloop, GameInfo};

fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "my_game",
        friendly_name : "Hello Keeshond!",
        base_width : 1280,
        base_height : 720,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    gameloop.run();
}
```

Now go back to your terminal and type

```
cargo run
```

And you should have a blank window titled "Hello Keeshond". Congratulations on your first ever Keeshond program! It's not much of course, but these next several chapters will teach you the concepts you need to know before the "Bat and Ball Game" tutorial where you'll make your second ever Keeshond program!
