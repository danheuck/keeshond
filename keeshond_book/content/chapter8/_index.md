+++
title = "Handling Player Input"
weight = 8
sort_by = "weight"
+++

Unless you are making a zero-player game, you probably want to have some form of user interaction. This involves figuring out what keys or buttons a user is pressing, or perhaps holding down, and turning those into actions. Keeshond's input system is inspired by the rebindable controls of classic PC games, where any given action can have one or more keys or buttons associated with it. This makes it easy to build games where players can use the controls that they want to use.

Currently the input system only supports key bindings but will soon open up to allow many more forms of input.
