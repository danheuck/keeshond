+++
title = "Input Binds"
weight = 1
+++

All input-related functionality is managed by the `Input` object. You can get at this object through the `GameControl`.

If you just want to read the input state:

```rust
let input = game.input();
```

Or if you want to change input bindings:

```rust
let mut input = game.input_mut();
```

In the input system, everything is an Action, which is simply a way of referring to some logical control. Examples of Actions might be "jump" or "restart_level". Every Action is identified by a string, and can have one or more bindings associated with it. During setup of your game, you might have code similar to the following:

```rust
let input = gameloop.control_mut().input_mut();

input.add_action("jump");
input.add_bind("jump", "space");
input.add_action("restart_level");
input.add_bind("restart_level", "r");
```

When assigning a bind, you need to provide a string for the name of the key or button you wish to use.

- Keyboard: A list of key names can be found [here, in the leftmost column titled "Key Name"](https://wiki.libsdl.org/SDL_Scancode).
- Gamepad: Coming soon
- Mouse: Coming soon

Actions can be checked in several different ways:

- `Input::down_once()` - True for one frame once a key/button is pressed down. If held, is false for subsequent frames.
- `Input::up_once()` - True for one frame once the key/button is released.
- `Input::held()` - True if the key/button is held down. This includes the frame where it was first pressed down.
- `Input::time_held()` - Returns the number of frames a key/button was held for.
- `Input::time_released()` - Returns the number of frames a key/button was released for.
