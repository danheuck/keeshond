+++
title = "Making Pixel Art Games"
weight = 5
+++

Keeshond has a couple great features that allow you to make some nice looking pixel or retro-style games. These are not enabled by default, but turning them on is easy:

```rust
let gameinfo = GameInfo
{
    package_name : "neoretro",
    friendly_name : "Keeshond Neo-Retro Example",
    base_width : 384,
    base_height : 224,
    viewport_mode : ViewportMode::Pixel,
    texture_filtering : false,
    default_zoom : 2,
    .. Default::default()
};
```

First, it is vital to set the `base_width` and `base_height` to the resolution we want to draw everything at. Here we use 384x224. This is a widescreen-friendly resolution that was used by the Capcom CPS arcade hardware (Street Fighter II) as well as... the Virtual Boy. The next important thing is to set the `viewport_mode`. By setting it to `ViewportMode::Pixel`, we tell the renderer that we want to render the game at the given resolution and then scale it up to fit the screen, prioritizing crispness.

We also set `texture_filtering` to false so we can get those chunky, unfiltered pixels. For windowed mode users, we have `default_zoom`. This will scale up the game's default window size so that it doesn't start out microscopic on today's displays, but the user is free to adjust the scale as well, or switch to or from fullscreen by pressing Alt + Enter. Just make sure the default is set to something that will fit on most users' displays!
