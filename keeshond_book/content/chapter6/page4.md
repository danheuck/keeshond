+++
title = "Resource Type: Shaders"
weight = 4
+++

Keeshond allows you to use your own GLSL fragment shaders to customize the appearance of your sprites. Shader resources go in the `shaders` directory inside each Package, and must have a `.glsl` file extension. This will not be a tutorial on how to write GLSL shaders - there are many other resources for that. Still, here is a quick sample.

This is the standard shader, for reference:

```
#version 140

uniform sampler2D t_texture;
in vec2 v_uv;
in vec3 v_color;
in float v_alpha;

out vec4 TargetScreen;

void main()
{
    vec4 color = texture(t_texture, v_uv);
    
    color = color * vec4(v_color, v_alpha);
    TargetScreen = color;
}
```

The `t_texture` variable provides access to the source texture used by the sprite. `v_uv` contains the UV coordinates (the section of the Sheet/texture to use). `v_color` and `v_alpha` include the RGBA coefficients passed in for the sprite. Write to `TargetScreen` to output your pixels.

Here is an example of a shader that draws the sprite with inverted colors:

```
#version 140

uniform sampler2D t_texture;
in vec2 v_uv;
in vec3 v_color;
in float v_alpha;

out vec4 TargetScreen;

void main()
{
    vec4 color = texture(t_texture, v_uv);
    
    color = vec4(1.0 - color.r, 1.0 - color.g, 1.0 - color.b, color.a)
        * vec4(v_color, v_alpha);
    TargetScreen = color;
}
```

Play around and see what you come up with!
