+++
title = "Creating a DrawerSystem"
weight = 3
+++

In the ECS chapter we briefly touched on DrawerSystems. These are where we write our drawing code after all game logic updates are processed. As a reminder, game state cannot be modified inside a DrawerSystem. If you need to move an object around, do it in a ThinkerSystem beforehand. DrawerSystems are purely meant for display purposes.

Here is an empty `DrawerSystem`:

```rust
struct SpriteDrawer {}

impl DrawerSystem for SpriteDrawer
{
    fn draw(&self,
        components : &ComponentControl,
        drawing : &mut Box<dyn DrawControl>,
        transform : &DrawTransform,
        interpolation : f32)
    {
        // Code goes here
    }
}
```

First, you may notice the familiar `ComponentControl`. Note that during drawing you can only read data, not modify it. Next is the `DrawControl`. This is what you use to perform all your drawing operations. Then there's the `DrawTransform` for the screen. You'll want to clone this into a new transform and then perform all your translate/rotate/scale operations on it. The `interpolation` value is there for rendering interpolation. It tells you how far between logic frames the current display frame is. You can ignore it if you're not using this feature.

Let's implement our `SpriteDrawer`. First, we'll make a new Component:

```rust
struct Sprite
{
    x : f32,
    y : f32,
    scale_x : f32,
    scale_y : f32,
    angle : f32,
    sheet_id : DataId,
    sheet_x : f32,
    sheet_y : f32,
    sheet_w : f32,
    sheet_h : f32,
}

impl Component for Sprite {}
```

The first several items are self-explanatory. Then there's the `sheet_id`. This is because we use a `DataId` to specify which Sheet to draw with. Next is the region from the Sheet that we want to use when drawing this sprite, given by `sheet_x`, `sheet_y`, `sheet_w`, and `sheet_h`. These are values ranging from 0.0 (top-left or no width/height) to 1.0 (bottom-right or full width/height). This is important so that we can use multiple sprites from any given Sheet.

Now let's implement the `DrawerSystem` itself.

```rust
let sprite_store = components.store::<Sprite>();

for (_, sprite) in sprite_store.iter()
{
    let mut transform = transform.clone();
    transform.translate(sprite.x, sprite.y);
    transform.rotate(sprite.angle);
    transform.scale(sprite.scale_x, sprite.scale_y);
    
    let info = DrawSpriteInfo
    {
        sheet_id : sprite.sheet_id,
        shader_id : 0,
        transform,
        transform_offset_x : -0.5,
        transform_offset_y : -0.5,
        texture_x : sprite.sheet_x,
        texture_y : sprite.sheet_y,
        texture_w : sprite.sheet_w,
        texture_h : sprite.sheet_h,
        r : 1.0,
        g : 1.0,
        b : 1.0,
        alpha : 1.0
    };
    
    drawing.draw_sprite(&info);
}
```

First we retrieve an immutable reference to the `ComponentStore` for `Sprite`s, unlike the mutable reference we would use for `ThinkerSystem`s. Then we iterate through every `Sprite` and draw them in order. We make a clone of the `RenderTransform` that is passed into this function, and then translate, rotate, and scale it, in that order specifically. Translate must come first so it's not affected by the rotation or scaling. Rotation comes second. If you were to swap rotation and scaling, you'd find that the rotation would have a sort of "rolling squashing" effect.

Now, we need to inform the renderer about the sprite we want to draw. We have already touched on the `sheet_id`, the `transform`, and the sheet coordinates. But there are a few extra parameters here as well. The `shader_id` is a `DataId` pointing to the GLSL shader we wish to use for this sprite. For now we just want to use the standard shader, so we pass 0. 

Then there is the `transform_offset_x` and `transform_offset_y`. By default, all transformations are relative to the top-left corner of the sprite. Imagine putting this sprite on a corkboard using a pushpin. If you were to place the pushpin in the top-left (0, 0), and then rotate the sprite, it would appear to swing around the pin. In most cases we just want to center it, so we move the image left and up (-0.5, -0.5) relative to the pin so that the pin is centered. Now when we rotate the sprite, it stays centered in place.

Finally, there's `r`, `g`, `b`, and `alpha`. These parameters can be used to perform basic coloration of the sprite, or to make the sprite semitransparent. They can also be used as generic parameters if you are using a custom shader.

It is important to keep in mind how big of a sprite you want to draw. By default, the sprite transform will have a scale of 1.0. That's just a single pixel! Tiny! Be sure to scale it up using the pixel width and height of the source sprite!
