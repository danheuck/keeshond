+++
title = "Rendering Graphics"
weight = 6
sort_by = "weight"
+++

Graphics programming can be a complex task. A lot of work needs to be done just to display a simple sprite on-screen. This makes it a significant barrier for anyone looking to get started programming a game. With Keeshond however, much of this work is done for you behind the scenes, including automatic sprite batching. All you have to do is tell the renderer what to draw where and when.

Keeshond currently supports rendering via OpenGL 3, with a Vulkan renderer planned for a future release.
