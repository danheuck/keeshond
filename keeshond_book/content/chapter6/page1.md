+++
title = "Resource Type: Sheets"
weight = 1
+++

The first, and perhaps most important type of resource you'll work with is the Sheet.

A Sheet is essentially a texture that is loaded into graphics memory. Whenever you want to draw a sprite, you specify which Sheet you want to use. It is possible (and strongly recommended) to have multiple sprites on a single Sheet. Drawing multiple sprites in a row with the same Sheet is much faster than using a different Sheet every time you draw, so you should fit as many related graphics as you can within the same Sheet. Leave one or two pixels of transparent space between each sprite, or else the edges of sprites may bleed into each other.

Sheets are located within the `sheets` folder inside each Package. Files need to use the PNG (Portable Network Graphics) format, and use the `.png` file extension or they will not be picked up. Additionally, every Sheet should ideally be square in size, with the width and height being a power of two. For example, 512x512, 1024x1024, and 2048x2048 are all good sizes for Sheets. Keep in mind that there is a limit to how big textures can be, and this varies across graphics hardware. Many GPUs have a size limit of 4096x4096 or even 2048x2048, so you should decide on a maximum size and stick to it.
