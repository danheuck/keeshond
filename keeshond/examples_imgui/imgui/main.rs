extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{SimpleScene, ComponentControl, ImGuiSystem, NullSceneType, SceneControl};

use imgui;
use imgui::im_str;

pub struct MyGui
{
    demo_open : bool,
}

impl MyGui
{
    pub fn new() -> MyGui { MyGui { demo_open : true } }
}

impl ImGuiSystem<NullSceneType> for MyGui
{
    fn imgui_think(&mut self, ui : &mut imgui::Ui, _components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        imgui::Window::new(im_str!("ImGui Example"))
            .size([320.0, 200.0], imgui::Condition::FirstUseEver)
            .build(ui, ||
        {
            ui.text(im_str!("Hello Keeshond!"));
            ui.text_wrapped(im_str!("Keeshond is a 2D game engine with a focus on quickly bringing ideas onscreen."));
            
            ui.spacing();
            ui.separator();
            ui.spacing();
            
            ui.checkbox(im_str!("Show imgui demo window"), &mut self.demo_open);
        });
        
        if self.demo_open
        {
            ui.show_demo_window(&mut self.demo_open);
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "imgui",
        friendly_name : "Keeshond ImGui Example",
        base_width : 1280,
        base_height : 720,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let mut my_scene = Box::new(SimpleScene::new());
    
    my_scene.set_imgui_system(Box::new(MyGui::new()));
    
    gameloop.control_mut().goto_constructed_scene(my_scene);
    gameloop.run();
}
