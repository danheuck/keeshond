extern crate keeshond;
extern crate rand;

use keeshond::audio::{Voice, VoiceInfo};
use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{SimpleScene, Component, SceneControl, NullSceneType, ComponentControl, ThinkerSystem, ImGuiSystem};

use keeshond_datapack::DataId;
use keeshond_datapack::source::FilesystemSource;

use imgui;
use imgui::im_str;

pub struct VoiceControl
{
    new_volume : f32,
    new_pitch : f32,
    new_position : (f32, f32),
    new_distance : f32,
    new_rolloff_start : f32,
    new_local : bool,
    new_looping : bool,
    play_sound : bool,
    update_sound : Option<usize>,
    stop_sound : Option<usize>,
    unit_scale : f32,
    listener_position : (f32, f32),
    voice_generations : Vec<u64>,
    voice_info : Vec<Option<VoiceInfo>>,
    sound_id : DataId
}

impl VoiceControl
{
    pub fn new(control : &mut GameControl) -> VoiceControl
    {
        let mut voice_generations = Vec::new();
        let mut voice_info = Vec::new();
        
        for _ in 0..control.audio().max_voices()
        {
            voice_generations.push(0);
            voice_info.push(None);
        }
        
        VoiceControl
        {
            new_volume : 1.0,
            new_pitch : 1.0,
            new_position : (0.0, 0.0),
            new_distance : 1.0,
            new_rolloff_start : 0.0,
            new_local : false,
            new_looping : false,
            play_sound : false,
            update_sound : None,
            stop_sound : None,
            unit_scale : 1.0,
            listener_position : (0.0, 0.0),
            voice_info,
            voice_generations,
            sound_id : control.res_mut().sounds_mut().get_id_mut("audio_example", "ping_mono.wav")
                .expect("Failed to load package")
        }
    }
}

impl Component for VoiceControl {}

pub struct VoiceSystem {}

impl VoiceSystem
{
    pub fn new() -> VoiceSystem
    {
        VoiceSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for VoiceSystem
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, game : &mut GameControl)
    {
        let mut voice_control_store = components.store_mut::<VoiceControl>();
        let (_, voice_control) = voice_control_store.iter_mut().next().unwrap();
        
        game.audio_mut().set_unit_scale(voice_control.unit_scale);
        
        let (listener_x, listener_y) = voice_control.listener_position;
        game.audio_mut().set_listener_position(listener_x, listener_y);
        
        let voice_info = VoiceInfo
        {
            volume : voice_control.new_volume,
            pitch : voice_control.new_pitch,
            position : voice_control.new_position,
            distance : voice_control.new_distance,
            rolloff_start : voice_control.new_rolloff_start,
            local : voice_control.new_local,
            looping : voice_control.new_looping,
            .. Default::default()
        };
        
        if voice_control.play_sound
        {
            if let Some(voice) = game.audio_mut().play_sound_with(voice_control.sound_id, &voice_info)
            {
                voice_control.voice_generations[voice.slot] = voice.generation;
            }
            
            voice_control.play_sound = false;
        }
        
        if let Some(slot) = voice_control.update_sound
        {
            let voice = Voice
            {
                slot,
                generation : voice_control.voice_generations[slot]
            };
            
            game.audio_mut().set_voice(&voice, &voice_info);
            voice_control.update_sound = None;
        }
        
        if let Some(slot) = voice_control.stop_sound
        {
            let voice = Voice
            {
                slot,
                generation : voice_control.voice_generations[slot]
            };
            
            game.audio_mut().stop_voice(&voice);
            voice_control.stop_sound = None;
        }
        
        for i in 0..game.audio().max_voices()
        {
            let voice = Voice
            {
                slot : i,
                generation : voice_control.voice_generations[i]
            };
            
            voice_control.voice_info[i] = game.audio().voice(&voice);
        }
    }
}

pub struct MyGui {}

impl MyGui
{
    pub fn new() -> MyGui { MyGui {} }
}

fn clamp<T>(value : &mut T, min_value : T, max_value : T)
    where T : PartialOrd
{
    if *value < min_value
    {
        *value = min_value;
    }
    else if *value > max_value
    {
        *value = max_value;
    }
}

impl ImGuiSystem<NullSceneType> for MyGui
{
    fn imgui_think(&mut self, ui : &mut imgui::Ui, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let mut voice_control_store = components.store_mut::<VoiceControl>();
        let (_, voice_control) = voice_control_store.iter_mut().next().unwrap();
        
        imgui::Window::new(im_str!("Audio Voice Example"))
            .collapsible(false)
            .always_auto_resize(true)
            .build(ui, ||
        {
            ui.text(im_str!("Voice options"));
            ui.spacing();
            
            let push = ui.push_item_width(150.0);
            ui.input_float(im_str!("Volume"), &mut voice_control.new_volume).step(0.05).build();
            ui.input_float(im_str!("Pitch"), &mut voice_control.new_pitch).step(0.05).build();
            
            let (mut pos_x, mut pos_y) = voice_control.new_position;
            
            ui.input_float(im_str!("X"), &mut pos_x).step(0.1).build();
            ui.same_line(190.0);
            ui.input_float(im_str!("Y"), &mut pos_y).step(0.1).build();
            
            voice_control.new_position = (pos_x, pos_y);
            
            ui.input_float(im_str!("Distance"), &mut voice_control.new_distance).step(0.05).build();
            ui.input_float(im_str!("Rolloff start"), &mut voice_control.new_rolloff_start).step(0.05).build();
            push.pop(ui);
            
            ui.checkbox(im_str!("Local Position"), &mut voice_control.new_local);
            
            ui.checkbox(im_str!("Looping"), &mut voice_control.new_looping);
            
            clamp(&mut voice_control.new_volume, 0.0, 1.0);
            clamp(&mut voice_control.new_pitch, 0.05, 10.0);
            
            if ui.button(im_str!("Play new voice"), [120.0, 30.0])
            {
                voice_control.play_sound = true;
            }
            
            ui.spacing();
            ui.separator();
            ui.spacing();
            
            ui.text(im_str!("Listener options (applied immediately)"));
            ui.spacing();
            
            let (mut listener_x, mut listener_y) = voice_control.listener_position;
            
            let push = ui.push_item_width(150.0);
            ui.input_float(im_str!("Position unit scale"), &mut voice_control.unit_scale).step(1.0).build();
            ui.input_float(im_str!("Listener X"), &mut listener_x).step(0.1).build();
            ui.input_float(im_str!("Listener Y"), &mut listener_y).step(0.1).build();
            push.pop(ui);
            
            voice_control.listener_position = (listener_x, listener_y);
            
            clamp(&mut voice_control.unit_scale, 1.0, 999999.0);
        });
        
        imgui::Window::new(im_str!("Playing Voices"))
            .position([440.0, 60.0], imgui::Condition::FirstUseEver)
            .collapsible(false)
            .always_auto_resize(true)
            .build(ui, ||
        {
            let mut any_voices = false;
            
            for (slot, voice_option) in voice_control.voice_info.iter().enumerate()
            {
                if let Some(voice) = voice_option
                {
                    let mut extras = String::new();
                    
                    if voice.local
                    {
                        extras.push_str(", local");
                    }
                    if voice.looping
                    {
                        extras.push_str(", loop");
                    }
                    
                    ui.text(im_str!("Voice {} - vol = {:.3}, pitch = {:.3}, pos = {:.3}, {:.3}{}",
                        slot, voice.volume, voice.pitch, voice.position.0, voice.position.1, extras));
                    
                    let [info_x, _] = ui.item_rect_size();
                    
                    ui.same_line(info_x + 50.0);
                    if ui.small_button(&im_str!("Update voice {}", slot))
                    {
                        voice_control.update_sound = Some(slot);
                    }
                    
                    let [update_x, _] = ui.item_rect_size();
                    
                    ui.same_line(info_x + update_x + 60.0);
                    if ui.small_button(&im_str!("Stop {}", slot))
                    {
                        voice_control.stop_sound = Some(slot);
                    }
                    
                    any_voices = true;
                }
            }
            
            if !any_voices
            {
                ui.text(im_str!("(Nothing currently playing)"));
            }
        });
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "voice",
        friendly_name : "Keeshond Audio Voice Example",
        base_width : 1280,
        base_height : 720,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    gameloop.control_mut().res_mut().sounds_mut().load_package("audio_example").expect("Failed to load audio");
    
    let mut my_scene = Box::new(SimpleScene::new());
    
    let new_entity = my_scene.add_entity();
    let voice_control = VoiceControl::new(gameloop.control_mut());

    my_scene.register_component_type::<VoiceControl>();
    my_scene.add_component(&new_entity, voice_control);
    
    my_scene.add_thinker_system(Box::new(VoiceSystem::new()));
    my_scene.set_imgui_system(Box::new(MyGui::new()));
    
    gameloop.control_mut().goto_constructed_scene(my_scene);
    gameloop.run();
}
