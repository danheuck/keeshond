
extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{SimpleScene, ComponentControl, DrawerSystem};
use keeshond::renderer::{DrawBackgroundColorInfo, DrawControl, DrawTransform};

use keeshond_datapack::DataId;
use keeshond_datapack::source::FilesystemSource;

struct ShaderDrawer
{
    shader_id : DataId
}

impl ShaderDrawer
{
    fn new(control : &mut GameControl) -> ShaderDrawer
    {
        ShaderDrawer
        {
            shader_id : control.res_mut().shaders_mut().get_id_mut("shader_example", "gradient.glsl")
                .expect("Failed to load package"),
        }
    }
}

impl DrawerSystem for ShaderDrawer
{
    fn draw(&self, _components : &ComponentControl, drawing : &mut Box<dyn DrawControl>, _transform : &DrawTransform, _interpolation : f32)
    {
        let info = DrawBackgroundColorInfo
        {
            shader_id : self.shader_id,
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        };
        
        drawing.draw_background_color(&info);
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "shader",
        friendly_name : "Keeshond Shader Example",
        base_width : 1280,
        base_height : 720,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let mut my_scene = Box::new(SimpleScene::new());
    
    my_scene.add_drawer_system(Box::new(ShaderDrawer::new(gameloop.control_mut())));
    
    gameloop.control_mut().goto_constructed_scene(my_scene);
    gameloop.run();
}
