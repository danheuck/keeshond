
extern crate keeshond;
extern crate rand;
extern crate strum;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{Scene, SceneControl, Component, ComponentControl, DrawerSystem, ThinkerSystem, SceneType, SpawnControl, SpawnArg};
use keeshond::renderer::{DrawSpriteInfo, DrawControl, DrawTransform, SliceId};

use keeshond_datapack::DataId;
use keeshond_datapack::source::FilesystemSource;

use rand::Rng;
use strum_macros::EnumString;

const DOGGY_SCALE : f64 = 32.0;
const DOGGY_ANGLE_MAX : f32 = 0.2;
const DOGGY_GRAVITY : f64 = 0.8;
const DOGGY_HSPEED_MIN : f64 = 4.0;
const DOGGY_HSPEED_MAX : f64 = 10.0;
const DOGGY_VSPEED_MAX : f64 = 2.0;
const DOGGY_BOUNCE_MIN : f64 = 25.0;
const DOGGY_BOUNCE_MAX : f64 = 34.0;
const DOGGY_SLICE_MAX : SliceId = 3;


////////////////////////////////////////


#[derive(EnumString)]
enum DoggySpawnId
{
    Doggy
}

struct DoggyScene
{
    
}

impl SceneType for DoggyScene
{
    type SpawnableIdType = DoggySpawnId;
    
    fn new() -> Self where Self : Sized { DoggyScene {} }
    fn thinkers(&mut self, _game : &mut GameControl) -> Vec<Box<dyn ThinkerSystem<Self>>>
    {
        vec!
        [
            Box::new(DoggyThinker {})
        ]
    }
    fn drawers(&mut self, game : &mut GameControl) -> Vec<Box<dyn DrawerSystem>>
    {
        vec!
        [
            Box::new(DoggyDrawer::new(game))
        ]
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : DoggySpawnId,
        _game : &mut GameControl, _x : f64, _y : f64, _args : &[SpawnArg])
    {
        match spawnable_id
        {
            DoggySpawnId::Doggy =>
            {
                spawn.with(Doggy::new());
            }
        }
    }
}


////////////////////////////////////////


struct Doggy
{
    x : f64,
    y : f64,
    vel_x : f64,
    vel_y : f64,
    scale_x : f32,
    scale_y : f32,
    angle : f32,
    slice : SliceId
}

impl Doggy
{
    pub fn new() -> Doggy
    {
        let mut rng = rand::thread_rng();
        let mut scale_xy = 1.0;
        let mut angle = 0.0;
        let mut slice = 0;
        
        if rng.gen_bool(0.5)
        {
            scale_xy = rng.gen_range(0.8, 1.2) * scale_xy;
            angle = rng.gen_range(-DOGGY_ANGLE_MAX, DOGGY_ANGLE_MAX);
        }

        if rng.gen_bool(0.5)
        {
            slice = rng.gen_range(0, DOGGY_SLICE_MAX);
        }
        
        Doggy
        {
            x : 16.0,
            y : 16.0,
            vel_x : rng.gen_range(DOGGY_HSPEED_MIN, DOGGY_HSPEED_MAX),
            vel_y : rng.gen_range(0.0, DOGGY_VSPEED_MAX),
            scale_x : scale_xy,
            scale_y : scale_xy,
            angle,
            slice
        }
    }
    
    #[inline(always)]
    pub fn get_render_position(&self, interpolation : f32) -> (f32, f32)
    {
        (self.x as f32 + self.vel_x as f32 * interpolation, self.y as f32 + self.vel_y as f32 * interpolation)
    }
}

impl Component for Doggy
{
    fn maintain_ordering() -> bool where Self : Sized { true }
}

struct DoggyThinker {}

impl ThinkerSystem<DoggyScene> for DoggyThinker
{
    fn start(&mut self, _components : &mut ComponentControl, scene : &mut SceneControl<DoggyScene>, game : &mut GameControl)
    {
        for _ in 0..2
        {
            scene.spawn_later(DoggySpawnId::Doggy, game, 0.0, 0.0, &[]);
        }
    }
    
    fn think(&mut self, components : &mut ComponentControl, scene : &mut SceneControl<DoggyScene>, game : &mut GameControl)
    {
        let mut rng = rand::thread_rng();
        let mut doggy_store = components.store_mut::<Doggy>();
        
        let (base_width, base_height) = game.renderer().base_size();
        
        let left_side = DOGGY_SCALE / 2.0;
        let right_side = base_width as f64 - left_side;
        let bottom_side = base_height as f64 - left_side;
        
        for (_, doggy) in doggy_store.iter_mut()
        {
            doggy.vel_y += DOGGY_GRAVITY;
            doggy.x += doggy.vel_x;
            doggy.y += doggy.vel_y;
            
            if doggy.x < left_side && doggy.vel_x < 0.0
            {
                doggy.x = left_side;
                doggy.vel_x = rng.gen_range(DOGGY_HSPEED_MIN, DOGGY_HSPEED_MAX);
            }
            else if doggy.x > right_side && doggy.vel_x > 0.0
            {
                doggy.x = right_side;
                doggy.vel_x = rng.gen_range(-DOGGY_HSPEED_MAX, -DOGGY_HSPEED_MIN);
            }
            
            if doggy.y > bottom_side
            {
                doggy.y = bottom_side;
                doggy.vel_y = rng.gen_range(-DOGGY_BOUNCE_MAX, -DOGGY_BOUNCE_MIN);
            }
        }

        if game.input().held("spawn_faster")
        {
            for _ in 0..1000
            {
                scene.spawn_later(DoggySpawnId::Doggy, game, 0.0, 0.0, &[]);
            }
        }
        else if game.input().held("spawn") || game.input().down_once("spawn_once")
        {
            for _ in 0..100
            {
                scene.spawn_later(DoggySpawnId::Doggy, game, 0.0, 0.0, &[]);
            }
        }
        if game.input().held("despawn")
        {
            let mut iter = doggy_store.iter_mut();
            
            for _ in 0..100
            {
                if let Some((entity, _)) = iter.next()
                {
                    scene.remove_entity_later(&entity);
                }
            }
        }
        
        if game.input().down_once("reset")
        {
            let new_scene = Box::new(Scene::<DoggyScene>::new());
            game.goto_constructed_scene(new_scene);
        }
    }
}

struct DoggyDrawer
{
    doggy_sheet : DataId
}

impl DoggyDrawer
{
    fn new(control : &mut GameControl) -> DoggyDrawer
    {
        DoggyDrawer
        {
            doggy_sheet : control.res_mut().sheets_mut().get_id_mut("doggymark", "doggy.png")
                .expect("Failed to load package"),
        }
    }
}

impl DrawerSystem for DoggyDrawer
{
    fn draw(&self, components : &ComponentControl, drawing : &mut Box<dyn DrawControl>, transform : &DrawTransform, interpolation : f32)
    {
        let doggy_store = components.store::<Doggy>();
        let iter = doggy_store.iter();
        
        for (_, doggy) in iter
        {
            let (x, y) = doggy.get_render_position(interpolation);
            let transform = transform.clone();
            
            drawing.draw_sprite(&DrawSpriteInfo
            {
                sheet_id: self.doggy_sheet,
                shader_id: 0,
                x,
                y,
                angle: doggy.angle,
                scale_x: doggy.scale_x,
                scale_y: doggy.scale_y,
                slice: doggy.slice,
                transform,
                r: 1.0,
                g: 1.0,
                b: 1.0,
                alpha: 1.0
            });
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "doggymark",
        friendly_name : "Keeshond Doggymark",
        base_width : 1280,
        base_height : 720,
        texture_filtering : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    gameloop.control_mut().renderer_mut().set_window_title("Keeshond Doggymark | Space to spawn doggos! | S - Spawn slowly | Delete - Despawn | R - Reset");
    
    let mut rng = rand::thread_rng();
    
    // Make first few numbers more random
    for _ in 0..100
    {
        rng.gen::<f64>();
    }
    
    let input = gameloop.control_mut().input_mut();
    
    input.add_action("spawn");
    input.add_bind("spawn", "space");
    input.add_action("spawn_faster");
    input.add_bind("spawn_faster", "f");
    input.add_action("spawn_once");
    input.add_bind("spawn_once", "s");
    input.add_action("despawn");
    input.add_bind("despawn", "delete");
    input.add_action("reset");
    input.add_bind("reset", "r");
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let my_scene = Box::new(Scene::<DoggyScene>::new());
    
    gameloop.control_mut().goto_constructed_scene(my_scene);
    
    gameloop.run();
}
