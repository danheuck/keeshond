
extern crate keeshond;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{SimpleScene, SceneControl, NullSceneType, Component, ComponentControl, ThinkerSystem, DrawerSystem};
use keeshond::renderer::{DrawBackgroundColorInfo, DrawSpriteRawInfo, DrawControl, DrawTransform};

use keeshond_datapack::DataId;
use keeshond_datapack::source::FilesystemSource;

const LOGO_WIDTH : f32 = 512.0;
const LOGO_HEIGHT : f32 = 256.0;
const LOGO_HITBOX_WIDTH : f64 = 214.0;
const LOGO_HITBOX_HEIGHT : f64 = 66.0;

struct BgDrawer
{
    r : f32,
    g : f32,
    b : f32
}

impl BgDrawer
{
    fn new() -> BgDrawer
    {
        BgDrawer
        {
            r : 1.0,
            g : 1.0,
            b : 1.0
        }
    }
}

impl DrawerSystem for BgDrawer
{
    fn draw(&self, _components : &ComponentControl, drawing : &mut Box<dyn DrawControl>, _transform : &DrawTransform, _interpolation : f32)
    {
        let info = DrawBackgroundColorInfo
        {
            shader_id : 0,
            r : self.r,
            g : self.g,
            b : self.b,
            alpha : 1.0
        };
        
        drawing.draw_background_color(&info);
    }
}


struct Logo
{
    x : f64,
    y : f64,
    vel_x : f64,
    vel_y : f64
}

impl Logo
{
    pub fn new() -> Logo
    {
        Logo
        {
            x : LOGO_HITBOX_WIDTH,
            y : LOGO_HITBOX_HEIGHT,
            vel_x : 4.0,
            vel_y : 4.0
        }
    }
    
    pub fn get_render_position(&self, interpolation : f32) -> (f32, f32)
    {
        (self.x as f32 + self.vel_x as f32 * interpolation, self.y as f32 + self.vel_y as f32 * interpolation)
    }
}

impl Component for Logo {}

struct LogoThinker
{
    sound1 : DataId,
    sound2 : DataId
}

impl LogoThinker
{
    fn new(control : &mut GameControl) -> LogoThinker
    {
        LogoThinker
        {
            sound1 : control.res_mut().sounds_mut().get_id_mut("audio_example", "ping.wav")
                .expect("Failed to load package"),
            sound2 : control.res_mut().sounds_mut().get_id_mut("audio_example", "pong.wav")
                .expect("Failed to load package"),
        }
    }
}

impl ThinkerSystem<NullSceneType> for LogoThinker
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, game : &mut GameControl)
    {
        let mut logo_store = components.store_mut::<Logo>();
        let iter = logo_store.iter_mut();
        
        for (_, logo) in iter
        {
            logo.x += logo.vel_x;
            logo.y += logo.vel_y;
            
            if (logo.x < LOGO_HITBOX_WIDTH && logo.vel_x < 0.0)
                || (logo.x > 1280.0 - LOGO_HITBOX_WIDTH && logo.vel_x > 0.0)
            {
                logo.vel_x = 0.0 - logo.vel_x;
                game.audio_mut().play_sound(self.sound1);
            }
            
            if (logo.y < LOGO_HITBOX_HEIGHT && logo.vel_y < 0.0)
                || (logo.y > 720.0 - LOGO_HITBOX_HEIGHT && logo.vel_y > 0.0)
            {
                logo.vel_y = 0.0 - logo.vel_y;
                game.audio_mut().play_sound(self.sound2);
            }
        }
    }
}

struct LogoDrawer
{
    sheet : DataId
}

impl LogoDrawer
{
    fn new(control : &mut GameControl) -> LogoDrawer
    {
        LogoDrawer
        {
            sheet : control.res_mut().sheets_mut().get_id_mut("common", "objects.png")
                .expect("Failed to load package"),
        }
    }
}

impl DrawerSystem for LogoDrawer
{
    fn draw(&self, components : &ComponentControl, drawing : &mut Box<dyn DrawControl>, transform : &DrawTransform, interpolation : f32)
    {
        let logo_store = components.store::<Logo>();
        let iter = logo_store.iter();
        
        for (_, logo) in iter
        {
            let (x, y) = logo.get_render_position(interpolation);
            let mut transform = transform.clone();
            transform.translate(x as f32, y as f32);
            transform.scale(LOGO_WIDTH, LOGO_HEIGHT);
            
            let info = DrawSpriteRawInfo
            {
                sheet_id : self.sheet,
                shader_id : 0,
                transform,
                transform_offset_x : -0.5,
                transform_offset_y : -0.5,
                texture_x : 0.0,
                texture_y : 0.0,
                texture_w : 1.0,
                texture_h : 0.5,
                r : 1.0,
                g : 1.0,
                b : 1.0,
                alpha : 1.0
            };
            
            drawing.draw_sprite_raw(&info);
        }
    }
}

////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "audio",
        friendly_name : "Keeshond Audio Example",
        base_width : 1280,
        base_height : 720,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data");
    source_manager.borrow_mut().add_source(Box::new(source));
    
    gameloop.control_mut().res_mut().sounds_mut().load_package("audio_example").expect("Failed to load audio");
    
    let mut my_scene = Box::new(SimpleScene::new());
    
    let new_entity = my_scene.add_entity();
    let logo = Logo::new();

    my_scene.register_component_type::<Logo>();
    my_scene.add_component(&new_entity, logo);
    
    my_scene.add_drawer_system(Box::new(BgDrawer::new()));
    my_scene.add_thinker_system(Box::new(LogoThinker::new(gameloop.control_mut())));
    my_scene.add_drawer_system(Box::new(LogoDrawer::new(gameloop.control_mut())));
    
    gameloop.control_mut().goto_constructed_scene(my_scene);
    gameloop.run();
}
