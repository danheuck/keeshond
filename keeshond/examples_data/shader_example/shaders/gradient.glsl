#version 140

in vec2 v_uv;
in vec3 v_color;
in float v_alpha;

out vec4 TargetScreen;

void main()
{
    vec4 color = vec4(v_uv.x, v_uv.y, 1.0, 1.0);
    TargetScreen = color;
}
