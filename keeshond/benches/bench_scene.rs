#![feature(test)]

extern crate test;
extern crate keeshond;

use test::Bencher;
use rand::seq::SliceRandom;

use keeshond::gameloop::{GameInfo, GameControl};
use keeshond::scene::{BaseScene, SimpleScene, SceneControl, NullSceneType, Entity, Component, ComponentControl, ThinkerSystem};

const NUM_ITER : usize = 10_000;

struct Position
{
    x : f64,
    y : f64
}

impl Position
{
    pub fn new() -> Position
    {
        Position
        {
            x : 0.0,
            y : 0.0
        }
    }
}

impl Component for Position
{
    fn maintain_ordering() -> bool where Self : Sized { false }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

struct Velocity
{
    vel_x : f64,
    vel_y : f64
}

impl Velocity
{
    pub fn new() -> Velocity
    {
        Velocity
        {
            vel_x : 0.1,
            vel_y : 0.2
        }
    }
}

impl Component for Velocity
{
    fn maintain_ordering() -> bool where Self : Sized { false }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

struct PositionVelocity
{
    x : f64,
    y : f64,
    vel_x : f64,
    vel_y : f64
}

impl PositionVelocity
{
    pub fn new() -> PositionVelocity
    {
        PositionVelocity
        {
            x : 0.0,
            y : 0.0,
            vel_x : 0.1,
            vel_y : 0.2
        }
    }
}

impl Component for PositionVelocity
{
    fn maintain_ordering() -> bool where Self : Sized { false }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

struct PositionOrdered
{
    x : f64,
    y : f64
}


impl PositionOrdered
{
    pub fn new() -> PositionOrdered
    {
        PositionOrdered
        {
            x : 0.0,
            y : 0.0
        }
    }
}

impl Component for PositionOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}

struct VelocityOrdered
{
    vel_x : f64,
    vel_y : f64
}

impl VelocityOrdered
{
    pub fn new() -> VelocityOrdered
    {
        VelocityOrdered
        {
            vel_x : 0.1,
            vel_y : 0.2
        }
    }
}

impl Component for VelocityOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
    fn capacity_hint() -> usize where Self : Sized { NUM_ITER }
}


////////////////////////////////////////


struct PosVelThinker {}

impl ThinkerSystem<NullSceneType> for PosVelThinker
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let mut pos_store = components.store_mut::<Position>();
        let vel_store = components.store_mut::<Velocity>();
        
        let iter = pos_store.iter_mut();
    
        for (entity, pos) in iter
        {
            if let Some(vel) = vel_store.get_entity(&entity)
            {
                pos.x += vel.vel_x;
                pos.y += vel.vel_y;
            }
        }
    }
}

fn make_scene_multi_component() -> SimpleScene
{
    let mut my_scene = SimpleScene::new();
    
    for _ in 0..NUM_ITER
    {
        let new_entity = my_scene.add_entity();
        
        my_scene.add_component(&new_entity, Position::new());
        my_scene.add_component(&new_entity, Velocity::new());
    }
    
    my_scene.add_thinker_system(Box::new(PosVelThinker {}));
    
    my_scene
}

struct PosVelSingleThinker {}

impl ThinkerSystem<NullSceneType> for PosVelSingleThinker
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let mut posvel_store = components.store_mut::<PositionVelocity>();
        
        let iter = posvel_store.iter_mut();
    
        for (_, component) in iter
        {
            component.x += component.vel_x;
            component.y += component.vel_y;
        }
    }
}

fn make_scene_single_component() -> SimpleScene
{
    let mut my_scene = SimpleScene::new();
    
    for _ in 0..NUM_ITER
    {
        let new_entity = my_scene.add_entity();
        
        my_scene.add_component(&new_entity, PositionVelocity::new());
    }
    
    my_scene.add_thinker_system(Box::new(PosVelSingleThinker {}));
    
    my_scene
}

struct PosVelOrderedThinker {}

impl ThinkerSystem<NullSceneType> for PosVelOrderedThinker
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let mut pos_store = components.store_mut::<PositionOrdered>();
        let vel_store = components.store_mut::<VelocityOrdered>();
        
        let iter = pos_store.iter_mut();
    
        for (entity, pos) in iter
        {
            if let Some(vel) = vel_store.get_entity(&entity)
            {
                pos.x += vel.vel_x;
                pos.y += vel.vel_y;
            }
        }
    }
}

fn make_scene_removal() -> SimpleScene
{
    let mut my_scene = SimpleScene::new();
    
    my_scene.add_thinker_system(Box::new(PosVelThinker {}));
    
    for _ in 0..NUM_ITER
    {
        let new_entity = my_scene.add_entity();
        
        my_scene.add_component(&new_entity, Position::new());
        my_scene.add_component(&new_entity, Velocity::new());
    }
    
    my_scene
}

fn make_scene_removal_ordered() -> SimpleScene
{
    let mut my_scene = SimpleScene::new();
    
    my_scene.add_thinker_system(Box::new(PosVelOrderedThinker {}));
    
    for _ in 0..NUM_ITER
    {
        let new_entity = my_scene.add_entity();
        
        my_scene.add_component(&new_entity, PositionOrdered::new());
        my_scene.add_component(&new_entity, VelocityOrdered::new());
    }
    
    my_scene
}


////////////////////////////////////////


#[bench]
fn bench_build_multi_component(bencher : &mut Bencher)
{
    bencher.iter(|| make_scene_multi_component());
}

#[bench]
fn bench_update_multi_component(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
    let mut scene = make_scene_multi_component();
    
    bencher.iter(|| scene.think(&mut dummy));
}

#[bench]
fn bench_build_single_component(bencher : &mut Bencher)
{
    bencher.iter(|| make_scene_single_component());
}

#[bench]
fn bench_update_single_component(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
    let mut scene = make_scene_single_component();
    
    bencher.iter(|| scene.think(&mut dummy));
}

#[bench]
fn bench_update_empty_slots(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
    let mut scene = make_scene_single_component();
    
    for i in 1..NUM_ITER - 2
    {
        assert!(scene.remove_entity(&Entity::new(i, 1)));
    }
    
    bencher.iter(|| scene.think(&mut dummy));
}

#[bench]
fn bench_build_no_component(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut my_scene = Box::new(SimpleScene::new());
    
        for _ in 0..NUM_ITER
        {
            my_scene.add_entity();
        }
    });
}

#[bench]
fn bench_build_rebuild_2_cycles(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut my_scene = Box::new(SimpleScene::new());
        
        my_scene.add_thinker_system(Box::new(PosVelThinker {}));
        
        for gen in 1..3
        {
            for _ in 0..NUM_ITER
            {
                let new_entity = my_scene.add_entity();
                
                my_scene.add_component(&new_entity, Position::new());
                my_scene.add_component(&new_entity, Velocity::new());
            }
            
            for i in 0..NUM_ITER
            {
                assert!(my_scene.remove_entity(&Entity::new(i, gen)));
            }
        }
    });
}

#[bench]
fn bench_build_delete_all(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
        let mut my_scene = make_scene_removal();
        
        for i in 0..NUM_ITER
        {
            assert!(my_scene.remove_entity(&Entity::new(i, 1)));
        }
        
        // Force flushing of the component removal
        my_scene.think(&mut dummy);
    });
}

#[bench]
fn bench_build_delete_all_ordered(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
        let mut my_scene = make_scene_removal_ordered();
        
        for i in 0..NUM_ITER
        {
            assert!(my_scene.remove_entity(&Entity::new(i, 1)));
        }
        
        // Force flushing of the component removal
        my_scene.think(&mut dummy);
    });
}

#[bench]
fn bench_build_delete_gradual(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
        let mut my_scene = make_scene_removal();
        let rng = &mut rand::thread_rng();
        
        let mut ids : Vec<usize> = (0..NUM_ITER).collect();
        ids.shuffle(rng);
        
        while !ids.is_empty()
        {
            for _ in 0..(NUM_ITER / 10) + 1
            {
                if let Some(id) = ids.pop()
                {
                    assert!(my_scene.remove_entity(&Entity::new(id, 1)));
                }
            }
            
            // Force flushing of the component removal
            my_scene.think(&mut dummy);
        }
    });
}

#[bench]
fn bench_build_delete_gradual_ordered(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
        let mut my_scene = make_scene_removal_ordered();
        let rng = &mut rand::thread_rng();
        
        let mut ids : Vec<usize> = (0..NUM_ITER).collect();
        ids.shuffle(rng);
        
        while !ids.is_empty()
        {
            for _ in 0..(NUM_ITER / 10) + 1
            {
                if let Some(id) = ids.pop()
                {
                    assert!(my_scene.remove_entity(&Entity::new(id, 1)));
                }
            }
            
            // Force flushing of the component removal
            my_scene.think(&mut dummy);
        }
    });
}

#[bench]
fn bench_update_10000_systems_overhead(bencher : &mut Bencher)
{
    let mut dummy = GameControl::new(&GameInfo::default(), None).unwrap();
    let mut scene = Box::new(SimpleScene::new());
    
    let new_entity = scene.add_entity();
    scene.add_component(&new_entity, PositionVelocity::new());
    
    scene.add_thinker_system(Box::new(PosVelSingleThinker {}));
    
    bencher.iter(||
        for _ in 0..10_000
        {
            scene.think(&mut dummy);
        }
    );
}
