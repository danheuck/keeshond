#![feature(test)]

extern crate test;
extern crate keeshond;
extern crate cgmath;

use test::Bencher;
use keeshond::renderer::DrawTransform;

#[bench]
fn bench_transform_matrix(bencher : &mut Bencher)
{
    bencher.iter(||
    {
        for i in 0..100
        {
            for j in 0..1000
            {
                let (i_float, j_float) = (i as f32, j as f32);
                let mut transform = DrawTransform::identity();

                transform.translate(i_float, j_float);
                transform.rotate(i_float);
                transform.scale(i_float, j_float);

                test::black_box(transform);
             }
        }
    });
}
