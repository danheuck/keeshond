//! Rendering functionality

#[cfg(feature = "opengl")] pub mod opengl;
#[cfg(feature = "vulkan")] pub mod vulkan;

use cgmath::{SquareMatrix, Matrix3};

use std::collections::HashMap;
use std::io::Read;
use std::rc::Rc;
use std::cell::RefCell;

use serde::{Serialize, Deserialize};

use keeshond_datapack::{ReadSeek, DataId, DataObject, DataStore, DataError, PreparedStoreError};
use keeshond_datapack::source::Source;

#[cfg(feature = "imgui_base")] use imgui;

use crate::gameloop::GameResources;
use crate::scene::BaseScene;
#[cfg(feature = "opengl")] use crate::renderer::opengl::GlDrawControl;
#[cfg(feature = "vulkan")] use crate::renderer::vulkan::VulkanDrawControl;

#[derive(Copy, Clone, PartialEq)]
pub enum ViewportMode
{
    Independent,
    Pixel
}

/// A transformation matrix used for drawing operations
#[derive(Clone, Debug)]
pub struct DrawTransform
{
    mat : Matrix3<f32>
}

impl DrawTransform
{
    pub fn identity() -> DrawTransform
    {
        DrawTransform
        {
            mat : Matrix3::identity()
        }
    }
    
    pub fn from_array(array : &[f32; 9]) -> DrawTransform
    {
        DrawTransform
        {
            mat : Matrix3::new(array[0], array[1], array[2],
                               array[3], array[4], array[5],
                               array[6], array[7], array[8])
        }
    }
    
    #[inline(always)]
    pub fn translate(&mut self, x : f32, y : f32)
    {
        self.mat = self.mat * Matrix3::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, x, y, 1.0);
    }
    
    #[inline(always)]
    pub fn rotate(&mut self, angle : f32)
    {
        let (angle_sin, angle_cos) = angle.sin_cos();
        
        self.mat = self.mat * Matrix3::new(angle_cos, angle_sin, 0.0, -angle_sin, angle_cos, 0.0, 0.0, 0.0, 1.0);
    }
    
    #[inline(always)]
    pub fn scale(&mut self, scale_x : f32, scale_y : f32)
    {
        self.mat = self.mat * Matrix3::new(scale_x, 0.0, 0.0, 0.0, scale_y, 0.0, 0.0, 0.0, 1.0);
    }
    
    pub fn shear(&mut self, x : f32, y : f32)
    {
        self.mat = self.mat * Matrix3::new(1.0, y, 0.0, x, 1.0, 0.0, 0.0, 0.0, 1.0);
    }
}

#[derive(Copy, Clone, PartialEq)]
enum RendererOp
{
    #[allow(dead_code)]
    BackgroundColor,
    #[allow(dead_code)]
    Sprite
}

impl Default for RendererOp
{
    fn default() -> Self
    {
        RendererOp::BackgroundColor
    }
}

#[derive(Debug, Fail)]
pub enum RendererError
{
    #[fail(display = "Renderer init failed: {}", _0)]
    RendererInitFailed(String),
    #[fail(display = "Resource load failed: {}", _0)]
    LoadResourceFailed(String),
    #[cfg(feature = "imgui_base")]
    #[fail(display = "Imgui renderer failed: {}", _0)]
    ImguiRendererFailed(String)
}

/// Information on how to draw a background color
pub struct DrawBackgroundColorInfo
{
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
    pub shader_id : DataId
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SpriteSlice
{
    pub texture_x : f32,
    pub texture_y : f32,
    pub texture_w : f32,
    pub texture_h : f32,
    pub origin_x : f32,
    pub origin_y : f32,
}

pub type SliceId = usize;

/// Information on how to draw a sprite
pub struct DrawSpriteInfo
{
    pub sheet_id : DataId,
    pub shader_id : DataId,
    pub x : f32,
    pub y : f32,
    pub angle : f32,
    pub scale_x : f32,
    pub scale_y : f32,
    pub slice : SliceId,
    pub transform : DrawTransform,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32
}

/// Information on how to draw a sprite (raw form with just transform and texture coordinates)
pub struct DrawSpriteRawInfo
{
    pub sheet_id : DataId,
    pub shader_id : DataId,
    pub transform : DrawTransform,
    pub transform_offset_x : f32,
    pub transform_offset_y : f32,
    pub texture_x : f32,
    pub texture_y : f32,
    pub texture_w : f32,
    pub texture_h : f32,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32
}

/// (ADVANCED) Information on how to create a new [Renderer]
pub struct RendererNewInfo<'a>
{
    pub width : u32,
    pub height : u32,
    pub default_zoom : u32,
    pub texture_filtering : bool,
    pub fullscreen : bool,
    pub kiosk_mode : bool,
    pub viewport_mode : ViewportMode,
    pub video_subsystem : sdl2::VideoSubsystem,
    pub resources : &'a mut GameResources
}

/// A resource representing a texture sheet for drawing sprites from
pub struct Sheet
{
    width : u32,
    height : u32,
    image_raw : Vec<u8>,
    #[allow(dead_code)]
    force_filter : bool,
    #[allow(dead_code)]
    filtered : bool,
    #[allow(dead_code)]
    metadata : SheetMetadata
}

impl Sheet
{
    pub fn width(&self) -> u32
    {
        self.width
    }

    pub fn height(&self) -> u32
    {
        self.height
    }
}

impl DataObject for Sheet
{
    fn folder_name() -> &'static str where Self : Sized
    {
        "sheets"
    }
    fn want_file(pathname : &str) -> bool where Self : Sized
    {
        pathname.ends_with(".png")
    }
    fn from_io(reader: Box<dyn ReadSeek>, full_pathname : &str, source : &mut Box<dyn Source>) -> Result<Self, DataError> where Self : Sized
    {
        let decoder = png::Decoder::new(reader);
        let info_result = decoder.read_info();
        
        match info_result
        {
            Ok((info, mut info_reader)) =>
            {
                let mut image_raw = vec![0; info.buffer_size()];
                match info_reader.next_frame(&mut image_raw)
                {
                    Err(error) =>
                    {
                        return Err(DataError::BadData(format!("{}: {}", full_pathname, error)));
                    },
                    _ => {}
                }
                
                let filter = source.read_file(format!("{}.filtered", full_pathname).as_str()).is_ok();
                let unfilter = source.read_file(format!("{}.unfiltered", full_pathname).as_str()).is_ok();
                
                let force_filter = filter || unfilter;
                let filtered = !unfilter;

                let mut metadata = SheetMetadata { slice_names : HashMap::new(), slices : Vec::new() };

                let mut new_path = String::from(full_pathname);
                new_path.push_str(".json");

                if let Ok(mut json_read) = source.read_file(&new_path)
                {
                    let mut json_text = String::new();
                    let metadata_result = json_read.read_to_string(&mut json_text);

                    match metadata_result
                    {
                        Err(error) =>
                        {
                            return Err(DataError::BadData(format!("{}: {}", full_pathname, error)));
                        },
                        _ => {}
                    }

                    match serde_json::from_str(&json_text)
                    {
                        Ok(loaded_metadata) =>
                        {
                            metadata = loaded_metadata;
                        },
                        Err(error) =>
                        {
                            return Err(DataError::BadData(format!("{}: {}", full_pathname, error)));
                        }
                    }
                }
                
                return Ok(Sheet
                {
                    width : info.width as u32,
                    height : info.height as u32,
                    image_raw,
                    force_filter,
                    filtered,
                    metadata
                });
            },
            Err(png::DecodingError::IoError(io_error)) =>
            {
                return Err(DataError::IoError(io_error));
            },
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}: {}", full_pathname, error)));
            }
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SheetMetadata
{
    slice_names : HashMap<String, usize>,
    slices : Vec<SpriteSlice>
}

/// A resource representing a fragment shader that can be used when drawing
pub struct Shader
{
    #[allow(dead_code)]
    program_source : String
}

impl DataObject for Shader
{
    fn folder_name() -> &'static str where Self : Sized
    {
        "shaders"
    }
    fn want_file(pathname : &str) -> bool where Self : Sized
    {
        pathname.ends_with(".glsl")
    }
    fn from_io(mut reader: Box<dyn ReadSeek>, _full_pathname : &str, _source : &mut Box<dyn Source>) -> Result<Self, DataError> where Self : Sized
    {
        let mut program_source = String::new();
        match reader.read_to_string(&mut program_source)
        {
            Ok(_) =>
            {
                return Ok(Shader
                {
                    program_source
                });
            },
            Err(io_error) =>
            {
                return Err(DataError::IoError(io_error));
            }
        }
    }
}

trait DrawBackendEntryBase
{
    fn name(&self) -> &'static str;
    fn try_init(&self, renderer_new_info : &mut RendererNewInfo) -> DrawControlResult;
}

#[allow(dead_code)]
struct DrawBackendEntry<T : DrawControl>
{
    backend_name : &'static str,
    backend_type : std::marker::PhantomData<T>
}

impl<T : DrawControl> DrawBackendEntry<T>
{
    #[allow(dead_code)]
    fn new(name : &'static str) -> DrawBackendEntry<T>
    {
        DrawBackendEntry::<T>
        {
            backend_name : name,
            backend_type : std::marker::PhantomData
        }
    }
}

impl<T : DrawControl> DrawBackendEntryBase for DrawBackendEntry<T>
{
    fn name(&self) -> &'static str
    {
        self.backend_name
    }
    
    fn try_init(&self, renderer_new_info : &mut RendererNewInfo) -> DrawControlResult
    {
        T::new(renderer_new_info)
    }
}

pub type DrawControlResult = Result<(Box<dyn DrawControl>, Rc<RefCell<sdl2::video::Window>>), RendererError>;

/// Public interface to controlling rendering in your game
pub trait DrawControl
{
    fn new(renderer_new_info : &mut RendererNewInfo) -> DrawControlResult where Self : Sized;
    fn screen_transform(&self) -> &DrawTransform;
    fn sync_sheet_store(&mut self, sheet_store : &mut DataStore<Sheet>) -> Result<(), PreparedStoreError>;
    fn sync_shader_store(&mut self, shader_store : &mut DataStore<Shader>) -> Result<(), PreparedStoreError>;
    fn draw_sprite(&mut self, draw_info : &DrawSpriteInfo);
    fn draw_sprite_raw(&mut self, draw_info : &DrawSpriteRawInfo);
    fn draw_background_color(&mut self, draw_info : &DrawBackgroundColorInfo);
    #[cfg(feature = "imgui_base")]
    fn draw_imgui(&mut self, ui : imgui::Ui);
    fn recalculate_viewport(&mut self, base_width : f32, base_height : f32);
    fn start_drawing(&mut self);
    fn present(&mut self);
    fn flush_drawing(&mut self);
    #[cfg(feature = "imgui_base")]
    fn init_imgui_renderer(&mut self, imgui : &mut imgui::Context) -> Result<(), RendererError>;
}

/// (ADVANCED) Null rendering backend, for automatic tests that don't need sound
pub struct NullDrawControl
{
    transform : DrawTransform
}

impl NullDrawControl
{
    pub fn new_headless() -> NullDrawControl
    {
        NullDrawControl
        {
            transform : DrawTransform::identity()
        }
    }
}

impl DrawControl for NullDrawControl
{
    fn new(renderer_new_info : &mut RendererNewInfo) -> DrawControlResult where Self : Sized
    {
        let video_subsystem = &renderer_new_info.video_subsystem;
        let mut window_builder = video_subsystem.window("", renderer_new_info.width, renderer_new_info.height);
        
        window_builder.position_centered().resizable().allow_highdpi();
        
        let window_raw = try_or_else!(window_builder.build(),
            |error| Err(RendererError::RendererInitFailed(format!("Could not create game window: {}", error))));
        let window = Rc::new(RefCell::new(window_raw));
        
        Ok((Box::new(NullDrawControl
        {
            transform : DrawTransform::identity()
        }), window))
    }
    
    fn screen_transform(&self) -> &DrawTransform
    {
        &self.transform
    }
    
    #[allow(unused_variables)]
    fn sync_sheet_store(&mut self, sheet_store : &mut DataStore<Sheet>) -> Result<(), PreparedStoreError>
    {
        Ok(())
    }
    
    #[allow(unused_variables)]
    fn sync_shader_store(&mut self, shader_store : &mut DataStore<Shader>) -> Result<(), PreparedStoreError>
    {
        Ok(())
    }

    #[allow(unused_variables)]
    fn draw_sprite(&mut self, draw_info : &DrawSpriteInfo) {}
    #[allow(unused_variables)]
    fn draw_sprite_raw(&mut self, draw_info : &DrawSpriteRawInfo) {}
    #[allow(unused_variables)]
    fn draw_background_color(&mut self, draw_info : &DrawBackgroundColorInfo) {}
    #[cfg(feature = "imgui_base")]
    #[allow(unused_variables)]
    fn draw_imgui(&mut self, ui : imgui::Ui) {}
    #[allow(unused_variables)]
    fn recalculate_viewport(&mut self, base_width : f32, base_height : f32) {}
    #[allow(unused_variables)]
    fn start_drawing(&mut self) {}
    #[allow(unused_variables)]
    fn present(&mut self) {}
    #[allow(unused_variables)]
    fn flush_drawing(&mut self) {}
    #[cfg(feature = "imgui_base")]
    #[allow(unused_variables)]
    fn init_imgui_renderer(&mut self, imgui : &mut imgui::Context) -> Result<(), RendererError>
    {
        Ok(())
    }
}

/// Represents the renderer's game window
pub struct Renderer
{
    control : Box<dyn DrawControl>,
    window : Option<Rc<RefCell<sdl2::video::Window>>>,
    video_subsystem : Option<sdl2::VideoSubsystem>,
    base_width : f32,
    base_height : f32,
    kiosk_mode : bool
}

impl Renderer
{
    /// (ADVANCED) Creates a new instance. This is already created by the [crate::gameloop::GameControl]
    ///  so you don't need to create this yourself.
    pub fn new(renderer_new_info : Option<RendererNewInfo>) -> Result<Renderer, RendererError>
    {
        let mut control : Box<dyn DrawControl> = Box::new(NullDrawControl::new_headless());
        let mut window = None;
        let mut video_subsystem = None;
        let mut fullscreen = false;
        let mut kiosk_mode = false;
        
        if let Some(mut new_info) = renderer_new_info
        {
            new_info.default_zoom = new_info.default_zoom.max(1).min(8);
            
            #[allow(unused_mut)]
            let mut backend_list : Vec<Box<dyn DrawBackendEntryBase>> = Vec::new();
            
            #[cfg(feature = "vulkan")]
            backend_list.push(Box::new(DrawBackendEntry::<VulkanDrawControl>::new("vulkan")));
            #[cfg(feature = "opengl")]
            backend_list.push(Box::new(DrawBackendEntry::<GlDrawControl>::new("opengl")));
            
            let mut backend_error = Err(RendererError::RendererInitFailed("No renderers available".to_string()));
            
            for backend in backend_list
            {
                info!("Trying renderer backend \"{}\"...", backend.name());
                
                match backend.try_init(&mut new_info)
                {
                    Ok((out_control, out_window)) =>
                    {
                        control = out_control;
                        window = Some(out_window);
                        video_subsystem = Some(new_info.video_subsystem);
                        fullscreen = new_info.fullscreen;
                        kiosk_mode = new_info.kiosk_mode;
                        
                        info!("Using renderer backend \"{}\"", backend.name());
                        
                        break;
                    },
                    Err(error) =>
                    {
                        warn!("Renderer backend \"{}\" failed: {}", backend.name(), error);
                        backend_error = Err(error);
                    }
                }
            }
            
            if window.is_none()
            {
                return backend_error;
            }
        }
        
        let mut renderer = Renderer
        {
            control,
            window,
            video_subsystem,
            base_width : 100.0,
            base_height : 100.0,
            kiosk_mode
        };
        
        renderer.recalculate_viewport();
        renderer.set_fullscreen(fullscreen);
        
        Ok(renderer)
    }
    
    pub(crate) fn control(&mut self) -> &mut Box<dyn DrawControl>
    {
        &mut self.control
    }
    
    pub fn base_size(&self) -> (f32, f32)
    {
        (self.base_width, self.base_height)
    }
    
    pub fn set_base_size(&mut self, width : f32, height : f32)
    {
        self.base_width = width;
        self.base_height = height;
        
        self.recalculate_viewport();
    }
    
    pub fn window_size(&self) -> (u32, u32)
    {
        if let Some(window) = &self.window
        {
            return window.borrow().size();
        }
        
        (0, 0)
    }
    
    pub fn draw(&mut self, scene : &mut Box<dyn BaseScene>, resources : &mut GameResources, interpolation : f32)
    {
        self.control().sync_sheet_store(resources.sheets_mut())
        .expect("GPU sheet store sync failed");
        self.control().sync_shader_store(resources.shaders_mut())
            .expect("GPU shader store sync failed");

        self.control.start_drawing();
        let first_transform = self.control.screen_transform().clone();
        
        scene.draw(&mut self.control, &first_transform, interpolation);
    }
    
    pub fn present(&mut self)
    {
        self.control.present();
    }
    
    pub(crate) fn recalculate_viewport(&mut self)
    {
        self.control.recalculate_viewport(self.base_width, self.base_height);
    }
    
    pub fn window_title(&self) -> String
    {
        if let Some(window) = &self.window
        {
            let window_borrow = window.borrow();
            
            return window_borrow.title().to_string();
        }
        
        String::new()
    }
    
    pub fn set_window_title(&mut self, window_title : &str)
    {
        if let Some(window) = &mut self.window
        {
            let mut window_borrow = window.borrow_mut();
            
            if let Err(error) = window_borrow.set_title(window_title)
            {
                warn!("Window title error: {}", error);
            }
        }
    }
    
    pub fn fullscreen(&self) -> bool
    {
        if let Some(window) = &self.window
        {
            return window.borrow().fullscreen_state() == sdl2::video::FullscreenType::Off
        }
        
        false
    }
    
    pub fn set_fullscreen(&mut self, enabled : bool)
    {
        if let Some(window) = &mut self.window
        {
            let mut window_borrow = window.borrow_mut();
            
            if enabled || self.kiosk_mode
            {
                try_or_else!(window_borrow.set_fullscreen(sdl2::video::FullscreenType::Desktop),
                    |error| warn!("Could not set fullscreen mode: {}", error));
            }
            else
            {
                try_or_else!(window_borrow.set_fullscreen(sdl2::video::FullscreenType::Off),
                    |error| warn!("Could not set fullscreen mode: {}", error));
            }
        }
    }
    
    pub fn toggle_fullscreen(&mut self)
    {
        if let Some(window) = &mut self.window
        {
            let mut window_borrow = window.borrow_mut();
            
            if window_borrow.fullscreen_state() == sdl2::video::FullscreenType::Off || self.kiosk_mode
            {
                try_or_else!(window_borrow.set_fullscreen(sdl2::video::FullscreenType::Desktop),
                    |error| warn!("Could not set fullscreen mode: {}", error));
            }
            else
            {
                try_or_else!(window_borrow.set_fullscreen(sdl2::video::FullscreenType::Off),
                    |error| warn!("Could not set fullscreen mode: {}", error));
            }
        }
    }
    
    pub fn set_power_management_enabled(&mut self, enabled : bool)
    {
        if let Some(video_subsystem) = &mut self.video_subsystem
        {
            if enabled
            {
                video_subsystem.enable_screen_saver();
            }
            else
            {
                video_subsystem.disable_screen_saver();
            }
        }
    }
}
