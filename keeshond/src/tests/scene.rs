use crate::gameloop::{GameInfo, GameControl};
use crate::renderer::{DrawControl, DrawTransform, DrawBackgroundColorInfo};
use crate::scene::{BaseScene, SimpleScene, SceneControl, NullSceneType, ComponentControl, Entity, Component, ThinkerSystem, DrawerSystem};
use rand::seq::SliceRandom;

#[derive(Debug)]
struct FooComponent { runs : u32 }

impl FooComponent
{
    fn new() -> FooComponent
    {
        FooComponent { runs : 0 }
    }
}

impl Component for FooComponent {}

#[derive(Debug)]
struct BarComponent { runs : u32 }

impl BarComponent
{
    fn new() -> BarComponent
    {
        BarComponent { runs : 0 }
    }
}

impl Component for BarComponent {}

#[derive(Debug)]
struct FooComponentOrdered { runs : u32 }

impl FooComponentOrdered
{
    fn new() -> FooComponentOrdered
    {
        FooComponentOrdered { runs : 0 }
    }
}

impl Component for FooComponentOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
}

#[derive(Debug)]
struct BarComponentOrdered { runs : u32 }

impl BarComponentOrdered
{
    fn new() -> BarComponentOrdered
    {
        BarComponentOrdered { runs : 0 }
    }
}

impl Component for BarComponentOrdered
{
    fn maintain_ordering() -> bool where Self : Sized { true }
}

#[derive(Debug)]
struct FooSystem {}

impl FooSystem
{
    fn new() -> FooSystem
    {
        FooSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for FooSystem
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let mut foo_store = components.store_mut::<FooComponent>();
        let iter = foo_store.iter_mut();
    
        for (_, foo) in iter
        {
            foo.runs += 1;
        }
    }
}

#[derive(Debug)]
struct FooBarSystem {}

impl FooBarSystem
{
    fn new() -> FooBarSystem
    {
        FooBarSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for FooBarSystem
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let mut foo_store = components.store_mut::<FooComponent>();
        let mut bar_store = components.store_mut::<BarComponent>();
        
        let iter = foo_store.iter_mut();
    
        for (entity, foo) in iter
        {
            if let Some(bar) = bar_store.get_entity_mut(&entity)
            {
                foo.runs += 10;
                bar.runs += 10;
            }
        }
    }
}

#[derive(Debug)]
struct FooBarSystemOrdered {}

impl FooBarSystemOrdered
{
    fn new() -> FooBarSystemOrdered
    {
        FooBarSystemOrdered {}
    }
}

impl ThinkerSystem<NullSceneType> for FooBarSystemOrdered
{
    fn think(&mut self, components : &mut ComponentControl, _scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let mut foo_store = components.store_mut::<FooComponentOrdered>();
        let mut bar_store = components.store_mut::<BarComponentOrdered>();
        
        let iter = foo_store.iter_mut();
    
        for (entity, foo) in iter
        {
            if let Some(bar) = bar_store.get_entity_mut(&entity)
            {
                foo.runs += 10;
                bar.runs += 10;
            }
        }
    }
}

#[derive(Debug)]
struct BarAdderSystem {}

impl BarAdderSystem
{
    fn new() -> BarAdderSystem
    {
        BarAdderSystem {}
    }
}

impl ThinkerSystem<NullSceneType> for BarAdderSystem
{
    fn think(&mut self, components : &mut ComponentControl, scene : &mut SceneControl<NullSceneType>, _game : &mut GameControl)
    {
        let bar_store = components.store_mut::<BarComponent>();

        print!("{}", bar_store.count());

        let entity = scene.add_entity_later();
        scene.add_component_later::<BarComponent>(&entity, BarComponent::new());
    }
}

#[derive(Debug)]
struct DrawSystemGood {}

impl DrawSystemGood
{
    fn new() -> DrawSystemGood
    {
        DrawSystemGood {}
    }
}

impl DrawerSystem for DrawSystemGood
{
    fn draw(&self, components : &ComponentControl, drawing : &mut Box<dyn DrawControl>, _transform : &DrawTransform, _interpolation : f32)
    {
        let foo_store = components.store::<FooComponent>();
        
        for (_, foo) in foo_store.iter()
        {
            drawing.draw_background_color(&DrawBackgroundColorInfo
            {
                r : foo.runs as f32,
                g : 1.0,
                b : 1.0,
                alpha : 1.0,
                shader_id : 0
            });
        }
    }
}

#[derive(Debug)]
struct DrawSystemBad {}

impl DrawSystemBad
{
    fn new() -> DrawSystemBad
    {
        DrawSystemBad {}
    }
}

impl DrawerSystem for DrawSystemBad
{
    fn draw(&self, components : &ComponentControl, drawing : &mut Box<dyn DrawControl>, _transform : &DrawTransform, _interpolation : f32)
    {
        let mut foo_store = components.store_mut::<FooComponent>();
        
        for (_, foo) in foo_store.iter_mut()
        {
            foo.runs += 1;
            
            drawing.draw_background_color(&DrawBackgroundColorInfo
            {
                r : foo.runs as f32,
                g : 1.0,
                b : 1.0,
                alpha : 1.0,
                shader_id : 0
            });
        }
    }
}

#[test]
fn scene_empty()
{
    let mut scene = SimpleScene::new();
    
    // Check basic state
    assert_eq!(scene.entity_count(), 0);
    
    // Check empty state doesn't break when used for weird stuff
    scene.think(&mut GameControl::new(&GameInfo::default(), None).unwrap());
    
    let bad_handle = Entity::new(0, 0);
    
    assert_eq!(scene.remove_entity(&bad_handle), false);
    assert_eq!(scene.remove_component::<FooComponent>(&bad_handle), false);
    assert_eq!(scene.add_component(&bad_handle, FooComponent::new()), false);
    assert_eq!(scene.remove_component::<FooComponent>(&bad_handle), false);
}

#[test]
fn scene_add_entity()
{
    let mut scene = SimpleScene::new();
    
    assert_eq!(scene.entity_count(), 0);
    
    let entity = scene.add_entity();
    
    assert_eq!(scene.entity_count(), 1);
    assert_eq!(scene.has_entity(&entity), true);
    
    assert_eq!(scene.remove_entity(&entity), true);
    
    assert_eq!(scene.entity_count(), 0);
    assert_eq!(scene.has_entity(&entity), false);
    
    let entity1 = scene.add_entity();
    assert_eq!(scene.entity_count(), 1);
    assert_eq!(scene.has_entity(&entity1), true);
    
    let entity2 = scene.add_entity();
    assert_eq!(scene.entity_count(), 2);
    assert_eq!(scene.has_entity(&entity2), true);
    
    let entity3 = scene.add_entity();
    assert_eq!(scene.entity_count(), 3);
    assert_eq!(scene.has_entity(&entity3), true);
}

#[test]
fn scene_null_entity()
{
    let mut scene = SimpleScene::new();

    let entity1 = Entity::null();
    assert!(entity1.is_null());

    let entity2 = Entity::default();
    assert!(entity2.is_null());

    let entity3 = scene.add_entity();
    assert!(!entity3.is_null());

    assert_eq!(scene.add_component(&entity2, FooComponent::new()), false);
    assert_eq!(scene.add_component(&entity3, FooComponent::new()), true);

    assert_eq!(scene.has_entity(&entity2), false);
    assert_eq!(scene.has_entity(&entity3), true);
}

#[test]
fn scene_basic_system()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new(&GameInfo::default(), None).unwrap();
    
    let entity = scene.add_entity();
    assert_eq!(scene.add_component(&entity, FooComponent::new()), true);
    
    let store = scene.component_control().store::<FooComponent>();
    let component = store.get_entity(&entity).unwrap();
    assert_eq!(component.runs, 0);
    
    std::mem::drop(store);
    
    scene.think(&mut gameloop);
    
    // System not added yet, should remain 0
    
    let store = scene.component_control().store::<FooComponent>();
    let component = store.get_entity(&entity).unwrap();
    assert_eq!(component.runs, 0);
    
    std::mem::drop(store);
    
    scene.add_thinker_system(Box::new(FooSystem::new()));
    
    scene.think(&mut gameloop);
    
    // System was added, should have processed the component
    
    let store = scene.component_control().store::<FooComponent>();
    let component = store.get_entity(&entity).unwrap();
    assert_eq!(component.runs, 1);
}

#[test]
fn scene_multi_component_system()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new(&GameInfo::default(), None).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    let entity3 = scene.add_entity();
    let entity4 = scene.add_entity();
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity3, BarComponent::new()), true);
    assert_eq!(scene.add_component(&entity4, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity4, BarComponent::new()), true);
    
    scene.add_thinker_system(Box::new(FooSystem::new()));
    
    scene.think(&mut gameloop);
    
    // Only the Foo system was added
    
    {
        let store = scene.component_control().store::<FooComponent>();
        assert!(store.get_entity(&entity1).is_none());
        assert_eq!(store.get_entity(&entity2).unwrap().runs, 1);
        assert!(store.get_entity(&entity3).is_none());
        assert_eq!(store.get_entity(&entity4).unwrap().runs, 1);
        let store = scene.component_control().store::<BarComponent>();
        assert!(store.get_entity(&entity1).is_none());
        assert!(store.get_entity(&entity2).is_none());
        assert_eq!(store.get_entity(&entity3).unwrap().runs, 0);
        assert_eq!(store.get_entity(&entity4).unwrap().runs, 0);
    }
    
    scene.add_thinker_system(Box::new(FooBarSystem::new()));
    
    scene.think(&mut gameloop);
    
    // Both the Foo and Foobar systems were added. Both should have run.
    
    let store = scene.component_control().store::<FooComponent>();
    assert!(store.get_entity(&entity1).is_none());
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 2);
    assert!(store.get_entity(&entity3).is_none());
    assert_eq!(store.get_entity(&entity4).unwrap().runs, 12);
    let store = scene.component_control().store::<BarComponent>();
    assert!(store.get_entity(&entity1).is_none());
    assert!(store.get_entity(&entity2).is_none());
    assert_eq!(store.get_entity(&entity3).unwrap().runs, 00);
    assert_eq!(store.get_entity(&entity4).unwrap().runs, 10);
}

#[test]
fn scene_remove_entity_component()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new(&GameInfo::default(), None).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    
    scene.add_thinker_system(Box::new(FooSystem::new()));
    
    scene.think(&mut gameloop);
    
    let store = scene.component_control().store::<FooComponent>();
    assert_eq!(store.get_entity(&entity1).unwrap().runs, 1);
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 1);
    
    std::mem::drop(store);
    
    // Prevent adding component where one already exists
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), false);
    assert_eq!(scene.remove_component::<FooComponent>(&entity1), true);
    
    // Prevent duplicate removal
    
    assert_eq!(scene.remove_component::<FooComponent>(&entity1), false);
    
    let store = scene.component_control().store::<FooComponent>();
    assert!(store.get_entity(&entity1).is_none());
    
    std::mem::drop(store);
    
    scene.think(&mut gameloop);
    
    let store = scene.component_control().store::<FooComponent>();
    assert_eq!(store.get_entity(&entity2).unwrap().runs, 2);
    
    std::mem::drop(store);
    
    assert_eq!(scene.remove_entity(&entity2), true);
    assert_eq!(scene.remove_entity(&entity2), false);
    
    scene.think(&mut gameloop);
    
    let store = scene.component_control().store::<FooComponent>();
    assert!(store.get_entity(&entity2).is_none());
}

#[test]
fn scene_remove_many()
{
    let mut scene = SimpleScene::new();
    
    for gen in 1..11
    {
        for _ in 0..1000
        {
            let new_entity = scene.add_entity();
            
            assert_eq!(scene.add_component(&new_entity, FooComponent::new()), true);
            assert_eq!(scene.add_component(&new_entity, BarComponent::new()), true);
        }
        
        for i in 0..1000
        {
            assert!(scene.remove_entity(&Entity::new(i, gen)));
        }
    }
}

#[test]
fn scene_remove_random()
{
    for _ in 0..100
    {
        let mut scene = SimpleScene::new();
        let mut entities = Vec::new();
        let rng = &mut rand::thread_rng();
        
        for _ in 0..10
        {
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponent::new()), true);
                
                entities.push(new_entity);
            }
            
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponent::new()), true);
                assert_eq!(scene.add_component(&new_entity, BarComponent::new()), true);
                
                entities.push(new_entity);
            }
            
            entities.shuffle(rng);
            
            for _ in 0..100
            {
                let entity = entities.pop().unwrap();
                assert!(scene.remove_entity(&entity));
            }
            
            let store = scene.component_control().store::<FooComponent>();
            
            for entity in &entities
            {
                assert!(scene.has_entity(entity));
                assert!(store.contains(entity));
            }
        }
        
        while !entities.is_empty()
        {
            let entity = entities.pop().unwrap();
            assert!(scene.remove_entity(&entity));
        }
    }
}

#[test]
fn scene_remove_random_ordered()
{
    for _ in 0..100
    {
        let mut scene = SimpleScene::new();
        let mut gameloop = GameControl::new(&GameInfo::default(), None).unwrap();
        let mut entities = Vec::new();
        let rng = &mut rand::thread_rng();
        
        scene.add_thinker_system(Box::new(FooBarSystemOrdered::new()));
        
        for _ in 0..10
        {
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponentOrdered::new()), true);
                
                entities.push(new_entity);
            }
            
            for _ in 0..100
            {
                let new_entity = scene.add_entity();
                
                assert_eq!(scene.add_component(&new_entity, FooComponentOrdered::new()), true);
                assert_eq!(scene.add_component(&new_entity, BarComponentOrdered::new()), true);
                
                entities.push(new_entity);
            }
            
            entities.shuffle(rng);
            
            for _ in 0..100
            {
                let entity = entities.pop().unwrap();
                assert!(scene.remove_entity(&entity));
            }
            
            let store = scene.component_control().store::<FooComponentOrdered>();
            
            for entity in &entities
            {
                assert!(scene.has_entity(entity));
                assert!(store.contains(entity));
            }
            
            std::mem::drop(store);
            
            // Check consistency after purging entities
            scene.think(&mut gameloop);
            
            let store = scene.component_control().store::<FooComponentOrdered>();
            
            for entity in &entities
            {
                assert!(scene.has_entity(entity));
                assert!(store.contains(entity));
            }
        }
        
        while !entities.is_empty()
        {
            let entity = entities.pop().unwrap();
            assert!(scene.remove_entity(&entity));
        }
    }
}

#[test]
fn scene_component_registration()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new(&GameInfo::default(), None).unwrap();

    scene.add_thinker_system(Box::new(FooSystem::new()));
    scene.add_thinker_system(Box::new(BarAdderSystem::new()));
    scene.add_thinker_system(Box::new(FooBarSystem::new()));
    scene.add_drawer_system(Box::new(DrawSystemGood::new()));

    scene.think(&mut gameloop);

    assert_eq!(scene.component_control().store::<BarComponent>().count(), 1);

    scene.draw(gameloop.renderer_mut().control(), &DrawTransform::identity(), 0.0);
    scene.think(&mut gameloop);

    let entity1 = scene.add_entity();
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);

    scene.think(&mut gameloop);
    scene.draw(gameloop.renderer_mut().control(), &DrawTransform::identity(), 0.0);

    let entity2 = scene.add_entity();
    assert_eq!(scene.add_component(&entity2, BarComponent::new()), true);

    scene.think(&mut gameloop);
    scene.draw(gameloop.renderer_mut().control(), &DrawTransform::identity(), 0.0);
}

#[test]
fn scene_immutable_draw()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new(&GameInfo::default(), None).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    
    scene.add_drawer_system(Box::new(DrawSystemGood::new()));
    
    scene.draw(gameloop.renderer_mut().control(), &DrawTransform::identity(), 0.0);
}

#[test]
#[should_panic(expected = "Mutable component access is not allowed while drawing.")]
fn scene_mutable_draw_should_panic()
{
    let mut scene = SimpleScene::new();
    let mut gameloop = GameControl::new(&GameInfo::default(), None).unwrap();
    
    let entity1 = scene.add_entity();
    let entity2 = scene.add_entity();
    
    assert_eq!(scene.add_component(&entity1, FooComponent::new()), true);
    assert_eq!(scene.add_component(&entity2, FooComponent::new()), true);
    
    scene.add_drawer_system(Box::new(DrawSystemBad::new()));
    
    scene.think(&mut gameloop);
    scene.draw(gameloop.renderer_mut().control(), &DrawTransform::identity(), 0.0);
}
