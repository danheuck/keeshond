/// (ADVANCED) Vulkan rendering backend

use std::rc::Rc;
use std::cell::RefCell;

use crate::renderer::{DrawControl, DrawControlResult, DrawTransform, DrawSpriteInfo, DrawSpriteRawInfo, DrawBackgroundColorInfo, RendererOp, RendererNewInfo, RendererError, Sheet, Shader};

use keeshond_datapack::{DataId, DataStore, PreparedStore, PreparedStoreError, DataPreparer};
use gfx_backend_vulkan;

use ash::version::InstanceV1_0;
use ash::vk::Handle;

pub struct VulkanDrawControl
{
    transform : DrawTransform
}

impl VulkanDrawControl
{
    
}

impl DrawControl for VulkanDrawControl
{
    fn new(renderer_new_info : &mut RendererNewInfo) -> DrawControlResult where Self : Sized
    {
        warn!("The Vulkan renderer is UNFINISHED!");
        
        let video_subsystem = &renderer_new_info.video_subsystem;
        let mut window_builder = video_subsystem.window("", renderer_new_info.width, renderer_new_info.height);
        
        window_builder.position_centered().resizable().allow_highdpi().vulkan();
        
        let instance = gfx_backend_vulkan::Instance::create("Keeshond Vulkan Renderer", 1);
        
        let mut extension_list = String::new();
        
        for extension in instance.extensions
        {
            extension_list.push(' ');
            extension_list.push_str(&extension.to_string_lossy());
        }
        
        info!("Vulkan instance extensions:{}", extension_list);
        
        let window_raw = try_or_else!(window_builder.build(),
            |error| Err(RendererError::RendererInitFailed(format!("Could not create game window: {}", error))));
        
        let sdl_surface = try_or_else!(window_raw.vulkan_create_surface(instance.raw.0.handle().as_raw() as usize),
            |error| Err(RendererError::RendererInitFailed(format!("Could not create Vulkan surface: {}", error))));
        
        let window = Rc::new(RefCell::new(window_raw));
        
        Ok((Box::new(VulkanDrawControl
        {
            transform : DrawTransform::identity()
        }), window))
    }
    
    fn screen_transform(&self) -> &DrawTransform
    {
        &self.transform
    }
    
    #[allow(unused_variables)]
    fn sync_sheet_store(&mut self, sheet_store : &mut DataStore<Sheet>) -> Result<(), PreparedStoreError>
    {
        Ok(())
    }
    
    #[allow(unused_variables)]
    fn sync_shader_store(&mut self, shader_store : &mut DataStore<Shader>) -> Result<(), PreparedStoreError>
    {
        Ok(())
    }
    
    #[allow(unused_variables)]
    fn draw_sprite(&mut self, draw_info : &DrawSpriteInfo) {}

    #[allow(unused_variables)]
    fn draw_sprite_raw(&mut self, draw_info: &DrawSpriteRawInfo) {}

    #[allow(unused_variables)]
    fn draw_background_color(&mut self, draw_info : &DrawBackgroundColorInfo) {}
    #[cfg(feature = "imgui_base")]
    #[allow(unused_variables)]
    fn draw_imgui(&mut self, ui : imgui::Ui) {}
    #[allow(unused_variables)]
    fn recalculate_viewport(&mut self, base_width : f32, base_height : f32) {}
    #[allow(unused_variables)]
    fn start_drawing(&mut self) {}
    #[allow(unused_variables)]
    fn present(&mut self) {}
    #[allow(unused_variables)]
    fn flush_drawing(&mut self) {}
    #[cfg(feature = "imgui_base")]
    #[allow(unused_variables)]
    fn init_imgui_renderer(&mut self, imgui : &mut imgui::ImGui) -> Result<(), RendererError>
    {
        Ok(())
    }
}