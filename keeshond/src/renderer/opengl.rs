//! (ADVANCED) OpenGL 3 rendering backend

use std::io::Cursor;
use std::rc::Rc;
use std::cell::RefCell;

use glium::{Frame, Surface, Program, ProgramCreationError, IndexBuffer, VertexBuffer};
use glium::texture::{RawImage2d, Texture2d, UncompressedFloatFormat};
use glium::uniforms::{Sampler, MagnifySamplerFilter, MinifySamplerFilter, SamplerWrapFunction};
use glium::program::ProgramCreationInput;
use glium::backend::Facade;
use crate::thirdparty::glium_sdl2::{DisplayBuild, SDL2Facade};

use keeshond_datapack::{DataId, DataStore, PreparedStore, PreparedStoreError, DataPreparer};

use crate::renderer::{DrawControl, DrawControlResult, DrawTransform, DrawSpriteRawInfo, DrawBackgroundColorInfo, RendererOp, RendererNewInfo, RendererError, Sheet, Shader, ViewportMode, DrawSpriteInfo};

#[cfg(feature = "imgui_opengl")] use imgui_glium_renderer;

const INSTANCES_MAX : usize = 8192;

const UNBORKED_IDENTITY_TRANSFORM : [f32; 9] = [2.0, 0.0, 0.0, 0.0, -2.0, 0.0, -1.0, 1.0, 1.0];
const UNBORKED_UPSCALE_TRANSFORM : [f32; 9] = [2.0, 0.0, 0.0, 0.0, 2.0, 0.0, -1.0, -1.0, 1.0];

#[derive(Copy, Clone)]
struct Vertex
{
    a_position : [f32; 2],
}

implement_vertex!(Vertex, a_position);

#[derive(Copy, Clone, Default)]
struct Instance
{
    a_transform_col1 : [f32; 2],
    a_transform_col2 : [f32; 2],
    a_transform_col3 : [f32; 2],
    a_transform_offset : [f32; 2],
    a_tex_coords : [f32; 4],
    a_color : [f32; 3],
    a_alpha : f32
}

implement_vertex!(Instance, a_transform_col1, a_transform_col2, a_transform_col3,
    a_transform_offset, a_tex_coords, a_color, a_alpha);

const VERTEX_QUAD : [Vertex; 4] =
[
    Vertex { a_position: [ 0.0, 0.0 ] },
    Vertex { a_position: [ 1.0, 0.0 ] },
    Vertex { a_position: [ 1.0, 1.0 ] },
    Vertex { a_position: [ 0.0, 1.0 ] }
];
const INDEX_QUAD : [u16; 6] = [ 0, 1, 2, 2, 3, 0 ];

struct GpuSpriteSlice
{
    pub texture_x : f32,
    pub texture_y : f32,
    pub texture_w : f32,
    pub texture_h : f32,
    pub scale_x : f32,
    pub scale_y : f32,
    pub offset_x : f32,
    pub offset_y : f32,
}

struct GpuSheetHandle
{
    texture : Texture2d,
    mag_filter : MagnifySamplerFilter,
    min_filter : MinifySamplerFilter,
    slices : Vec<GpuSpriteSlice>
}

struct GpuSheetPreparer
{
    display : Rc<RefCell<SDL2Facade>>,
    texture_filtering : bool
}

impl GpuSheetPreparer
{
    fn new(display : Rc<RefCell<SDL2Facade>>, texture_filtering : bool) -> GpuSheetPreparer
    {
        GpuSheetPreparer
        {
            display,
            texture_filtering
        }
    }
}

impl DataPreparer<Sheet, GpuSheetHandle> for GpuSheetPreparer
{
    fn prepare(&mut self, data : &mut Sheet, _id : DataId) -> GpuSheetHandle
    {
        let display_ref : &SDL2Facade = &self.display.borrow();
        let mut image_raw = Vec::new();
        
        std::mem::swap(&mut image_raw, &mut data.image_raw);
        
        let image = RawImage2d::from_raw_rgba(image_raw, (data.width, data.height));
        let texture = Texture2d::new(display_ref, image).expect("Texture upload failed");
        
        let filtered;
        
        if data.force_filter
        {
            filtered = data.filtered;
        }
        else
        {
            filtered = self.texture_filtering;
        }
        
        let mag_filter = match filtered
        {
            true => MagnifySamplerFilter::Linear,
            false => MagnifySamplerFilter::Nearest
        };
        let min_filter = match filtered
        {
            true => MinifySamplerFilter::Linear,
            false => MinifySamplerFilter::Nearest
        };

        let mut slices = Vec::new();

        for slice in &data.metadata.slices
        {
            let texture_w = slice.texture_w / data.width as f32;
            let texture_h = slice.texture_h / data.height as f32;

            slices.push(GpuSpriteSlice
            {
                texture_x: slice.texture_x / data.width as f32,
                texture_y: slice.texture_y / data.height as f32,
                texture_w,
                texture_h,
                scale_x : slice.texture_w,
                scale_y : slice.texture_h,
                offset_x: -slice.origin_x / slice.texture_w,
                offset_y: -slice.origin_y / slice.texture_h,
            })
        }
        
        GpuSheetHandle
        {
            texture,
            mag_filter,
            min_filter,
            slices
        }
    }
    fn unprepare(&mut self, _prepared : &mut GpuSheetHandle, _id : DataId)
    {
        
    }
}

struct GpuShaderHandle
{
    program : Program
}

struct GpuShaderPreparer
{
    display : Rc<RefCell<SDL2Facade>>
}

impl GpuShaderPreparer
{
    fn new(display : Rc<RefCell<SDL2Facade>>) -> GpuShaderPreparer
    {
        GpuShaderPreparer
        {
            display
        }
    }
}

impl DataPreparer<Shader, GpuShaderHandle> for GpuShaderPreparer
{
    fn prepare(&mut self, data : &mut Shader, _id : DataId) -> GpuShaderHandle
    {
        let program = make_program(&self.display.borrow(),
            include_str!("shader/vert_standard_140.glsl"),
            &data.program_source).expect("Shader compilation failed");
        
        GpuShaderHandle
        {
            program
        }
    }
    fn unprepare(&mut self, _prepared : &mut GpuShaderHandle, _id : DataId)
    {
        
    }
}

#[derive(Copy, Clone, Default)]
struct InstanceBufferParam
{
    op : RendererOp,
    sheet : DataId,
    shader : DataId
}

fn make_program(display : &SDL2Facade, vertex_shader : &str, fragment_shader : &str) -> Result<Program, ProgramCreationError>
{
    let source = ProgramCreationInput::SourceCode
    {
        vertex_shader,
        fragment_shader,
        geometry_shader : None,
        tessellation_control_shader : None,
        tessellation_evaluation_shader : None,
        transform_feedback_varyings : None,
        uses_point_size : false,
        outputs_srgb : true
    };
    
    glium::Program::new(display, source)
}

fn upscale_instance_from_transform(transform : &DrawTransform) -> Instance
{
    Instance
    {
        a_transform_col1 : [ transform.mat.x.x, transform.mat.x.y ],
        a_transform_col2 : [ transform.mat.y.x, transform.mat.y.y ],
        a_transform_col3 : [ transform.mat.z.x, transform.mat.z.y ],
        a_transform_offset : [ 0.0, 0.0 ],
        a_tex_coords : [0.0, 0.0, 1.0, 1.0],
        a_color : [1.0, 1.0, 1.0],
        a_alpha : 1.0
    }
}

pub struct GlDrawControl
{
    display : Rc<RefCell<SDL2Facade>>,
    window : Rc<RefCell<sdl2::video::Window>>,
    scale_width : f32,
    scale_height : f32,
    viewport_offset : (f32, f32),
    viewport_mode : ViewportMode,
    quad_buffer : VertexBuffer<Vertex>,
    index_buffer : IndexBuffer<u16>,
    instance_buffer_src : Box<[Instance ; INSTANCES_MAX]>,
    instance_buffer_param : Box<[InstanceBufferParam ; INSTANCES_MAX]>,
    instance_buffer : VertexBuffer<Instance>,
    default_texture : Texture2d,
    shader_list : Vec<Program>,
    scissor : Option<glium::Rect>,
    frame : Option<Frame>,
    pixel_scale : u32,
    last_pixel_scale : u32,
    pixel_texture : Option<Texture2d>,
    pixel_upscale_texture : Option<Texture2d>,
    current_index : usize,
    last_view_transform : DrawTransform,
    gpu_sheet_store : PreparedStore<Sheet, GpuSheetHandle>,
    gpu_shader_store : PreparedStore<Shader, GpuShaderHandle>,
    #[cfg(feature = "imgui_opengl")]
    imgui_renderer : Option<imgui_glium_renderer::Renderer>
}

impl GlDrawControl
{
    fn draw_buffers(&mut self)
    {
        if self.current_index <= 0
        {
            return;
        }
        
        let last = self.current_index;
        
        self.instance_buffer.invalidate();
        self.instance_buffer.write(&self.instance_buffer_src[..]);
        
        let mut last_drawn : usize = 0;
        let mut current_op = RendererOp::BackgroundColor;
        let mut current_sheet : DataId = 0;
        let mut current_shader : DataId = 0;
        
        // I can't belive Rust is making me do this
        let buffer_params = self.instance_buffer_param.clone();
        
        let mut do_draw = |first : usize, last : usize, sheet : DataId, shader : DataId, op : RendererOp|
        {
            if first >= last
            {
                return;
            }
            
            if let Some(frame) = &mut self.frame
            {
                let mut texture_ref = &self.default_texture;
                let mut shader_ref = &self.shader_list[op as usize];
                let mut mag_filter = MagnifySamplerFilter::Linear;
                let mut min_filter = MinifySamplerFilter::Linear;
                
                if let Some(gpu_sheet_info) = self.gpu_sheet_store.get(sheet)
                {
                    texture_ref = &gpu_sheet_info.texture;
                    mag_filter = gpu_sheet_info.mag_filter;
                    min_filter = gpu_sheet_info.min_filter;
                }
                if let Some(gpu_shader_info) = self.gpu_shader_store.get(shader)
                {
                    shader_ref = &gpu_shader_info.program;
                }
                
                let uniform = uniform!
                {
                    t_texture : Sampler::new(texture_ref)
                        .magnify_filter(mag_filter).minify_filter(min_filter)
                };
                
                match self.viewport_mode
                {
                    ViewportMode::Independent =>
                    {
                        let params = glium::DrawParameters
                        {
                            blend : glium::draw_parameters::Blend::alpha_blending(),
                            scissor : self.scissor,
                            .. Default::default()
                        };
                        
                        frame.draw((&self.quad_buffer,
                            self.instance_buffer.slice(first..last)
                                .expect("Failed to prepare instance buffer").per_instance()
                                .expect("Failed to prepare instance buffer")),
                            &self.index_buffer, shader_ref,
                            &uniform, &params).expect("Draw operation failed");
                    },
                    ViewportMode::Pixel =>
                    {
                        let params = glium::DrawParameters
                        {
                            blend : glium::draw_parameters::Blend::alpha_blending(),
                            scissor : self.scissor,
                            multisampling : false,
                            dithering : false,
                            .. Default::default()
                        };
                        
                        self.pixel_texture.as_ref().unwrap().as_surface().draw((&self.quad_buffer,
                            self.instance_buffer.slice(first..last)
                                .expect("Failed to prepare instance buffer").per_instance()
                                .expect("Failed to prepare instance buffer")),
                            &self.index_buffer, shader_ref,
                            &uniform, &params).expect("Draw operation failed");
                    }
                }
                
                
            }
        };
        
        for i in 0..last
        {
            let param = &buffer_params[i];
            
            if current_op == param.op && current_sheet == param.sheet
                && current_shader == param.shader && i < last - 1
            {
                continue;
            }
            
            do_draw(last_drawn, i, current_sheet, current_shader, current_op);
            
            current_op = param.op;
            current_sheet = param.sheet;
            current_shader = param.shader;
            last_drawn = i;
        }
        
        do_draw(last_drawn, last, current_sheet, current_shader, current_op);
        
        self.current_index = 0;
    }
    
    fn update_pixel_upscale(&mut self)
    {
        if self.pixel_scale == self.last_pixel_scale
        {
            return;
        }
        
        self.last_pixel_scale = self.pixel_scale;
        let display_ref : &SDL2Facade = &self.display.borrow();
        let pixel_surface = self.pixel_texture.as_mut().unwrap().as_surface();
        let (width, height) = pixel_surface.get_dimensions();
        
        self.pixel_upscale_texture = Some(try_or_else!(Texture2d::empty_with_format(
            display_ref, UncompressedFloatFormat::U8U8U8U8, glium::texture::MipmapsOption::NoMipmap,
            width * self.pixel_scale, height * self.pixel_scale),
            |error| panic!("Could not create upscale framebuffer: {}", error)));
    }
}

impl DrawControl for GlDrawControl
{
    fn new(renderer_new_info : &mut RendererNewInfo) -> DrawControlResult where Self : Sized
    {
        let video_subsystem = &renderer_new_info.video_subsystem;
        let mut window_builder = video_subsystem.window("", renderer_new_info.width * renderer_new_info.default_zoom,
        renderer_new_info.height * renderer_new_info.default_zoom);
        
        window_builder.position_centered().resizable().allow_highdpi();
        
        video_subsystem.gl_attr().set_context_profile(sdl2::video::GLProfile::Core);
        video_subsystem.gl_attr().set_context_version(3, 2);
        
        let display_raw = try_or_else!(window_builder.build_glium(),
            |error| Err(RendererError::RendererInitFailed(format!("Could not create game window: {}", error))));
        
        let mut pixel_texture = None;
        
        match renderer_new_info.viewport_mode
        {
            ViewportMode::Independent => (),
            ViewportMode::Pixel =>
            {
                pixel_texture = Some(try_or_else!(Texture2d::empty_with_format(
                    &display_raw, UncompressedFloatFormat::U8U8U8U8, glium::texture::MipmapsOption::NoMipmap, renderer_new_info.width, renderer_new_info.height),
                    |error| Err(RendererError::RendererInitFailed(format!("Could not create framebuffer: {}", error)))));
            }
        };
        
        let quad_buffer = try_or_else!(glium::VertexBuffer::new(&display_raw, &VERTEX_QUAD),
            |error| Err(RendererError::RendererInitFailed(format!("Could not create vertex buffer: {}", error))));
        let index_buffer = try_or_else!(glium::IndexBuffer::new(&display_raw,
            glium::index::PrimitiveType::TrianglesList, &INDEX_QUAD),
            |error| Err(RendererError::RendererInitFailed(format!("Could not create index buffer: {}", error))));
        let instance_buffer = try_or_else!(glium::VertexBuffer::<Instance>::empty_dynamic(&display_raw, INSTANCES_MAX),
            |error| Err(RendererError::RendererInitFailed(format!("Could not create instance buffer: {}", error))));
        
        let context = display_raw.get_context();
        info!("Renderer: {} {}", context.get_opengl_vendor_string(), context.get_opengl_renderer_string());
        info!("OpenGL version: {}", context.get_opengl_version_string());
        if let Some(free_vram) = context.get_free_video_memory()
        {
            info!("VRAM: {} MiB free", free_vram / 1024 / 1024);
        }
        
        let shader_background = try_or_else!(make_program(&display_raw,
            include_str!("shader/vert_standard_140.glsl"),
            include_str!("shader/frag_solidcolor_140.glsl")),
            |error| Err(RendererError::RendererInitFailed(format!("Could not load shader: {}", error))));
        let shader_sprite = try_or_else!(make_program(&display_raw,
            include_str!("shader/vert_standard_140.glsl"),
            include_str!("shader/frag_sprite_140.glsl")),
            |error| Err(RendererError::RendererInitFailed(format!("Could not load shader: {}", error))));
        
        let shader_list = vec!(shader_background, shader_sprite);
        
        let decoder = png::Decoder::new(Cursor::new(include_bytes!("data/default.png").to_vec()));
        let (info, mut reader) = try_or_else!(decoder.read_info(),
            |error| Err(RendererError::LoadResourceFailed(format!("Failed to read PNG: {}", error))));
        let mut texture_raw = vec![0; info.buffer_size()];
        try_or_else!(reader.next_frame(&mut texture_raw),
            |error| Err(RendererError::LoadResourceFailed(format!("Failed to decode PNG: {}", error))));
        
        let image = RawImage2d::from_raw_rgba(texture_raw, (info.width, info.height));
        let default_texture = try_or_else!(Texture2d::new(&display_raw, image),
            |error| Err(RendererError::RendererInitFailed(format!("Could not upload texture: {}", error))));
        
        let window = display_raw.window_clone();
        
        let display = Rc::new(RefCell::new(display_raw));
        let sheet_data_preparer = Box::new(GpuSheetPreparer::new(display.clone(), renderer_new_info.texture_filtering));
        let shader_data_preparer = Box::new(GpuShaderPreparer::new(display.clone()));
        
        let gpu_sheet_store = try_or_else!(PreparedStore::new(renderer_new_info.resources.sheets_mut(),
            sheet_data_preparer),
            |error| Err(RendererError::LoadResourceFailed(format!("Failed to create GPU sheet handle store: {}", error))));
        
        let gpu_shader_store = try_or_else!(PreparedStore::new(renderer_new_info.resources.shaders_mut(), 
            shader_data_preparer),
            |error| Err(RendererError::LoadResourceFailed(format!("Failed to create GPU shader handle store: {}", error))));
        
        Ok((Box::new(GlDrawControl
        {
            display,
            window : window.clone(),
            viewport_offset : (0.0, 0.0),
            viewport_mode : renderer_new_info.viewport_mode,
            scale_width : 0.0,
            scale_height : 0.0,
            quad_buffer,
            index_buffer,
            instance_buffer_src : Box::new([Default::default() ; INSTANCES_MAX]),
            instance_buffer_param : Box::new([Default::default() ; INSTANCES_MAX]),
            instance_buffer,
            default_texture,
            shader_list,
            scissor : None,
            frame : None,
            pixel_scale : 1,
            last_pixel_scale : 0,
            pixel_texture,
            pixel_upscale_texture : None,
            current_index : 0,
            last_view_transform : DrawTransform::identity(),
            gpu_sheet_store,
            gpu_shader_store,
            #[cfg(feature = "imgui_opengl")]
            imgui_renderer : None
        }), window))
    }
    
    fn screen_transform(&self) -> &DrawTransform
    {
        &self.last_view_transform
    }
    
    fn sync_sheet_store(&mut self, sheet_store : &mut DataStore<Sheet>) -> Result<(), PreparedStoreError>
    {
        self.gpu_sheet_store.sync(sheet_store)
    }
    
    fn sync_shader_store(&mut self, shader_store : &mut DataStore<Shader>) -> Result<(), PreparedStoreError>
    {
        self.gpu_shader_store.sync(shader_store)
    }

    fn draw_sprite(&mut self, draw_info: &DrawSpriteInfo)
    {
        let mut transform = draw_info.transform.clone();
        let mut transform_offset_x = -0.5;
        let mut transform_offset_y = -0.5;
        let mut texture_x = 0.0;
        let mut texture_y = 0.0;
        let mut texture_w = 1.0;
        let mut texture_h = 1.0;
        let mut sheet_id = 0;

        transform.translate(draw_info.x, draw_info.y);
        if draw_info.angle != 0.0
        {
            transform.rotate(draw_info.angle);
        }

        let mut got_slice = false;

        if let Some(gpu_sheet_info) = self.gpu_sheet_store.get(draw_info.sheet_id)
        {
            if draw_info.slice < gpu_sheet_info.slices.len()
            {
                let slice = &gpu_sheet_info.slices[draw_info.slice];

                transform.scale(draw_info.scale_x * slice.scale_x, draw_info.scale_y * slice.scale_y);

                sheet_id = draw_info.sheet_id;
                transform_offset_x = slice.offset_x;
                transform_offset_y = slice.offset_y;
                texture_x = slice.texture_x;
                texture_y = slice.texture_y;
                texture_w = slice.texture_w;
                texture_h = slice.texture_h;

                got_slice = true;
            }
        }

        if !got_slice
        {
            transform.scale(draw_info.scale_x * 32.0, draw_info.scale_y * 32.0);
        }

        self.draw_sprite_raw(&DrawSpriteRawInfo
        {
            sheet_id,
            shader_id: draw_info.shader_id,
            transform: transform,
            transform_offset_x,
            transform_offset_y,
            texture_x,
            texture_y,
            texture_w,
            texture_h,
            r: draw_info.r,
            g: draw_info.g,
            b: draw_info.b,
            alpha: draw_info.alpha
        });
    }
    
    fn draw_sprite_raw(&mut self, draw_info : &DrawSpriteRawInfo)
    {
        if self.current_index >= INSTANCES_MAX
        {
            self.flush_drawing();
        }
        
        let transform = &draw_info.transform;
        let instance = Instance
        {
            a_transform_col1 : [ transform.mat.x.x, transform.mat.x.y ],
            a_transform_col2 : [ transform.mat.y.x, transform.mat.y.y ],
            a_transform_col3 : [ transform.mat.z.x, transform.mat.z.y ],
            a_transform_offset : [ draw_info.transform_offset_x, draw_info.transform_offset_y ],
            a_tex_coords : [ draw_info.texture_x, draw_info.texture_y, draw_info.texture_w, draw_info.texture_h ],
            a_color : [ draw_info.r, draw_info.g, draw_info.b ],
            a_alpha : draw_info.alpha
        };
        self.instance_buffer_src[self.current_index] = instance;
        self.instance_buffer_param[self.current_index] = InstanceBufferParam
        {
            op : RendererOp::Sprite,
            sheet : draw_info.sheet_id,
            shader : draw_info.shader_id
        };
        
        self.current_index += 1;
    }
    
    fn draw_background_color(&mut self, draw_info : &DrawBackgroundColorInfo )
    {
        if self.current_index >= INSTANCES_MAX
        {
            self.flush_drawing();
        }
        
        let instance = Instance
        {
            a_transform_col1 : [ 2.0, 0.0 ],
            a_transform_col2 : [ 0.0, -2.0 ],
            a_transform_col3 : [ -1.0, 1.0 ],
            a_transform_offset : [ 0.0, 0.0 ],
            a_tex_coords : [ 0.0, 0.0, 1.0, 1.0 ],
            a_color : [ draw_info.r, draw_info.g, draw_info.b ],
            a_alpha : draw_info.alpha
        };
        self.instance_buffer_src[self.current_index] = instance;
        self.instance_buffer_param[self.current_index] = InstanceBufferParam
        {
            op : RendererOp::BackgroundColor,
            sheet : 0,
            shader : draw_info.shader_id
        };
        
        self.current_index += 1;
    }
    
    #[cfg(feature = "imgui_base")]
    fn draw_imgui(&mut self, ui : imgui::Ui)
    {
        self.flush_drawing();
        
        if let Some(frame) = &mut self.frame
        {
            if let Some(imgui_renderer) = &mut self.imgui_renderer
            {
                let draw_data = ui.render();
                imgui_renderer.render(frame, &draw_data).expect("imgui render failed");
            }
        }
    }
    
    fn flush_drawing(&mut self)
    {
        self.draw_buffers();
        
        self.display.borrow().get_context().flush();
    }
    
    fn recalculate_viewport(&mut self, base_width : f32, base_height : f32)
    {
        if self.frame.is_some()
        {
            warn!("Cannot recalculate viewport in the middle of drawing");
            return;
        }
        
        let (width, height) = self.window.borrow().size();
        let base_ratio = base_width / base_height;
        let ratio = width as f32 / height as f32;
        
        match self.viewport_mode
        {
            ViewportMode::Independent =>
            {
                self.scale_width = base_width;
                self.scale_height = base_height;
                
                let mut new_scissor = glium::Rect { left : 0, bottom : 0, width, height };
                
                if ratio > base_ratio
                {
                    // Wider than base resolution
                    self.scale_width = ratio / base_ratio * base_width;
                    new_scissor.width = (height as f32 * base_ratio) as u32;
                    new_scissor.left = ((width) - new_scissor.width) / 2;
                }
                else if ratio < base_ratio
                {
                    // Taller than base resolution
                    self.scale_height = base_ratio / ratio * base_height;
                    new_scissor.height = (width as f32 / base_ratio) as u32;
                    new_scissor.bottom = ((height) - new_scissor.height) / 2;
                }
                
                self.viewport_offset = ((self.scale_width - base_width) / 2.0, (self.scale_height - base_height) / 2.0);
                
                self.scissor = Some(new_scissor);
            },
            ViewportMode::Pixel =>
            {
                self.scale_width = 1.0;
                self.scale_height = 1.0;
                
                if ratio > base_ratio
                {
                    self.scale_width = ratio / base_ratio;
                }
                else if ratio < base_ratio
                {
                    self.scale_height = base_ratio / ratio;
                }
                
                self.viewport_offset = ((self.scale_width - 1.0) / 2.0, (self.scale_height - 1.0) / 2.0);
                self.scissor = None;
                
                self.pixel_scale = ((width as f32 / base_width / self.scale_width).round() as u32).max(1);
            }
        }
    }
    
    fn start_drawing(&mut self)
    {
        if self.frame.is_some()
        {
            panic!("Cannot call draw() twice without a present()");
        }
        
        self.frame = Some(self.display.borrow().draw());
        
        self.frame.as_mut().unwrap().clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
        
        let mut view_transform = DrawTransform::from_array(&UNBORKED_IDENTITY_TRANSFORM);
        
        match self.viewport_mode
        {
            ViewportMode::Independent =>
            {
                let (x, y) = self.viewport_offset;
                
                view_transform.scale(1.0 / self.scale_width, 1.0 / self.scale_height);
                view_transform.translate(x, y);
            },
            ViewportMode::Pixel =>
            {
                let mut pixel_surface = self.pixel_texture.as_mut().unwrap().as_surface();
                let (width, height) = pixel_surface.get_dimensions();
                
                pixel_surface.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
                
                view_transform.scale(1.0 / width as f32, 1.0 / height as f32);
            }
        }
        
        self.last_view_transform = view_transform.clone();
    }
    
    fn present(&mut self)
    {
        if self.frame.is_some()
        {
            match self.viewport_mode
            {
                ViewportMode::Independent => (),
                ViewportMode::Pixel =>
                {
                    // Low-res pixel to high-res pixel
                    
                    let mut transform = DrawTransform::from_array(&UNBORKED_UPSCALE_TRANSFORM);
                    
                    let params = glium::DrawParameters::default();
                    let instance = upscale_instance_from_transform(&transform);
                    
                    self.instance_buffer.invalidate();
                    self.instance_buffer.map_write().set(0, instance);
                    
                    self.update_pixel_upscale();
                    
                    let uniform = uniform!
                    {
                        t_texture : Sampler::new(self.pixel_texture.as_ref().unwrap())
                            .magnify_filter(MagnifySamplerFilter::Nearest)
                            .minify_filter(MinifySamplerFilter::Nearest)
                    };
                    
                    self.pixel_upscale_texture.as_mut().unwrap().as_surface().draw((&self.quad_buffer,
                        self.instance_buffer.slice(0..1)
                            .expect("Failed to prepare instance buffer").per_instance()
                            .expect("Failed to prepare instance buffer")),
                        &self.index_buffer, &self.shader_list[RendererOp::Sprite as usize],
                        &uniform, &params).expect("Draw operation failed");
                    
                    // High-res pixel to screen
                    
                    let uniform = uniform!
                    {
                        t_texture : Sampler::new(self.pixel_upscale_texture.as_ref().unwrap())
                            .wrap_function(SamplerWrapFunction::Clamp)
                    };
                    
                    let (x, y) = self.viewport_offset;
                    transform.scale(1.0 / self.scale_width, 1.0 / self.scale_height);
                    transform.translate(x, y);
                    let instance = upscale_instance_from_transform(&transform);
                    
                    self.instance_buffer.map_write().set(1, instance);
                    
                    self.frame.as_mut().unwrap().draw((&self.quad_buffer,
                        self.instance_buffer.slice(1..2)
                            .expect("Failed to prepare instance buffer").per_instance()
                            .expect("Failed to prepare instance buffer")),
                        &self.index_buffer, &self.shader_list[RendererOp::Sprite as usize],
                        &uniform, &params).expect("Draw operation failed");
                }
            }
            
            let mut frame = None;        
            std::mem::swap(&mut frame, &mut self.frame);
            
            frame.unwrap().finish().expect("Draw finish failed");
        }
    }
    
    #[cfg(feature = "imgui_base")]
    fn init_imgui_renderer(&mut self, imgui : &mut imgui::Context) -> Result<(), RendererError>
    {
        if self.imgui_renderer.is_some()
        {
            return Err(RendererError::ImguiRendererFailed("Already initialized".to_string()));
        }
        
        let imgui_renderer = try_or_else!(imgui_glium_renderer::Renderer::init(
            imgui, &*self.display.borrow()),
            |error| Err(RendererError::ImguiRendererFailed(format!("Could not initialize imgui renderer: {}", error))));
        
        self.imgui_renderer = Some(imgui_renderer);
        
        Ok(())
    }
}
