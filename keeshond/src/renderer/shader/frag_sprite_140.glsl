#version 140

uniform sampler2D t_texture;
in vec2 v_uv;
in vec3 v_color;
in float v_alpha;

out vec4 TargetScreen;

void main()
{
    vec4 color = texture(t_texture, v_uv);
    
    color = color * vec4(v_color, v_alpha);
    TargetScreen = color;
}
