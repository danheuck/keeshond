#version 140

in vec2 a_position;
in vec2 a_transform_col1;
in vec2 a_transform_col2;
in vec2 a_transform_col3;
in vec2 a_transform_offset;
in vec4 a_tex_coords;
in vec3 a_color;
in float a_alpha;

out vec2 v_uv;
out float v_alpha;
out vec3 v_color;

void main()
{
    mat3 instance_transform = mat3(vec3(a_transform_col1, 0.0), vec3(a_transform_col2, 0.0), vec3(a_transform_col3, 1.0));
    vec2 position = (instance_transform * vec3(a_position + a_transform_offset, 1.0)).xy;
    
    v_uv = vec2(a_position.x * a_tex_coords[2] + a_tex_coords[0], a_position.y * a_tex_coords[3] + a_tex_coords[1]);
    v_alpha = a_alpha;
    v_color = a_color;
    
    gl_Position = vec4(position, 0.0, 1.0);
}
