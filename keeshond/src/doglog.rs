//! (ADVANCED) Keeshond's default logger implementation

use hashers::fx_hash::FxHasher;
use log::{Log, Record, LevelFilter, Metadata, SetLoggerError};

use std::collections::HashSet;
use std::hash::BuildHasherDefault;

pub struct DogLog
{
    allowed_packages : HashSet<String, BuildHasherDefault<FxHasher>>,
}

fn prefix(text : &str, pos : usize) -> &str
{
    &text[..pos]
}

impl DogLog
{
    pub fn new() -> DogLog
    {
        let mut allowed_packages = HashSet::with_hasher(BuildHasherDefault::<FxHasher>::default());
        allowed_packages.insert(String::from("keeshond"));
        
        DogLog
        {
            allowed_packages
        }
    }
    
    pub fn add_package_filter(&mut self, package_name : String)
    {
        self.allowed_packages.insert(package_name);
    }
    
    pub fn init(logger : DogLog, max_level : LevelFilter) -> Result<(), SetLoggerError>
    {
        let result = log::set_boxed_logger(Box::new(logger));
        
        if result.is_ok()
        {
            log::set_max_level(max_level);
        }
        
        result
    }
}

impl Log for DogLog
{
    fn enabled(&self, metadata: &Metadata) -> bool
    {
        let mut target : &str = &metadata.target();
        if let Some(namespacer) = metadata.target().find(':')
        {
            target = prefix(metadata.target(), namespacer);
        }
        
        self.allowed_packages.contains(&String::from(target))
    }
    
    fn log(&self, record : &Record)
    {
        if self.enabled(record.metadata())
        {
            println!("[ {:5} {} ] {}", record.level(), record.target(), record.args());
        }
    }
    
    fn flush(&self)
    {
    
    }
}
