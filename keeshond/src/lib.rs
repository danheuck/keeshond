//! # 🐶 KEESHOND Game Engine 🐶
//! 
//! Keeshond is a 2D game engine with a focus on quickly bringing ideas onscreen.
//! 
//! 
//! # Goals
//! 
//! - Runs reasonably well on hardware from 2010
//! - Deterministic game logic across systems, important for speedrunners
//! - Implements features games need, without adding unneeded complexity
//! - First-class Linux support via SDL2
//! 
//! # How to use this documentation
//!
//! The document you see here serves as an API reference. It is not intended as a
//! starting point for new users. If you're looking to get started, it is suggested
//! that you look at other material instead, such as the included examples.
//! 
//! Keeshond is designed like a framework, in which you construct a gameloop,
//! which handle the program flow for you automatically. There is some plumbing
//! that you normally do not need to care about. For example, there is usually no
//! need to manually create the [gameloop::GameControl], as the framework creates
//! this for you when you create the [gameloop::Gameloop].
//! However, such functionality is publicly exported in the case that they may be
//! helpful to more experienced users.
//! 
//! If you just want to make a game and use the framework as it is designed,
//! look for structs and functions that are marked *(ADVANCED)* - these are items
//! that may be ignored as you will not normally be working with them.

#[cfg(test)] extern crate rand;

#[cfg(all(feature = "imgui_base", feature = "opengl", not(feature = "imgui_opengl")))]
compile_error!("You must enable the \"imgui_opengl\" feature to use \"imgui_base\" with \"opengl\".");
#[cfg(all(feature = "imgui_base", feature = "vulkan", not(feature = "imgui_vulkan")))]
compile_error!("You must enable the \"imgui_vulkan\" feature to use \"imgui_base\" with \"vulkan\".");

#[cfg(feature = "graphical_panic")] extern crate backtrace;

#[cfg(feature = "imgui_base")] extern crate imgui;
#[cfg(feature = "imgui_base")] extern crate imgui_sys;
#[cfg(feature = "imgui_opengl")] extern crate imgui_glium_renderer;
#[cfg(feature = "imgui_base")] #[macro_use] extern crate cstr_macro;

#[macro_use] extern crate try_or;
extern crate failure;
#[macro_use] extern crate failure_derive;
#[macro_use] extern crate log;
#[macro_use] extern crate downcast_rs;
extern crate getopts;
extern crate sdl2;
extern crate sdl2_sys;
extern crate serde;
extern crate serde_json;
extern crate hound;
extern crate lewton;
extern crate alto;
extern crate hashers;
extern crate bit_set;
extern crate bitarray_set;
extern crate typenum;
extern crate generic_array;
extern crate cgmath;
#[cfg(feature = "opengl")] #[macro_use] extern crate glium;
#[cfg(feature = "vulkan")] extern crate gfx_hal;
#[cfg(feature = "vulkan")] extern crate gfx_backend_vulkan;

mod thirdparty;
#[cfg(test)] mod tests;

pub mod doglog;
#[cfg(feature = "imgui_base")] mod imgui_handler;
pub mod audio;
pub mod input;
pub mod scene;
pub mod renderer;
pub mod gameloop;
