# 🐶 KEESHOND Game Engine 🐶

## About

Keeshond is a 2D game engine with a focus on quickly bringing ideas onscreen.


## Getting started

Check out The Keeshond Book:
https://cosmicchipsocket.com/keeshond_book


## Project goals

- Runs reasonably well on hardware from 2010
- Deterministic game logic across systems, important for speedrunners
- Implements features games need, without adding unneeded complexity
- First-class Linux support via SDL2


## Current features

- Gameloop with fixed timestep synchronization
- Custom-built ECS system designed for decoupled game/drawing logic
- Keyboard and gamepad input system
- Fast sprite rendering (can draw close to 100k sprites at 60 FPS on a 2010 CPU and capable GPU)
- Asset loading from the filesystem
- Audio system via OpenAL
- Resolution-independent viewport scaling with maintained aspect ratio
- Optional pixel upscaling with anti dot crawl for neo-retro games
- Optional imgui integration


## Features to come

- Multiplayer gamepad and mouse input
- Audio streaming
- Asset loading from .pk3 files
- Timeline framework, for choreographing animations, sequences, and more
- Tilemaps
- Collision engine via ncollide
- Scene editor


## Running examples

In order to get the examples to load their resources, run them from within the `keeshond` directory.

```
cd keeshond
cargo run --example doggymark
```


## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.


## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
