# Keeshond TODOs

## Todo

- [ ] Workaround init problems with default OpenAL device
- [ ] ECS QoL improvements
- [ ] Costume/Actor system
- [ ] Costume editor
- [ ] Vulkan renderer

- [ ] Update The Keeshond Book Part 1
- [ ] Keeshond Book appendix for input key names

- [ ] .pk3 loading
- [ ] 1:1 viewports
- [ ] Configuration file support
- [ ] Datapack support for file streaming
- [ ] Mouse and legacy joystick input
- [ ] Multiplayer gamepad input
- [ ] Music streaming playback
- [ ] Music stack - crossfading, jingles, multitrack
- [ ] Object culling and deactivation
- [ ] Tilemaps
- [ ] Object-to-object collision
- [ ] Object-to-tilemap collision
- [ ] Scene format
- [ ] Scene editor
- [ ] ImGui "wants input" and texture integration
- [ ] Touch events
- [ ] Unified package loading across types

## Done for 0.14

- [x] ImGui systems made more ergonomic

## Done for 0.13

- [x] Sprite slices
- [x] Gamepad input
- [x] Input axes
- [x] Four-way, eight-way, angle + distance directional input helpers
- [x] Ignore Enter when using Alt + Enter shortcut

## Done for 0.12

- [x] Entity now available as SpawnArg
- [x] spawn_later() returns an Entity
- [x] Entity is now cloneable
- [x] Entity now properly nullable
- [x] Fix readding entity with unpurged slot
- [x] Purge entries on mutable store access, not just mutable iterator
- [x] Basic support for global singletons

## Done for 0.11

- [x] Fix slow resource loading
- [x] ECS components now auto-register!
- [x] Update The Keeshond Book Part 1

## Done for 0.10

- [x] ECS QoL improvements
- [x] Fix game skipping initial scene frames due to loading lag
- [x] Fix playing sound on same frame as loading sound resource

## Done for 0.9

- [x] Component sorting
- [x] Frame interpolation improvements (frame cap, CLI switch)
- [x] Warning when forgetting to set initial scene
- [x] Spawn arguments
- [x] Fixed issue with doing component adds and removals on the same frame

## Done for 0.8

- [x] ECS QoL enhancements
- [x] Package listing support in keeshond_datapack
- [x] The Keeshond Book Part 1

## Done for 0.7

- [x] Object prefabs/prototypes
- [x] User-friendly GUI panic
- [x] Pixel viewports
- [x] Texture filtering made optional

## Done for 0.6

- [x] Faster transform matrix
- [x] Game properties
- [x] Kiosk mode
- [x] Internal commandline arguments

## Done for 0.5

- [x] Fullscreen mode
- [x] Sound playback
- [x] Sound voice controls (stop, adjust volume/pan/pitch)
