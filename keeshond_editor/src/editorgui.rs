use keeshond::gameloop::GameControl;
use keeshond::scene::{Component, ComponentControl, SceneControl, ImGuiSystem, NullSceneType};

use keeshond_datapack::source::{SourceId, FilesystemSource};

use std::collections::{HashMap, HashSet};
use std::string::ToString;

use strum::{EnumCount, IntoEnumIterator};
use strum_macros::EnumIter;

use imgui;
use imgui::{im_str, Condition};
use imgui::{ImString, StyleColor};

use crate::document::Document;
use crate::document::sheetdocument::SheetDocument;

const FILEPATH_MAXSIZE : usize = 65536;

#[derive(Clone, Serialize, Deserialize)]
struct EditorConfig
{
    source_path: String
}

impl Default for EditorConfig
{
    fn default() -> EditorConfig
    {
        EditorConfig
        {
            source_path : "".to_string()
        }
    }
}

#[derive(Copy, Clone, EnumCount, EnumIter, Display)]
pub enum ResourceType
{
    #[strum(to_string="sheets")]
    Sheets
}

pub struct ResourceContents
{
    packages_to_load : HashSet<String>,
    package_contents : HashMap<String, Vec<String>>
}

impl ResourceContents
{
    pub fn new() -> ResourceContents
    {
        ResourceContents
        {
            packages_to_load : HashSet::new(),
            package_contents : HashMap::new()
        }
    }
}

struct DocumentEntry
{
    document : Box<dyn Document>,
    name : String
}

pub struct EditorControl
{
    config : EditorConfig,
    need_reload: bool,
    load_source : bool,
    source_id : SourceId,
    packages_listed : bool,
    im_source_path : ImString,
    package_list : Vec<String>,
    package_errors : Vec<String>,
    res : Vec<ResourceContents>,
    documents : Vec<DocumentEntry>,
    name_to_document : HashMap<String, usize>,
    document_to_focus : Option<usize>
}

impl EditorControl
{
    pub fn new() -> EditorControl
    {
        let mut res = Vec::new();
        
        for _ in 0 .. ResourceType::count()
        {
            res.push(ResourceContents::new());
        }
        
        EditorControl
        {
            config : confy::load("keeshond_editor").unwrap_or(EditorConfig::default()),
            need_reload: false,
            load_source : false,
            source_id : 0,
            packages_listed : false,
            im_source_path : ImString::with_capacity(FILEPATH_MAXSIZE),
            package_list : Vec::new(),
            package_errors : Vec::new(),
            res,
            documents : Vec::new(),
            name_to_document : HashMap::new(),
            document_to_focus : None
        }
    }

    pub fn save_config(&mut self)
    {
        self.config.source_path = self.im_source_path.to_str().to_string();

        match confy::store("keeshond_editor", self.config.clone())
        {
            Ok(_) =>
            {
                info!("Config saved.");
            },
            Err(error) =>
            {
                error!("Error while saving config: {}", error);
            }
        }
    }

    pub fn switch_project(&mut self, game : &mut GameControl)
    {
        self.source_id = 0;
        self.load_source = false;
        self.need_reload = true;

        game.source_manager().borrow_mut().clear();
    }

    pub fn load_sources(&mut self, game: &mut GameControl)
    {
        if self.need_reload
        {
            game.res_mut().sheets_mut().unload_all();

            self.package_list.clear();

            for res_type in ResourceType::iter()
            {
                let res = &mut self.res[res_type as usize];

                for (package, _) in &res.package_contents
                {
                    res.packages_to_load.insert(package.clone());
                }
            }

            self.packages_listed = false;
            self.need_reload = false;
        }

        if self.source_id == 0 && self.load_source
        {
            let source_manager = game.source_manager();
            let source = FilesystemSource::new(&self.im_source_path.to_str());

            self.source_id = source_manager.borrow_mut().add_source(Box::new(source));

            self.load_source = false;
        }

        if self.source_id != 0 && !self.packages_listed
        {
            let source_manager = game.source_manager();
            let mut source_manager_borrow = source_manager.borrow_mut();
            let source_option = source_manager_borrow.source(self.source_id);

            if let Some(source) = source_option
            {
                self.package_list = source.list_packages();
            }

            self.packages_listed = true;
        }

        for res_type in ResourceType::iter()
        {
            let res = &mut self.res[res_type as usize];
            for package_name in res.packages_to_load.iter()
            {
                let mut contents = Vec::new();
                let result;

                match res_type
                {
                    ResourceType::Sheets =>
                        {
                            result = game.res_mut().sheets_mut().load_package(&package_name);
                        }
                };

                match result
                {
                    Ok(_) =>
                        {
                            contents = game.res_mut().sheets_mut().list_package_contents(&package_name);
                            info!("Loaded {} package \"{}\"", res_type.to_string(), &package_name);
                        },
                    Err(error) =>
                        {
                            let package_error = format!("Failed to load {} package \"{}\": {}",
                                                        res_type.to_string(), &package_name, error.to_string());
                            error!("{}", package_error);

                            self.package_errors.push(package_error);
                        }
                }

                res.package_contents.insert(package_name.clone(), contents);
            }

            res.packages_to_load.clear();
        }
    }

    pub fn open_document(&mut self, game : &mut GameControl, res_type : ResourceType, package_name : &str, item_name : &str)
    {
        let document = SheetDocument::load(self, game, package_name, item_name);
        let name = format!("{}/{}", package_name, item_name);

        if let Some(window_index) = self.name_to_document.get(&name)
        {
            self.document_to_focus = Some(*window_index);
        }
        else
        {
            self.document_to_focus = Some(self.documents.len());

            self.name_to_document.insert(name.clone(), self.documents.len());
            self.documents.push(DocumentEntry
            {
                document : Box::new(document),
                name
            });
        }
    }

    pub fn close_document(&mut self, index : usize)
    {
        self.documents.remove(index);
        self.refresh_name_lookup();
    }

    pub fn close_all_documents(&mut self)
    {
        self.documents.clear();
        self.refresh_name_lookup();
    }

    pub fn refresh_name_lookup(&mut self)
    {
        self.name_to_document.clear();

        for i in 0..self.documents.len()
        {
            let entry = &self.documents[i];
            self.name_to_document.insert(entry.name.clone(), i);
        }
    }

    pub fn reload(&mut self)
    {
        self.need_reload = true;
    }
}

impl Component for EditorControl {}

pub struct EditorGui
{
    packages_open : bool,
    demo_open : bool,
    imgui_guide_open : bool,
    imgui_about_open : bool,
}

impl EditorGui
{
    pub fn new() -> EditorGui
    {
        EditorGui
        {
            packages_open : true,
            demo_open : false,
            imgui_guide_open : false,
            imgui_about_open : false
        }
    }
    
    fn main_menubar(&mut self, editor : &mut EditorControl, ui : &mut imgui::Ui, game : &mut GameControl)
    {
        ui.main_menu_bar(||
        {
            ui.menu(im_str!("File"), true, ||
            {
                if imgui::MenuItem::new(im_str!("Close All Documents")).build(ui)
                {
                    editor.close_all_documents();
                }
                if imgui::MenuItem::new(im_str!("Reload Packages")).build(ui)
                {
                    editor.reload();
                }
                ui.separator();
                if imgui::MenuItem::new(im_str!("Switch Project")).build(ui)
                {
                    editor.switch_project(game);
                }
                ui.separator();
                if imgui::MenuItem::new(im_str!("Quit")).build(ui)
                {
                    game.quit();
                }
            });
            
            ui.menu(im_str!("View"), true, ||
            {
                if imgui::MenuItem::new(im_str!("Packages")).selected(self.packages_open).build(ui)
                {
                    self.packages_open = !self.packages_open;
                }
            });
            
            ui.menu(im_str!("Debug"), true, ||
            {
                if imgui::MenuItem::new(im_str!("Show imgui demo window")).selected(self.demo_open).build(ui)
                {
                    self.demo_open = !self.demo_open;
                }
            });
            
            ui.menu(im_str!("Help"), true, ||
            {
                if imgui::MenuItem::new(im_str!("ImGui User Guide")).selected(self.imgui_guide_open).build(ui)
                {
                    self.imgui_guide_open = !self.imgui_guide_open;
                }
                if imgui::MenuItem::new(im_str!("About ImGui")).selected(self.imgui_about_open).build(ui)
                {
                    self.imgui_about_open = !self.imgui_about_open;
                }
            });
        });
    }
    
    fn view_packages(&mut self, editor : &mut EditorControl, ui : &mut imgui::Ui, game : &mut GameControl)
    {
        let mut load_res = ResourceType::Sheets;
        let mut load_package_name = String::new();
        let mut load_item_name = String::new();

        if self.packages_open
        {
            imgui::Window::new(im_str!("Packages")).size([250.0, 600.0], imgui::Condition::FirstUseEver).opened(&mut self.packages_open).build(ui, ||
            {
                let mut package_list = Vec::new();
                
                std::mem::swap(&mut editor.package_list, &mut package_list);
                
                for package_name in &package_list
                {
                    imgui::TreeNode::new(&im_str!("{}", package_name)).flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH).build(ui, ||
                    {
                        for res_type in ResourceType::iter()
                        {
                            let res = &mut editor.res[res_type as usize];

                            imgui::TreeNode::new(&im_str!("{}", res_type.to_string())).flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
                                .build(ui, ||
                            {
                                if !res.package_contents.contains_key(package_name)
                                {
                                    res.packages_to_load.insert(package_name.clone());
                                }
                                else
                                {
                                    for item in res.package_contents[package_name].iter()
                                    {
                                        imgui::TreeNode::new(&im_str!("{}", item)).flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
                                            .bullet(true).opened(false, Condition::Always).build(ui, ||
                                        {
                                            load_res = res_type;
                                            load_package_name = package_name.clone();
                                            load_item_name = item.clone();
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
                
                std::mem::swap(&mut editor.package_list, &mut package_list);
            });
        }

        if !load_package_name.is_empty()
        {
            editor.open_document(game, load_res, &load_package_name, &load_item_name);
        }
    }

    fn document_windows(&mut self, editor : &mut EditorControl, ui : &mut imgui::Ui, game : &mut GameControl)
    {
        let mut documents_to_close = Vec::new();

        for i in 0..editor.documents.len()
        {
            let mut document = &mut editor.documents[i].document;
            let raise = editor.document_to_focus == Some(i);
            let mut window_open = true;
            let window_name = im_str!("{}", document.window_name());

            let window = imgui::Window::new(&window_name).focused(raise).opened(&mut window_open).position([320.0, 48.0], imgui::Condition::Appearing)
                .size([640.0, 640.0], imgui::Condition::Appearing);

            document.do_ui(game, window, ui);

            if !window_open
            {
                documents_to_close.push(i);
            }
        }

        for i in &documents_to_close
        {
            editor.close_document(*i);
        }

        editor.document_to_focus = None;
    }
    
    fn main_gui(&mut self, editor : &mut EditorControl, ui : &mut imgui::Ui, game : &mut GameControl)
    {
        self.main_menubar(editor, ui, game);
        self.view_packages(editor, ui, game);
        self.document_windows(editor, ui, game);
        
        if editor.package_errors.len() > 0
        {
            imgui::Window::new(im_str!("Package Errors")).position([640.0, 360.0], imgui::Condition::Appearing)
                .position_pivot([0.5, 0.5])
                .size([640.0, 360.0], imgui::Condition::Appearing).build(ui, ||
            {
                imgui::ChildWindow::new("package_error_list").size([0.0, -40.0]).border(true).build(ui, ||
                {
                    for error in &editor.package_errors
                    {
                        ui.text_wrapped(&im_str!("{}", error));
                    }
                });
                
                ui.spacing();
                
                if ui.button(im_str!("Dismiss"), [100.0, 30.0])
                {
                    editor.package_errors.clear();
                }
            });
        }
        
        if self.demo_open
        {
            ui.show_demo_window(&mut self.demo_open);
        }
        
        if self.imgui_guide_open
        {
            imgui::Window::new(im_str!("ImGui User Guide")).always_auto_resize(true)
                .opened(&mut self.imgui_guide_open).build(ui, ||
            {
                ui.show_user_guide();
            });
        }
        
        if self.imgui_about_open
        {
            ui.show_about_window(&mut self.imgui_about_open);
        }
    }
}

impl ImGuiSystem<NullSceneType> for EditorGui
{
    fn start(&mut self, _components : &mut ComponentControl, scene : &mut SceneControl<NullSceneType>, game : &mut GameControl)
    {
        let entity = scene.add_entity_later();
        let mut editor = EditorControl::new();

        editor.im_source_path.push_str(&editor.config.source_path);

        scene.add_component_later(&entity, editor);

        if let Some(imgui) = game.imgui_mut()
        {
            let mut style = imgui.style_mut();

            style.use_light_colors();
            style.window_title_align = [0.5, 0.5];
            style.frame_rounding = 1.0;
            style.frame_border_size = 1.0;
            style.window_rounding = 3.0;

            let colors = &mut style.colors;

            colors[StyleColor::Border as usize] = [0.00, 0.00, 0.00, 0.50];
            colors[StyleColor::FrameBgHovered as usize] = [0.98, 0.26, 0.52, 0.40];
            colors[StyleColor::FrameBgActive as usize] = [0.98, 0.26, 0.52, 0.67];
            colors[StyleColor::CheckMark as usize] = [0.98, 0.26, 0.52, 1.00];
            colors[StyleColor::SliderGrab as usize] = [0.98, 0.26, 0.52, 0.78];
            colors[StyleColor::SliderGrabActive as usize] = [0.80, 0.46, 0.58, 0.60];
            colors[StyleColor::Button as usize] = [0.98, 0.26, 0.52, 0.40];
            colors[StyleColor::ButtonHovered as usize] = [0.98, 0.26, 0.52, 1.00];
            colors[StyleColor::ButtonActive as usize] = [0.98, 0.06, 0.39, 1.00];
            colors[StyleColor::Header as usize] = [0.98, 0.26, 0.52, 0.31];
            colors[StyleColor::HeaderHovered as usize] = [0.98, 0.26, 0.52, 0.80];
            colors[StyleColor::HeaderActive as usize] = [0.98, 0.26, 0.52, 1.00];
            colors[StyleColor::SeparatorHovered as usize] = [0.80, 0.14, 0.37, 0.78];
            colors[StyleColor::SeparatorActive as usize] = [0.80, 0.14, 0.37, 1.00];
            colors[StyleColor::ResizeGripHovered as usize] = [0.98, 0.26, 0.52, 0.67];
            colors[StyleColor::ResizeGripActive as usize] = [0.98, 0.26, 0.52, 0.95];
            colors[StyleColor::Tab as usize] = [0.84, 0.76, 0.79, 0.93];
            colors[StyleColor::TabHovered as usize] = [0.98, 0.26, 0.52, 0.80];
            colors[StyleColor::TabActive as usize] = [0.88, 0.60, 0.70, 1.00];
            colors[StyleColor::TabUnfocusedActive as usize] = [0.91, 0.74, 0.80, 1.00];
            colors[StyleColor::TextSelectedBg as usize] = [0.98, 0.26, 0.52, 0.35];
            colors[StyleColor::DragDropTarget as usize] = [0.98, 0.26, 0.52, 0.95];
            colors[StyleColor::NavHighlight as usize] = [0.98, 0.26, 0.52, 0.80];
        }
    }

    fn end(&mut self, components : &mut ComponentControl, scene : &mut SceneControl<NullSceneType>, game : &mut GameControl)
    {
        let mut comp_editor_control = components.store_mut::<EditorControl>();
        let (_, editor) = comp_editor_control.iter_mut().next().unwrap();

        editor.save_config();
    }

    fn imgui_think(&mut self, ui : &mut imgui::Ui, components : &mut ComponentControl, scene : &mut SceneControl<NullSceneType>, game : &mut GameControl)
    {
        let mut comp_editor_control = components.store_mut::<EditorControl>();
        let (_, editor) = comp_editor_control.iter_mut().next().unwrap();

        editor.load_sources(game);

        if editor.source_id == 0
        {
            imgui::Window::new(im_str!("Select Package Source")).collapsible(false).save_settings(false)
                .always_auto_resize(true).position([640.0, 360.0], imgui::Condition::Appearing)
                .position_pivot([0.5, 0.5]).build(ui, ||
            {
                ui.text_wrapped(im_str!("Please enter the path to your game's data folder."));
                ui.spacing();
                
                let push = ui.push_item_width(250.0);
                if ui.input_text(im_str!(""), &mut editor.im_source_path)
                    .enter_returns_true(true).build()
                {
                    editor.load_source = true;
                }
                push.pop(ui);
                
                if ui.button(im_str!("Next"), [100.0, 30.0])
                {
                    editor.load_source = true;
                }
            });
        }
        else
        {
            self.main_gui(editor, ui, game);
        }
    }
}
