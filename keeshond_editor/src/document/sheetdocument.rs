use crate::document::Document;
use crate::editorgui::EditorControl;

use keeshond::gameloop::GameControl;
use keeshond_datapack::DataId;

use imgui::{im_str, Ui};

pub struct SheetDocument
{
    package_name : String,
    item_name : String,
    data_id : DataId
}

impl Document for SheetDocument
{
    fn load(editor : &mut EditorControl, game : &mut GameControl, package_name : &str, item_name: &str) -> Self where Self : Sized
    {
        let mut data_id = 0;

        if let Ok(id) = game.res_mut().sheets_mut().get_id_mut(package_name, item_name)
        {
            data_id = id;
        }

        SheetDocument
        {
            package_name : String::from(package_name),
            item_name : String::from(item_name),
            data_id
        }
    }

    fn do_ui(&mut self, game: &mut GameControl, window_builder : imgui::Window, ui : &mut imgui::Ui)
    {
        window_builder.build(ui, ||
        {
            if let Some(sheet) = game.res_mut().sheets_mut().get_mut(self.data_id)
            {
                ui.text_wrapped(&im_str!("Width: {}", sheet.width()));
                ui.text_wrapped(&im_str!("Height: {}", sheet.height()));
            }
        });
    }

    fn window_name(&self) -> String
    {
        format!("Sheet :: {}/{}", self.package_name, self.item_name)
    }
}
