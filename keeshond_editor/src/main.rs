extern crate keeshond;
extern crate keeshond_datapack;
extern crate strum;
extern crate confy;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate strum_macros;
#[macro_use] extern crate log;

use keeshond::gameloop::{Gameloop, GameInfo};
use keeshond::scene::SimpleScene;

mod editorgui;
mod document;

////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "keeshond_editor",
        friendly_name : "Keeshond Game Editor",
        base_width : 1280,
        base_height : 720,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    let mut my_scene = Box::new(SimpleScene::new());

    my_scene.set_imgui_system(Box::new(editorgui::EditorGui::new()));
    
    gameloop.control_mut().goto_constructed_scene(my_scene);
    gameloop.run();
}
