use crate::editorgui::EditorControl;
use keeshond::gameloop::GameControl;

pub mod sheetdocument;

pub trait Document
{
    fn load(editor : &mut EditorControl, game : &mut GameControl, package_name : &str, item_name : &str) -> Self where Self : Sized;
    fn do_ui(&mut self, game: &mut GameControl, window_builder : imgui::Window, ui : &mut imgui::Ui);
    fn window_name(&self) -> String;
}
